import 'package:flutter/material.dart';

class AdminFloatingActionButton extends StatelessWidget {
  final Function onPressed;

  const AdminFloatingActionButton({
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 75.0,
      width: 75.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(18.0),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
          ),
        ],
      ),
      child: FloatingActionButton(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Image(
            image: AssetImage('assets/images/icons/rounded_arrows.png'),
          ),
        ),
        onPressed: onPressed,
      ),
    );
  }
}
