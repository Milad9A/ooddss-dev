import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';

class PaymentDetails extends StatelessWidget {
  final bool couponHasDottedLine;

  const PaymentDetails({
    this.couponHasDottedLine = false,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        couponHasDottedLine
            ? Column(
                children: [
                  DottedBorder(
                    strokeWidth: 1.2,
                    padding: EdgeInsets.all(0),
                    dashPattern: [8],
                    child: Container(
                      padding: EdgeInsets.only(
                          bottom: 20.0, right: 12.0, left: 12.0, top: 8.0),
                      color: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            'Remove',
                            style: TextStyle(
                              fontSize: 11.0,
                              color: Color(0xFFDC264D),
                              decoration: TextDecoration.underline,
                              decorationThickness: 1.5,
                            ),
                          ),
                          SizedBox(height: 10.0),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Used Coupon',
                                style: TextStyle(
                                  fontSize: 15.0,
                                ),
                              ),
                              Text(
                                'OODDSS10',
                                style: TextStyle(
                                  fontSize: 15.0,
                                  color: Color(0xFF6F7071),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0),
                ],
              )
            : Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Used Coupon',
                          style: TextStyle(
                            fontSize: 15.0,
                          ),
                        ),
                        Text(
                          'OODDSS10',
                          style: TextStyle(
                            fontSize: 15.0,
                            color: Color(0xFF6F7071),
                          ),
                        ),
                      ],
                    ),
                    Divider(thickness: 1.0),
                  ],
                ),
            ),
        Padding(
          padding: couponHasDottedLine
              ? EdgeInsets.symmetric(horizontal: 14.0)
              : EdgeInsets.symmetric(horizontal: 8.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 80.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Subtotal'),
                        Text(':'),
                      ],
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                    children: [
                      Text(
                        '35.500',
                        style: TextStyle(
                          color: Color(0xFF6F7071),
                        ),
                      ),
                      Text(
                        'KWD',
                        textAlign: TextAlign.end,
                        style: TextStyle(
                          fontSize: 7.0,
                          color: Color(0xFF6F7071),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 6.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 80.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Discount'),
                        Text(':'),
                      ],
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                    children: [
                      Text(
                        '-5.500',
                        style: TextStyle(
                          color: Color(0xFFDC264D),
                        ),
                      ),
                      Text(
                        'KWD',
                        textAlign: TextAlign.end,
                        style: TextStyle(
                          fontSize: 7.0,
                          color: Color(0xFFDC264D),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 6.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 80.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Shipping'),
                        Text(':'),
                      ],
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                    children: [
                      Text(
                        '2.500',
                        style: TextStyle(
                          color: Color(0xFF7FAB33),
                        ),
                      ),
                      Text(
                        'KWD',
                        textAlign: TextAlign.end,
                        style: TextStyle(
                          fontSize: 7.0,
                          color: Color(0xFF7FAB33),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 6.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Total',
                    style: TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                    children: [
                      Text(
                        '32.500',
                        style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        'KWD',
                        textAlign: TextAlign.end,
                        style: TextStyle(
                          fontSize: 8.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
