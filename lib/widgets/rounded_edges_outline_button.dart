import 'package:flutter/material.dart';

class RoundedEdgesOutlineButton extends StatelessWidget {
  final Color borderColor;
  final String title;
  final Color textColor;
  final Function onPressed;
  final double height;

  const RoundedEdgesOutlineButton({
    @required this.borderColor,
    @required this.title,
    @required this.textColor,
    @required this.onPressed,
    this.height = 52.0,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      child: OutlineButton(
        padding: EdgeInsets.only(top: 2.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        ),
        borderSide: BorderSide(
          color: borderColor,
          width: 1.7,
        ),
        onPressed: onPressed,
        child: Text(
          title,
          style: TextStyle(
            color: textColor,
            fontSize: 18.0,
            letterSpacing: 1.25,
          ),
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );
  }
}
