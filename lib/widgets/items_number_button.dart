import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class ItemsNumberButton extends StatefulWidget {
  final Function onChanged;

  const ItemsNumberButton({
    @required this.onChanged,
  });

  @override
  _ItemsNumberButtonState createState() => _ItemsNumberButtonState();
}

class _ItemsNumberButtonState extends State<ItemsNumberButton> {
  var value = 1;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60.0,
//      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(30.0),
        border: Border.all(
          width: 1.3,
          color: Color(0xFFCCCADA),
        ),
      ),
      child: Row(
        children: [
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    setState(() {
                      if (value > 0) value--;
                    });
                    widget.onChanged(value);
                  },
                  child: Icon(
                    Icons.remove,
                    size: 20.0,
                  ),
                ),
                Container(
                  width: 30.0,
                  child: Center(
                    child: AutoSizeText(
                      value.toString(),
                      maxLines: 1,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0,
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      value++;
                    });
                    widget.onChanged(value);
                  },
                  child: Icon(
                    Icons.add,
                    size: 20.0,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
