import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import '../models/my_notification_model.dart';
import '../utils/colors_repository.dart' as repo;

class NotificationListTile extends StatefulWidget {
  final MyNotification notification;
  final Function didChange;

  NotificationListTile(this.notification, {@required this.didChange})
      : assert(notification != null, "A notification object can't be null");

  @override
  _NotificationListTileState createState() => _NotificationListTileState();
}

class _NotificationListTileState extends State<NotificationListTile> {
  Color _fabColor;
  SlidableController _controller;

  void handleSlideIsOpenChanged(bool isOpen) {
    setState(() {
      _fabColor = isOpen ? repo.greyBackGround : Colors.white;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    _fabColor = Colors.white;
    _controller = new SlidableController(
        onSlideIsOpenChanged: handleSlideIsOpenChanged,
        onSlideAnimationChanged: (u) {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
              bottom: BorderSide(
                  color: Colors.grey, width: 0.24, style: BorderStyle.solid),
              top: BorderSide(
                  color: Colors.grey, width: 0.24, style: BorderStyle.solid))),
      child: Slidable(
        actionPane: SlidableDrawerActionPane(),
        closeOnScroll: true,
        controller: _controller,
        actionExtentRatio: 0.14,
        child: Container(
          color: _fabColor,
          child: ListTile(
            // should deal null in model notification
            dense: true,
            title: Text(
              widget.notification.title != null
                  ? widget.notification.title
                  : '',
              style: TextStyle(
                color: const Color(0xFF0F0F0F),
                fontSize: 15.0,
                fontFamily: 'Grava',
                fontWeight: FontWeight.bold,
              ),
            ),
            subtitle: Text(
              widget.notification.body != null ? widget.notification.body : '',
              style: TextStyle(
                color: repo.greyTextColor,
                fontSize: 14.8,
                fontFamily: 'Grava',
              ),
            ),
            onTap: () {},
          ),
        ),
        secondaryActions: <Widget>[
          Container(
            decoration: BoxDecoration(color: repo.red),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 2,
                ),
                Image.asset(
                  'assets/images/icons/remove.png',
                  width: 18,
                  height: 18,
                ),
                SizedBox(
                  height: 6,
                ),
                Text(
                  'Remove',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 11.0,
                    fontFamily: 'Grava',
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
