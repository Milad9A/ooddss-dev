import 'package:flutter/material.dart';

class CustomDropDownButton<T> extends StatefulWidget {
  final ValueChanged<T> onChange;
  final String title;
  final T selectedItem;
  final Widget widget;
  final List<T> items;
  final String icon;

  CustomDropDownButton(
      {this.onChange,
      this.title,
      this.selectedItem,
      this.widget,
      this.items,
      this.icon})
      : assert(onChange != null, "Function onChange can't be  null")
  // ,assert(items != null, "Items of Custom DropDown can't be null")
  ;

  @override
  _CustomDropDownButtonState createState() => _CustomDropDownButtonState();
}

class _CustomDropDownButtonState<T> extends State<CustomDropDownButton> {
  T selectedItem;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    selectedItem = widget.selectedItem;
  }

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      onTap: widget.items != null
          ? () async {
              dynamic item = await showBottomSheet(context, widget.items);
              if (item != null) {
                setState(() {
                  selectedItem = item;
                });
                widget.onChange(item);
              }
            }
          : () {},
      child: Card(
        elevation: 2.6,
        color: Colors.white,
        shape: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(18)),
            borderSide: BorderSide.none),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(18)),
          ),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(18)),
                  ),
                  padding:
                      EdgeInsets.only(top: 5.5, bottom: 5, left: 20, right: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: widget.title == null
                        ? <Widget>[
                            selectedItem != null
                                ? Text(
                                    selectedItem.toString(),
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 16.0),
                                  )
                                : Container()
                          ]
                        : <Widget>[
                            Text(
                              widget.title,
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: selectedItem != null ? 11.5 : 14.5),
                            ),
                            SizedBox(
                              height: 1,
                            ),
                            selectedItem != null
                                ? Text(
                                    selectedItem.toString(),
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 14.5),
                                  )
                                : Container()
                          ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(14),
                child: Image.asset(
                  widget.icon != null
                      ? widget.icon
                      : "assets/images/icons/arrow_down.png",
                  height: 17,
                  width: 17,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Future<dynamic> showBottomSheet(
    BuildContext context, List<dynamic> items) async {
  return showModalBottomSheet<dynamic>(
    shape: OutlineInputBorder(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16.0), topRight: Radius.circular(16.0)),
    ),
    context: context,
    builder: (builder) {
      return Container(
        height: 180.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16.0), topRight: Radius.circular(16.0)),
          color: Colors.white,
        ),
        child: ListView(
          children: items
              .map((e) => ListTile(
                    title: Text(e.toString()),
                    onTap: () {
                      Navigator.pop(context, e);
                    },
                  ))
              .toList(),
        ),
      );
    },
  );
}
