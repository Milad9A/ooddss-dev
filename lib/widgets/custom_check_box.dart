import 'package:flutter/material.dart';

class CustomCheckBox extends StatefulWidget {
  final int value;
  final String title;
  bool isChecked;
  final Widget widget;
  final Function onSelected;

  CustomCheckBox(
      {@required this.value,
      @required this.title,
      @required this.isChecked,
      @required this.onSelected,
      this.widget});

  @override
  _CustomCheckBoxState createState() => _CustomCheckBoxState();
}

class _CustomCheckBoxState extends State<CustomCheckBox> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onSelected,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 34,
            height: 34,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                color: Colors.grey[400],
                style: BorderStyle.solid,
                width: 0.6,
              ),
              borderRadius: BorderRadius.all(Radius.circular(12)),
            ),
            child: widget.isChecked
                ? Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: Image.asset(
                      'assets/images/icons/check.png',
                    ),
                  )
                : Container(),
          ),
          SizedBox(
            width: 12,
          ),
          Flexible(
              child: widget.widget == null
                  ? Text(
                      widget.title,
                      style: TextStyle(color: Colors.black, fontSize: 16),
                    )
                  : widget.widget),
        ],
      ),
    );
  }
}

class CustomGroupCheckBox extends StatefulWidget {
  final List<String> titles;
  final List<Widget> widgets;
  final bool allowNullCheck;
  final ValueChanged<int> onChange;
  int idxChecked;

  CustomGroupCheckBox(
      {@required this.titles,
      this.allowNullCheck = false,
      @required this.onChange,
      this.idxChecked,
      this.widgets});

  @override
  _CustomGroupCheckBoxState createState() => _CustomGroupCheckBoxState();
}

class _CustomGroupCheckBoxState extends State<CustomGroupCheckBox> {
  int idxChecked;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    idxChecked = widget.idxChecked;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: widget.titles
          .asMap()
          .keys
          .map((idx) => Padding(
                padding: const EdgeInsets.only(top: 8.0, bottom: 8),
                child: CustomCheckBox(
                  title: widget.titles.elementAt(idx),
                  widget: widget.widgets == null ||
                          widget.widgets.length != widget.titles.length
                      ? null
                      : widget.widgets.elementAt(idx),
                  value: idx,
                  isChecked: idxChecked == idx ? true : false,
                  onSelected: () {
                    if (widget.allowNullCheck == true) {
                      if (idxChecked == idx) {
                        setState(() {
                          idxChecked = -1;
                        });
                        widget.onChange(-1);
                      } else {
                        setState(() {
                          idxChecked = idx;
                        });
                        widget.onChange(idx);
                      }
                    } else {
                      if (idxChecked != idx) {
                        setState(() {
                          idxChecked = idx;
                        });

                        widget.onChange(idx);
                      }
                    }
                  },
                ),
              ))
          .toList(),
    );
  }
}
