import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import '../models/product_model.dart';

import 'items_number_button.dart';

class ShoppingCartTile extends StatefulWidget {
  final Product product;
  final bool isLastTile;
  final bool isWishlistTile;

  const ShoppingCartTile({
    @required this.product,
    @required this.isLastTile,
    this.isWishlistTile = false,
  });

  @override
  _ShoppingCartTileState createState() => _ShoppingCartTileState();
}

class _ShoppingCartTileState extends State<ShoppingCartTile> {
  var myGroup = AutoSizeGroup();

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Column(
      children: [
        Container(
          padding: EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 10.0,
          ),
          child: Column(
            children: [
              Container(
                height: screenWidth * 0.2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  children: [
                    Expanded(
                      child: Row(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(18.0),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black45,
                                  blurRadius: 3.3,
                                  offset: Offset(0, 2),
                                ),
                              ],
                            ),
                            child: Container(
                              height: screenWidth * 0.2,
                              width: screenWidth * 0.2,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                              child: Image(
                                height: screenWidth * 0.2,
                                width: screenWidth * 0.2,
                                image: AssetImage(
                                  widget.product.productImageURL,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: 20.0),
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  widget.product.productName,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                !widget.isWishlistTile
                                    ? Container(
                                        height: 40.0,
                                        width: 95.0,
                                        child: ItemsNumberButton(
                                          onChanged: (value) {
                                            widget.product
                                                .onItemsNumberChanged(value);
                                          },
                                        ),
                                      )
                                    : Container(
                                        width: 85.0,
                                        child: AutoSizeText(
                                          'Available : ${widget.product.numberOfAvailableProducts}',
                                          maxLines: 1,
                                          style: TextStyle(
                                            color: Color(0xFFDC264D),
                                            fontSize: 13.0,
                                          ),
                                        ),
                                      ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          children: [
                            Text(
                              widget.product.productPrice,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Color(0xFF7D7E7F),
                              ),
                            ),
                            SizedBox(width: 2.0),
                            Text(
                              widget.product.productPriceCurrency,
                              style: TextStyle(
                                fontSize: 7.0,
                                fontWeight: FontWeight.bold,
                                color: Color(0xFF7D7E7F),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            GestureDetector(
                              onTap: widget.isWishlistTile
                                  ? widget.product.onPressedHeart
                                  : widget.product.onPressedAddToCart,
                              child: Image(
                                height: 23.0,
                                image: AssetImage(
                                  !widget.isWishlistTile
                                      ? 'assets/images/icons/blank_heart.png'
                                      : 'assets/images/icons/add_to_cart_icon.png',
                                ),
                              ),
                            ),
                            SizedBox(width: 25.0),
                            GestureDetector(
                              onTap: widget.product.onPressedDelete,
                              child: Image(
                                height: 24.0,
                                image: AssetImage(
                                  'assets/images/icons/delete.png',
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 15.0),
              !widget.isWishlistTile
                  ? Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 85.0,
                          child: AutoSizeText(
                            'Available : ${widget.product.numberOfAvailableProducts}',
                            maxLines: 1,
                            style: TextStyle(
                              color: Color(0xFFDC264D),
                              fontSize: 13.0,
                            ),
                          ),
                        ),
                        SizedBox(width: 15.0),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              widget.product.color != null
                                  ? SpecsRow(
                                      specName: 'Color',
                                      specValue: widget.product.color,
                                    )
                                  : SizedBox.shrink(),
                              (widget.product.size != null &&
                                      widget.product.color != null)
                                  ? SizedBox(height: 14.0)
                                  : SizedBox.shrink(),
                              widget.product.size != null
                                  ? SpecsRow(
                                      specName: 'Size  ',
                                      specValue: widget.product.size,
                                    )
                                  : SizedBox.shrink(),
                            ],
                          ),
                        ),
                      ],
                    )
                  : SizedBox.shrink(),
            ],
          ),
        ),
        !widget.isLastTile
            ? Divider(
                indent: 10.0,
                endIndent: 14.0,
                color: Color(0xFF7D7E7F),
                thickness: 0.5,
              )
            : SizedBox.shrink(),
      ],
    );
  }
}

class SpecsRow extends StatelessWidget {
  final String specName;
  final String specValue;

  const SpecsRow({
    @required this.specName,
    @required this.specValue,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          '$specName',
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(width: 10.0),
        Text(
          ':  $specValue',
          style: TextStyle(
            fontWeight: FontWeight.w600,
            color: Color(0xFF7D7E7F),
          ),
        ),
      ],
    );
  }
}
