import 'package:flutter/material.dart';
import '../models/product_model.dart';

import 'product_order_tile.dart';

class ProductsListCard extends StatelessWidget {
  final List<Product> products;
  final Widget childOfProductTile;
  final bool hasImage;

  const ProductsListCard({
    @required this.products,
    this.childOfProductTile,
    this.hasImage = true,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(14.0),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            offset: Offset(2.0, 2.0),
            color: Colors.black12,
          ),
          BoxShadow(
            offset: Offset(-2.0, 0.0),
            color: Colors.black12,
          ),
        ],
      ),
      padding: EdgeInsets.symmetric(vertical: 10.0),
      child: Column(
        children: products
            .asMap()
            .map(
              (index, product) => MapEntry(
                index,
                Container(
                  child: ProductOrderTile(
                    product: product,
                    index: index,
                    hasImage: hasImage,
                    child: childOfProductTile,
                  ),
                ),
              ),
            )
            .values
            .toList(),
      ),
    );
  }
}
