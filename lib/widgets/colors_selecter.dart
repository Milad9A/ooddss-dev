import 'package:flutter/material.dart';

// This class for display list of list of colors and selected the color
// [[Colors.red, Colors.black],[ Colors.black],[Colors.purpleAccent], [Colors.lightGreenAccent],]

class ColorsSelecter extends StatefulWidget {
  final List<List<Color>> colors;
  final ValueChanged<List<Color>> onSelected;
  final bool enableSelected;

  ColorsSelecter(
      {@required this.colors, this.onSelected, this.enableSelected = true})
      : assert(colors != null,
            "ColorsSelecter must contain list of list of colors");

  @override
  _ColorsSelecterState createState() => _ColorsSelecterState();
}

class _ColorsSelecterState extends State<ColorsSelecter> {
  List<Color> colorSelected;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return GridView.extent(
      maxCrossAxisExtent: width / 11,
      physics: NeverScrollableScrollPhysics(),
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      crossAxisSpacing: 12.0,
      mainAxisSpacing: 12.0,
      children: widget.colors
          .map((col) => GestureDetector(
                onTap: widget.enableSelected
                    ? () {
                        setState(() {
                          colorSelected = col;
                        });
                        widget.onSelected(col);
                      }
                    : null,
                child: Container(
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.circle,
                      border: Border.all(
                          width: 0.6,
                          color:
                              colorSelected == col ? Colors.red : Colors.grey,
                          style: BorderStyle.solid)),
                  child: Container(
                    decoration: BoxDecoration(
                      color: col.elementAt(0),
                      shape: BoxShape.circle,
                      gradient: col.length > 1
                          ? LinearGradient(
                              stops: [1, 1],
                              begin: Alignment.centerRight,
                              end: Alignment.center,
                              colors: col,
                            )
                          : null,
                    ),
                  ),
                ),
              ))
          .toList(),
    );
  }
}
