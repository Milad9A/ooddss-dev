import 'package:flutter/material.dart';

import '../../expandable_view.dart';

class PaymentInformationExpandableContainer extends StatelessWidget {
  const PaymentInformationExpandableContainer({
    Key key,
    @required this.fontSize,
    @required this.kwdFontSize,
  }) : super(key: key);

  final double fontSize;
  final double kwdFontSize;

  @override
  Widget build(BuildContext context) {
    return ExpandableView(
        title: 'YOUR PAYMENT',
        leadingIconPath: 'assets/images/icons/payment_icon.png',
        childWidget: Container(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(vertical:8.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 5,
                        child: Text(
                          'Payment Method',
                          style: TextStyle(
                              fontSize: fontSize,
                              fontWeight: FontWeight.w700,
                              fontFamily: 'Grava'),
                        ),
                      ),
                      Expanded(

                        flex: 1,
                        child: Text(':',textAlign:TextAlign.start,style: TextStyle(
                            fontSize: fontSize,
                            fontWeight: FontWeight.w700,
                            fontFamily: 'Grava'),),
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(
                          'CASH',
                          textAlign: TextAlign.end,
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontFamily: 'Grava',
                              color: Color(0xff0595ed),
                              fontSize: fontSize),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical:8.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 5,
                        child: Text(
                          'SubTotal',
                          style: TextStyle(
                              fontSize: fontSize,
                              fontWeight: FontWeight.w400,
                              fontFamily: 'Grava'),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(':',textAlign:TextAlign.start,style: TextStyle(
                            fontSize: fontSize,
                            fontWeight: FontWeight.w700,
                            fontFamily: 'Grava'),),
                      ),
                      Expanded(
                        flex: 4,
                        child: RichText(
                          textAlign: TextAlign.end,
                          text: TextSpan(
                              text: '35.350',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: fontSize,
                                fontFamily: 'Grava',
                                color: Color(0xff858585),
                              ),
                              children: [
                                TextSpan(
                                  text: ' KWD',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: kwdFontSize,
                                    fontFamily: 'Grava',
                                    color: Color(0xff858585),
                                  ),
                                ),
                              ]),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical:8.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 5,
                        child: Text(
                          'Discount',
                          style: TextStyle(
                              fontSize: fontSize,
                              fontWeight: FontWeight.w400,
                              fontFamily: 'Grava'),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(':',textAlign:TextAlign.start,style: TextStyle(
                            fontSize: fontSize,
                            fontWeight: FontWeight.w700,
                            fontFamily: 'Grava'),),
                      ),
                      Expanded(
                        flex: 4,
                        child: RichText(
                          textAlign: TextAlign.end,
                          text: TextSpan(
                              text: '-5.500',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: fontSize,
                                fontFamily: 'Grava',
                                color: Color(0xffdc264d),
                              ),
                              children: [
                                TextSpan(
                                  text: ' KWD',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: kwdFontSize,
                                    fontFamily: 'Grava',
                                    color: Color(0xffdc264d),
                                  ),
                                ),
                              ]),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical:8.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 5,
                        child: Text(
                          'Shipping',
                          style: TextStyle(
                              fontSize: fontSize,
                              fontWeight: FontWeight.w400,
                              fontFamily: 'Grava'),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(':',textAlign:TextAlign.start,style: TextStyle(
                            fontSize: fontSize,
                            fontWeight: FontWeight.w700,
                            fontFamily: 'Grava'),),
                      ),
                      Expanded(
                        flex: 4,
                        child: RichText(
                          textAlign: TextAlign.end,
                          text: TextSpan(
                              text: '2500',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: fontSize,
                                fontFamily: 'Grava',
                                color: Color(0xff7fab33),
                              ),
                              children: [
                                TextSpan(
                                  text: ' KWD',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: kwdFontSize,
                                    fontFamily: 'Grava',
                                    color: Color(0xff7fab33),
                                  ),
                                ),
                              ]),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical:8.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 5,
                        child: Text(
                          'Total',
                          style: TextStyle(
                              fontSize: fontSize,
                              fontWeight: FontWeight.w700,
                              fontFamily: 'Grava'),
                        ),
                      ),
                      Expanded(

                        flex: 1,
                        child: Text(':',textAlign:TextAlign.start,style: TextStyle(
                            fontSize: fontSize,
                            fontWeight: FontWeight.w700,
                            fontFamily: 'Grava'),),
                      ),
                      Expanded(
                        flex: 4,
                        child: RichText(
                          textAlign: TextAlign.end,
                          text: TextSpan(
                              text: '23.500',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: fontSize+5,
                                fontFamily: 'Grava',
                                color: Colors.black,
                              ),
                              children: [
                                TextSpan(
                                  text: ' KWD',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: kwdFontSize+5,
                                    fontFamily: 'Grava',
                                    color: Colors.black,
                                  ),
                                ),
                              ]),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}