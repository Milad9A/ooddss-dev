import 'package:flutter/material.dart';

class FiltersContainer extends StatelessWidget {
  const FiltersContainer({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(color: Colors.grey[200],
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Container(

                child: ListTile(
                  title: Text('CATEGORY',style: TextStyle(fontFamily: 'Grava',fontSize: 16,fontWeight: FontWeight.w400),),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(top:2.0),
                    child: (Text('Arabic Books',style: TextStyle(fontFamily: 'Grava',fontSize: 16,fontWeight: FontWeight.w400,color: Color(0xaa353535)),)),
                  ),
                  trailing: (Image.asset('assets/images/icons/arrow_right.png',width: 7,height: 13,)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(12,0,12,0),
                child: Divider(height: 4,color: Colors.black45,),
              ),
              Container(

                child: ListTile(
                  title: Text('BRAND',style: TextStyle(fontFamily: 'Grava',fontSize: 16,fontWeight: FontWeight.w400),),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(top:2.0),
                    child: (Text('Apple',style: TextStyle(fontFamily: 'Grava',fontSize: 16,fontWeight: FontWeight.w400,color: Color(0xaa353535)),)),
                  ),
                  trailing: (Image.asset('assets/images/icons/arrow_right.png',width: 7,height: 13,)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(12,0,12,0),
                child: Divider(height: 4,color: Colors.black45,),
              ),
              Container(

                child: ListTile(
                  title: Text('GENDER',style: TextStyle(fontFamily: 'Grava',fontSize: 16,fontWeight: FontWeight.w400),),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(top:2.0),
                    child: (Text('None',style: TextStyle(fontFamily: 'Grava',fontSize: 16,fontWeight: FontWeight.w400,color: Color(0xaa353535)),)),
                  ),
                  trailing: (Image.asset('assets/images/icons/arrow_right.png',width: 7,height: 13,)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(12,0,12,0),
                child: Divider(height: 4,color: Colors.black45,),
              ),
              Container(

                child: ListTile(
                  title: Text('SHIPPING',style: TextStyle(fontFamily: 'Grava',fontSize: 16,fontWeight: FontWeight.w400),),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(top:2.0),
                    child: (Text('OODDSS Shipping',style: TextStyle(fontFamily: 'Grava',fontSize: 16,fontWeight: FontWeight.w400,color: Color(0xaa353535)),)),
                  ),
                  trailing: (Image.asset('assets/images/icons/arrow_right.png',width: 7,height: 13,)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(12,0,12,0),
                child: Divider(height: 4,color: Colors.black45,),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
