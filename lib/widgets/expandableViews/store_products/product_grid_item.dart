import 'package:flutter/material.dart';

class ProductGridItem extends StatelessWidget {
  const ProductGridItem({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(color: Color(0x55353535)),
              right: BorderSide(color: Color(0x55353535)))),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(
                left: 22.66, top: 30, right: 2, bottom: 2),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Image.asset(
                'assets/images/icons/blank_heart.png',
                scale: 2,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 29.66, bottom: 29.66),
            child: FadeInImage(
              height: 133.66,
              width: 90,
              fit: BoxFit.contain,
              placeholder: AssetImage(
                'assets/images/icons/test_img.jpg',
              ),
              image: AssetImage(
                'assets/images/icons/shose_test.png',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(19.33, 0, 19.33, 23.33),
            child: Text(
              'Black & Red Shoes',
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  fontFamily: 'Grava',
                  fontWeight: FontWeight.w500,
                  fontSize: 16.66),
            ),
          ),
          Padding(
              padding: const EdgeInsets.fromLTRB(19.33, 0, 19.33, 5.66),
              child: Text(
                '35.000 KWD',
                textAlign: TextAlign.start,
                style: TextStyle(
                  decoration: TextDecoration.lineThrough,
                  fontWeight: FontWeight.w500,
                  fontSize: 11.94,
                  fontFamily: 'Grava',
                  color: Color(0xbb353535),
                ),
              )),
          Padding(
            padding: const EdgeInsets.fromLTRB(19.33, 0, 19.33, 5.66),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 8,
                  child: RichText(
                    textAlign: TextAlign.start,
                    text: TextSpan(
                        text: '25.99',
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 20.83,
                          fontFamily: 'Grava',
                          color: Colors.black,
                        ),
                        children: [
                          TextSpan(
                            text: ' KWD',
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 11.11,
                              fontFamily: 'Grava',
                              color: Colors.black,
                            ),
                          ),
                        ]),
                  ),
                ),
                Image.asset(
                  'assets/images/icons/add_to_cart_icon.png',
                  width: 18,
                  height: 21.33,
                  fit: BoxFit.fill,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
