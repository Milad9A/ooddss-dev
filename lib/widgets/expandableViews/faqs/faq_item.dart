import 'package:flutter/material.dart';

import 'text_expansion_tile.dart';

class FaqItem extends StatelessWidget {
  const FaqItem({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8),
      child: TextExpansionTile(
        title: Text('SHOP',
            style: TextStyle(
                fontFamily: 'Grava',
                fontWeight: FontWeight.w700,
                fontSize: 18)),
        children: <Widget>[
          TextExpansionTile(
            title: Padding(
              padding: const EdgeInsets.symmetric(vertical: 4.0),
              child: Text(
                'How do we make an return?',
                style: TextStyle(
                    color: Color(0xff0f6da9),
                    fontWeight: FontWeight.w400,
                    fontFamily: 'Grava',
                    fontSize: 17.5),
              ),
            ),
            trailing: Icon(
              Icons.add,
              color: Color(0xff0f6da9),
            ),
            children: <Widget>[
              Container(
                child: Text(
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontFamily: 'Grava',
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                  ),
                ),
              )
            ],
          ),
          TextExpansionTile(
            title: Padding(
              padding: const EdgeInsets.symmetric(vertical: 4.0),
              child: Text(
                'How do we make an return?',
                style: TextStyle(
                    color: Color(0xff0f6da9),
                    fontWeight: FontWeight.w400,
                    fontFamily: 'Grava',
                    fontSize: 17.5),
              ),
            ),
            trailing: Icon(
              Icons.add,
              color: Color(0xff0f6da9),
            ),
            children: <Widget>[
              Container(
                child: Text(
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontFamily: 'Grava',
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
