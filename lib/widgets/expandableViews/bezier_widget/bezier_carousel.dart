import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

import 'bezier_container.dart';

class BezierCarouselContainer extends StatefulWidget {
  @override
  _BezierCarouselContainerState createState() =>
      _BezierCarouselContainerState();
}

class _BezierCarouselContainerState extends State<BezierCarouselContainer> {
  List<Widget> images = [
    Image.asset(
      'assets/images/icons/landscape.jpg',
      fit: BoxFit.fill,
    ),
    Image.asset(
      'assets/images/icons/landscape.jpg',
      fit: BoxFit.cover,
    ),
    Image.asset(
      'assets/images/icons/landscape.jpg',
      fit: BoxFit.cover,
    ),
  ];
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * .25,
          child: ClipPath(
            clipper: BezierClipper(),
            child: Container(
              color: Colors.transparent,
              child: CarouselSlider(
                options: CarouselOptions(
                    disableCenter: true,
                    viewportFraction: 1,
                    enlargeCenterPage: false,
                    onPageChanged: (index, reason) {
                      setState(() {
                        _current = index;
                      });
                    }),
                items: images,
              ),
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: images.map((url) {
            int index = images.indexOf(url);
            return Container(
              width: _current == index ? 8 : 6.0,
              height: 40,
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: _current == index
                    ? Colors.lightGreenAccent
                    : Color.fromRGBO(0, 0, 0, 0.3),
              ),
            );
          }).toList(),
        )
      ],
    );
  }
}
