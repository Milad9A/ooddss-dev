import 'package:flutter/material.dart';

class BezierContainer extends StatefulWidget {
  final String image ;

  BezierContainer({this.image = 'assets/images/icons/test_img.jpg'});

  @override
  _BezierContainerState createState() => _BezierContainerState();
}

class _BezierContainerState extends State<BezierContainer> {
  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: BezierClipper(),
      child: Container(
        color: Colors.red,
        height: MediaQuery.of(context).size.height *.3,
        width: MediaQuery.of(context).size.width,
        child: Image.asset(widget.image,fit: BoxFit.fill,),
      ),
    );
  }
}

class BezierClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();
    path.lineTo(0, 0);
    path.lineTo(0, size.height);
    path.quadraticBezierTo(size.width/2, size.height*.8, size.width, size.height);
    path.lineTo(size.width, 0);
    path.quadraticBezierTo(size.width/2, size.height*.2, 0, 0);
    return path;


  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}
