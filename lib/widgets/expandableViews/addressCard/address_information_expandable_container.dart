import 'package:flutter/material.dart';

import '../../expandable_view.dart';

class AddressInformationExpandableContainer extends StatelessWidget {
  const AddressInformationExpandableContainer({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ExpandableView(
      title: 'BILLING ADDRESS',
      leadingIconPath: 'assets/images/icons/billing_address_icon.png',
      childWidget: Container(
        child: Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(50, 0, 10, 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Dar Al Awadhi Tower, Ahmad',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontFamily: 'Grava',
                      fontSize: 15,
                      fontWeight: FontWeight.w400),
                ),
                Text(
                  'Al Gaber Street, 25th Floor,',
                  style: TextStyle(
                      fontFamily: 'Grava',
                      fontSize: 15,
                      fontWeight: FontWeight.w400),
                ),
                Text(
                  'KUWAITNET, ',
                  style: TextStyle(
                      fontFamily: 'Grava',
                      fontSize: 15,
                      fontWeight: FontWeight.w500),
                ),
                Text(
                  'Opposite Arzan Financial House',
                  style: TextStyle(
                      fontFamily: 'Grava',
                      fontSize: 15,
                      fontWeight: FontWeight.w400),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
