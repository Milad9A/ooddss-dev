import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class history_order_card_item extends StatelessWidget {
  const history_order_card_item({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8.0, 8, 16, 8),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                          child: FadeInImage(
                            fit: BoxFit.fill,
                            placeholder: AssetImage('assets/images/icons/test_img.jpg'),
                            image: NetworkImage(
                                'https://images-na.ssl-images-amazon.com/images/I/71iS7jKcpZL._UY695_.jpg'),
                          )),
                      height: MediaQuery.of(context).size.height * 0.15,
                      width: MediaQuery.of(context).size.height * 0.15,
                      decoration: BoxDecoration(boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.22),
                            blurRadius: 0.7,
                            spreadRadius: 0.7,
                            offset: Offset(0, 2)),
                      ], borderRadius: BorderRadius.all(Radius.circular(15))),
                    )),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      '1',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: Color(0xff282828),
                          fontWeight: FontWeight.w500,
                          fontFamily: 'Grava',
                          fontSize: 15),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0, bottom: 4),
                    child: Text(
                      'x',
                      style: TextStyle(
                          color: Color(0xff282828),
                          fontWeight: FontWeight.w500,
                          fontFamily: 'Grava',
                          fontSize: 15),
                    ),
                  ),
                ),
                Expanded(
                  flex: 7,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 12.0, bottom: 4),
                    child: Text(
                      'Shoes in Red&black',
                      style: TextStyle(
                          color: Color(0xff282828),
                          fontWeight: FontWeight.w500,
                          fontFamily: 'Grava',
                          fontSize: 15),
                    ),
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0, bottom: 4),
                    child: RichText(
                      textAlign: TextAlign.end,
                      text: TextSpan(
                          text: '35.350',
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            fontFamily: 'Grava',
                            color: Color(0xff858585),
                          ),
                          children: [
                            TextSpan(
                              text: ' KWD',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 10,
                                fontFamily: 'Grava',
                                color: Color(0xff858585),
                              ),
                            ),
                          ]),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
