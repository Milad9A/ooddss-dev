import 'package:flutter/material.dart';

import '../../expandable_view.dart';

class HistoryOrderCard extends StatelessWidget {
  final Widget content;

  const HistoryOrderCard({
    @required this.content,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ExpandableView(
      title: '449',
      trailingColor: const Color(0xffff7200),
      trailingIcon: Icon(Icons.access_time),
      date: DateTime.now(),
      trailingText: 'PENDING',
      //leadingIcon: 'icons/shipping_icon.png',
      childWidget: content,
      // Column(
      //   children: <Widget>[
      //     history_order_card_item(),
      //     history_order_card_item(),
      //     history_order_card_item(),
      //     Padding(
      //       padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
      //       child: Row(
      //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //         children: <Widget>[
      //           Expanded(
      //             child: Padding(
      //               padding: const EdgeInsets.only(left: 32.0),
      //               child: Text(
      //                 'Total',
      //                 textAlign: TextAlign.start,
      //                 style: TextStyle(
      //                     fontFamily: 'Grava',
      //                     fontSize: 20,
      //                     color: Color(0xff353535),
      //                     fontWeight: FontWeight.w400),
      //               ),
      //             ),
      //           ),
      //           Expanded(
      //             child: RichText(
      //               textAlign: TextAlign.end,
      //               text: TextSpan(
      //                   text: '225.000',
      //                   style: TextStyle(
      //                     fontWeight: FontWeight.w500,
      //                     fontSize: 20,
      //                     fontFamily: 'Grava',
      //                     color: Colors.black,
      //                   ),
      //                   children: [
      //                     TextSpan(
      //                       text: ' KWD',
      //                       style: TextStyle(
      //                         fontWeight: FontWeight.w500,
      //                         fontSize: 15,
      //                         fontFamily: 'Grava',
      //                         color: Colors.black,
      //                       ),
      //                     ),
      //                   ]),
      //             ),
      //           )
      //         ],
      //       ),
      //     ),
      //     Align(
      //         alignment: Alignment.bottomCenter,
      //         child: SizedBox(
      //             height: 40,
      //             width: double.infinity,
      //             child: RaisedButton(
      //               shape: RoundedRectangleBorder(
      //                   borderRadius: BorderRadius.only(
      //                       bottomRight: Radius.circular(20),
      //                       bottomLeft: Radius.circular(20))),
      //               onPressed: () {},
      //               color: Color(0xff0f6da9),
      //               child: Text(
      //                 'ORDER DETAILS',
      //                 style: TextStyle(
      //                     color: Colors.white,
      //                     fontFamily: 'Grava',
      //                     fontWeight: FontWeight.w500),
      //               ),
      //             )))
      //   ],
    );
    // );
  }
}
