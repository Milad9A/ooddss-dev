import 'package:flutter/material.dart';

import '../utils/constants.dart';

class RoundedEdgesFlatButton extends StatelessWidget {
  final String title;
  final Color color;
  final Color textColor;
  final IconData icon;
  final Color iconColor;
  final Function onPressed;
  final EdgeInsets margin;

  const RoundedEdgesFlatButton({
    @required this.title,
    @required this.onPressed,
    this.color = kAccentColorBlue,
    this.textColor = Colors.white,
    this.icon,
    this.iconColor = Colors.white,
    this.margin = const EdgeInsets.symmetric(horizontal: 10.0),
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(18.0),
        boxShadow: [
          BoxShadow(
            color: color.withOpacity(0.34),
            blurRadius: 3.3,
            offset: Offset(0, 2.3),
          ),
        ],
      ),
      child: MaterialButton(
        padding: EdgeInsets.only(top: 2.0),
        height: 52.0,
        color: color,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        ),
        onPressed: onPressed,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            icon != null
                ? Row(
                    children: [
                      Icon(
                        icon,
                        color: iconColor,
                      ),
                      SizedBox(width: 8.0)
                    ],
                  )
                : SizedBox.shrink(),
            Text(
              title,
              style: TextStyle(
                color: textColor,
                fontSize: 18.0,
                letterSpacing: 1.25,
              ),
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
    );
  }
}
