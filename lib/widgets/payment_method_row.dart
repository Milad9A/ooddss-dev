import 'package:flutter/material.dart';

import 'custom_circular_checkbox.dart' as CustomCircularCheckbox;

class PaymentMethodRow extends StatefulWidget {
  final Function(String) onChanged;

  const PaymentMethodRow({
    this.onChanged,
  });

  @override
  _PaymentMethodRowState createState() => _PaymentMethodRowState();
}

class _PaymentMethodRowState extends State<PaymentMethodRow> {
  var val = true;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Column(
          children: [
            Image(
              height: 60.0,
              image: AssetImage('assets/images/icons/kuwaitnetCard.png'),
            ),
            SizedBox(height: 6.0),
            CustomCircularCheckbox.CircularCheckBox(
              onChanged: (value) {
                widget.onChanged('knet');
                setState(() {
                  val = !val;
                });
              },
              value: val,
            ),
          ],
        ),
        Column(
          children: [
            Image(
              height: 60.0,
              image: AssetImage('assets/images/icons/cash.png'),
            ),
            SizedBox(height: 6.0),
            CustomCircularCheckbox.CircularCheckBox(
              onChanged: (value) {
                widget.onChanged('cash');
                setState(() {
                  val = !val;
                });
              },
              value: !val,
            ),
          ],
        ),
      ],
    );
  }
}
