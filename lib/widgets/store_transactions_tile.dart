import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../models/transaction_model.dart';
import '../utils/constants.dart';

class StoreTransactionsTile extends StatefulWidget {
  final Transaction transaction;

  const StoreTransactionsTile({
    @required this.transaction,
  });

  @override
  _StoreTransactionsTileState createState() => _StoreTransactionsTileState();
}

class _StoreTransactionsTileState extends State<StoreTransactionsTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(
        widget.transaction.type ? 0xFFECF6E7 : 0xFFFBE9ED,
      ),
      margin: EdgeInsets.only(bottom: 1.2),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 12.0,
          vertical: 8.0,
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.baseline,
              children: [
                Row(
                  children: [
                    Text(
                      'Ref   :    ',
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Text(
                      widget.transaction.reference,
                      style: TextStyle(
                        color: kOODDSSGrey,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  children: [
                    Image(
                      height: 17.0,
                      image: AssetImage(
                        widget.transaction.type
                            ? 'assets/images/icons/arrow_up_green.png'
                            : 'assets/images/icons/arrow_down_red.png',
                      ),
                    ),
                    SizedBox(width: 2.5),
                    Padding(
                      padding: EdgeInsets.only(top: 4.7),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.baseline,
                        children: [
                          Text(
                            widget.transaction.amount,
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Color(
                                widget.transaction.type
                                    ? 0xFF188C04
                                    : 0xFFDC264D,
                              ),
                            ),
                          ),
                          SizedBox(width: 1.0),
                          Text(
                            widget.transaction.currency,
                            style: TextStyle(
                              fontSize: 7.0,
                              fontWeight: FontWeight.w600,
                              color: Color(
                                widget.transaction.type
                                    ? 0xFF188C04
                                    : 0xFFDC264D,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 7.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image(
                  height: 20.0,
                  image: AssetImage(
                    widget.transaction.type
                        ? 'assets/images/icons/kuwaitnetCard.png'
                        : 'assets/images/icons/outlinedCard.png',
                  ),
                ),
                Text(
                  DateFormat.yMd()
                      .format(widget.transaction.dateTime)
                      .toString(),
                  style: TextStyle(
                    fontSize: 13.0,
                    color: kOODDSSGrey,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
