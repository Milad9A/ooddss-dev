import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class WalletRow extends StatelessWidget {
  final String title;
  final String amount;

  const WalletRow({
    @required this.title,
    @required this.amount,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          width: MediaQuery.of(context).size.width / 1.5,
          child: AutoSizeText(
            title,
            maxLines: 1,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(right: 12.0),
          alignment: Alignment.centerRight,
          // width: 80.0,
          // height: 40.0,
          height: MediaQuery.of(context).size.height * 0.06,
          width: MediaQuery.of(context).size.width * 0.2,
          decoration: BoxDecoration(
            border: Border.all(
              width: 0.3,
              color: Colors.grey,
            ),
            borderRadius: BorderRadius.circular(14.0),
            color: Color(0xFFEBECEE),
          ),
          child: Text(
            amount,
            maxLines: 1,
            style: TextStyle(
              letterSpacing: 0.2,
            ),
          ),
        ),
      ],
    );
  }
}
