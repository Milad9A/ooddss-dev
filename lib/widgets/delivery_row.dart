import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class DeliveryRow extends StatelessWidget {
  const DeliveryRow({
    @required this.deliveryCompany,
    @required this.deliveryETA,
  });

  final String deliveryCompany;
  final String deliveryETA;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: MediaQuery.of(context).size.width / 2.5,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              // width: 80.0,
              width: MediaQuery.of(context).size.width * 0.19,
              margin: EdgeInsets.only(left: 10.0),
              padding: EdgeInsets.symmetric(
                horizontal: 18.0,
                vertical: 10.0,
              ),
              decoration: BoxDecoration(
                color: Color(0xFF0F6DA9).withOpacity(0.15),
                borderRadius: BorderRadius.circular(9.0),
              ),
              child: Image(
                height: 20.0,
                image: AssetImage(
                  'assets/images/icons/truck_blue.png',
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 100.0,
                      child: AutoSizeText(
                        deliveryCompany,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Container(
                      width: 100.0,
                      child: Text(
                        'Delivery in $deliveryETA days',
                        style: TextStyle(
                          fontSize: 11.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 4,
                child: Row(
                  children: [
                    Text('5.000'),
                    Text(
                      'KWD',
                      style: TextStyle(
                        fontSize: 7.0,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
