import 'package:flutter/material.dart';
import '../models/order_model.dart';

import 'expandable_view.dart';
import 'order_card_tile.dart';

class OrderCard extends StatelessWidget {
  final Order order;

  final String deliveryFee;

  const OrderCard({
    @required this.order,
    this.deliveryFee,
  });

  @override
  Widget build(BuildContext context) {
    return ExpandableView(
      title: order.number.toString(),
      trailingColor: order.getStatusColor(),
      trailingIcon: Image(
        height: 18.0,
        image: AssetImage(order.getStatusImageURL()),
      ),
      trailingText: order.status,
      date: order.dateTime,
      childWidget: Column(
        children: <Widget>[
          Column(
            children: order.products
                .map((product) => OrderCardTile(product: product))
                .toList(),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(8.0, 8, 16, 8),
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: 75.0,
                    padding: EdgeInsets.symmetric(
                      horizontal: 18.0,
                      vertical: 10.0,
                    ),
                    decoration: BoxDecoration(
                      color: Color(0xFF0F6DA9).withOpacity(0.15),
                      borderRadius: BorderRadius.circular(9.0),
                    ),
                    child: Image(
                      height: 20.0,
                      image: AssetImage(
                        'assets/images/icons/truck_blue.png',
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      '',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: Color(0xff282828),
                          fontWeight: FontWeight.w500,
                          fontFamily: 'Grava',
                          fontSize: 15),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0, bottom: 4),
                    child: Text(
                      '',
                      style: TextStyle(
                        color: Color(0xff282828),
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Grava',
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 7,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 12.0, bottom: 4),
                    child: Text(
                      'Delivery',
                      style: TextStyle(
                        color: Color(0xff282828),
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Grava',
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0, bottom: 4),
                    child: RichText(
                      textAlign: TextAlign.end,
                      text: TextSpan(
                        text: '05.000',
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 15,
                          fontFamily: 'Grava',
                          color: Color(0xff858585),
                        ),
                        children: [
                          TextSpan(
                            text: ' KWD',
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 8.0,
                              fontFamily: 'Grava',
                              color: Color(0xff858585),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      'Total  :',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontFamily: 'Grava',
                        fontSize: 20,
                        color: Color(0xff353535),
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: RichText(
                    textAlign: TextAlign.end,
                    text: TextSpan(
                      text: '225.000',
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 20,
                        fontFamily: 'Grava',
                        color: Colors.black,
                      ),
                      children: [
                        TextSpan(
                          text: ' KWD',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 12.0,
                            fontFamily: 'Grava',
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: SizedBox(
              height: 40,
              width: double.infinity,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(20),
                    bottomLeft: Radius.circular(20),
                  ),
                ),
                onPressed: () {},
                color: Color(0xff0f6da9),
                child: Text(
                  'ORDER DETAILS',
                  style: TextStyle(
                    letterSpacing: 1.0,
                    color: Colors.white,
                    fontSize: 15.0,
                    fontFamily: 'Grava',
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
