import 'package:flutter/material.dart';

import '../custom_dropdown_button.dart';
import '../custom_text_field.dart';
import '../rounded_edges_flat_button.dart';

class AdvancedSearchModal extends StatefulWidget {
  @override
  _AdvancedSearchModalState createState() => _AdvancedSearchModalState();
}

class _AdvancedSearchModalState extends State<AdvancedSearchModal> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[100],
      child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.only(left: 22, right: 22),
        children: <Widget>[
          Row(
            children: [
              Text(
                'ADVANCED SEARCH',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
          SizedBox(
            height: 16,
          ),
          CustomTextFields(
            TextEditingController(),
            padding: EdgeInsets.all(0),
            hintText: "Order No",
            validateFunction: (val) {
              return null;
            },
          ),
          SizedBox(
            height: 16,
          ),
          CustomTextFields(
            TextEditingController(),
            padding: EdgeInsets.all(0),
            hintText: "SKU",
            validateFunction: (val) {
              return null;
            },
          ),
          SizedBox(
            height: 16,
          ),
          CustomTextFields(
            TextEditingController(),
            padding: EdgeInsets.all(0),
            hintText: "Product Name",
            validateFunction: (val) {
              return null;
            },
          ),
          SizedBox(
            height: 16,
          ),
          CustomTextFields(
            TextEditingController(),
            padding: EdgeInsets.all(0),
            hintText: "Customer Name",
            validateFunction: (val) {
              return null;
            },
          ),
          SizedBox(
            height: 16,
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: CustomDropDownButton(
                  onChange: (val) {
                    print(val);
                  },
                  title: "From",
                  items: ["8-10-2020", "8-10-2019"],
                  selectedItem: "8-10-2019",
                  icon: "assets/images/icons/date.png",
                ),
              ),
              SizedBox(
                width: 8,
              ),
              Expanded(
                child: CustomDropDownButton(
                  onChange: (val) {
                    print(val);
                  },
                  title: "To",
                  items: ["8-10-2020", "8-10-2019"],
                  icon: "assets/images/icons/date.png",
                  selectedItem: "8-10-2020",
                ),
              ),
            ],
          ),
          SizedBox(
            height: 26,
          ),
          RoundedEdgesFlatButton(title: "GO", onPressed: () {}),
          SizedBox(
            height: 26,
          ),
        ],
      ),
    );
  }
}
