import 'package:flutter/material.dart';

import '../../utils/colors_repository.dart' as repo;

class SortByModal extends StatefulWidget {
  @override
  _SortByModalState createState() => _SortByModalState();
}

class _SortByModalState extends State<SortByModal> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    createSortItem(String title, String icon) => GestureDetector(
          onTap: () {
            print(title);
            Navigator.of(context).pop();
          },
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset(
                  'assets/images/icons/$icon.png',
                  width: width / 12,
                  height: width / 12,
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  title,
                  style: TextStyle(
                      color: repo.grey1,
                      fontWeight:
                          title == "Oldest" ? FontWeight.bold : FontWeight.w500,
                      fontSize: 13.4),
                )
              ],
            ),
          ),
        );

    return Container(
      color: Colors.grey[100],
      child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.only(left: 22, right: 22),
        children: <Widget>[
          Row(
            children: [
              Text(
                'SORT BY',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
          SizedBox(
            height: 22,
          ),
          Container(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(child: createSortItem("New", "ico_new")),
                Expanded(child: createSortItem("Oldest", "ico_oldest")),
                SizedBox(
                  width: 6,
                ),
                Expanded(child: createSortItem("Low To Hight", "low_to_hight")),
                SizedBox(
                  width: 16,
                ),
                Expanded(child: createSortItem("Hight to Low", "hight_to_low")),
              ],
            ),
          ),
          SizedBox(
            height: 32,
          ),
        ],
      ),
    );
  }
}
