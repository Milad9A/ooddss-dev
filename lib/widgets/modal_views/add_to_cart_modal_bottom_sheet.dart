import 'package:flutter/material.dart';

import '../colors_selecter.dart';
import '../items_number_button.dart';
import '../rounded_edges_flat_button.dart';

class AddToCartModal extends StatefulWidget {
  @override
  _AddToCartModalState createState() => _AddToCartModalState();
}

class _AddToCartModalState extends State<AddToCartModal> {
  List<Color> colorSelected;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      color: Colors.grey[100],
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(left: 22, right: 22),
          child: Column(
            children: <Widget>[
              Image.asset(
                "assets/images/icons/shose_test.png",
                width: width / 2.5,
                height: width / 2.5,
              ),
              SizedBox(
                height: 22,
              ),
              Text(
                'Shose in Red&Black',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 16,
              ),
              Text(
                '35.000 KWD',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.grey,
                    decoration: TextDecoration.lineThrough,
                    fontSize: 13,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 2,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    child: Text(
                      '25.95',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 17,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    width: 2,
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 2.0, right: 2.0, bottom: 2),
                    child: Text(
                      'KWD',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 11,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 2.0, right: 2.0, bottom: 2),
                    child: Text(
                      'Color: ',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    width: 2,
                  ),
                  Flexible(
                    child: Text(
                      'RED & BLACK',
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 12,
              ),
              ColorsSelecter(
                colors: [
                  [Colors.red, Colors.black],
                  [Colors.black],
                  [Colors.purpleAccent],
                  [Colors.lightGreenAccent],
                ],
                enableSelected: true,
                onSelected: (colors) {
                  print(colors.length);
//              setState(() {
//                colorSelected = colors;
//              });
                },
              ),
              SizedBox(
                height: 28,
              ),
              Container(
                width: width / 3,
                child: ItemsNumberButton(
                  onChanged: (val) {
                    print(val);
                  },
                ),
              ),
              SizedBox(
                height: 32,
              ),
              RoundedEdgesFlatButton(title: "ADD TO CART", onPressed: () {}),
              SizedBox(
                height: 22,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
