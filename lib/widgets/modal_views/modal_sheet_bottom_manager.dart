import 'package:flutter/material.dart';

Future<dynamic> showMyBottomSheet(BuildContext mainContext,
    {@required Widget page}) async {
  return showModalBottomSheet<dynamic>(
    shape: OutlineInputBorder(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16.0), topRight: Radius.circular(16.0)),
    ),
    backgroundColor: Colors.grey[100],
    context: mainContext,
    isScrollControlled: true,
    builder: (BuildContext context) {
      return Wrap(
        children: [
          Container(
            height: 42,
            padding: EdgeInsets.only(left: 6, right: 6, top: 4),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16.0),
                  topRight: Radius.circular(16.0)),
              color: Colors.grey[100],
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                  icon: Image.asset(
                    'assets/images/icons/minus.png',
                    width: 26,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ),
          page
        ],
      );
    },
  );
}
