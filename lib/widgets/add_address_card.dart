import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';

class AddAddressCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.fromLTRB(16.0, 10.0, 16.0, 24.0),
        padding: EdgeInsets.all(0.5),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(18.0),
        ),
        child: DottedBorder(
          strokeWidth: 1.2,
          padding: EdgeInsets.all(0),
          dashPattern: [8],
          color: Colors.grey,
          borderType: BorderType.RRect,
          radius: Radius.circular(18.0),
          child: Container(
            height: 125.0,
            padding: EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 16.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(18.0),
            ),
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.add_outlined,
                    color: Colors.grey,
                    size: 40.0,
                  ),
                  Text(
                    'ADD NEW ADDRESS',
                    style: TextStyle(
                      fontSize: 15.0,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
