import 'package:flutter/material.dart';

import '../utils/colors_repository.dart' as repo;

class CustomTextFields extends StatelessWidget {
  final TextEditingController _controller;
  final String hintText;
  final int maxLines;
  final TextInputType keyboardType;
  final Function validateFunction;
  final EdgeInsetsGeometry padding;
  final bool whiteFillColor;
  final Widget suffixWidget;

  CustomTextFields(this._controller,
      {this.hintText = "",
      this.maxLines = 1,
      this.keyboardType = TextInputType.text,
      this.whiteFillColor = true,
      this.suffixWidget,
      @required this.padding,
      this.validateFunction})
      : assert(_controller != null,
            "You should pass a controller to CustomTextFields"),
        assert(padding != null, "Padding can't be null");

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Material(
        elevation: 0.1,
        shadowColor: Colors.grey[100],
        color: whiteFillColor ? Colors.white : Colors.grey[200],
        borderRadius: BorderRadius.all(Radius.circular(18)),
        child: new TextFormField(
          controller: _controller,
          keyboardType: keyboardType,
          scrollPadding: EdgeInsets.all(32),
          decoration: new InputDecoration(
            hintText: hintText,
            suffixIcon: suffixWidget,
            errorMaxLines: 3,
            border: OutlineInputBorder(
              borderSide: BorderSide(
                  style: BorderStyle.solid,
                  width: 0.5,
                  color: Colors.grey[400]),
              borderRadius: BorderRadius.all(Radius.circular(18)),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  style: BorderStyle.solid,
                  width: 0.5,
                  color: Colors.grey[400]),
              borderRadius: BorderRadius.all(Radius.circular(18)),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  style: BorderStyle.solid,
                  width: 0.6,
                  color: Colors.grey[500]),
              borderRadius: BorderRadius.all(Radius.circular(18)),
            ),
            errorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  style: BorderStyle.solid, width: 0.6, color: Colors.red),
              borderRadius: BorderRadius.all(Radius.circular(18)),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  style: BorderStyle.solid, width: 0.6, color: Colors.red),
              borderRadius: BorderRadius.all(Radius.circular(18)),
            ),
            hintStyle: TextStyle(
              color: Colors.grey[500],
              fontSize: 16,
            ),
            contentPadding: EdgeInsets.fromLTRB(
                18.0, maxLines > 1 ? 16 : 0.0, 18.0, maxLines > 1 ? 16 : 0.0),
          ),
          cursorColor: repo.grey1,
          maxLines: maxLines,
          minLines: maxLines,
          textAlignVertical: TextAlignVertical(y: 0.09),
          style: TextStyle(
            color: repo.grey1,
            fontSize: 16,
          ),
          validator: (value) => validateFunction(value),
        ),
      ),
    );
  }
}
