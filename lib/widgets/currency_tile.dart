import 'package:flutter/material.dart';

import 'custom_circular_checkbox.dart' as CustomCircularCheckbox;

class CurrencyTile extends StatefulWidget {
  final String imageURL;
  final String currencyName;
  final bool lastTile;
  final Function onChanged;

  const CurrencyTile({
    @required this.imageURL,
    @required this.currencyName,
    @required this.lastTile,
    @required this.onChanged,
  });

  @override
  _CurrencyTileState createState() => _CurrencyTileState();
}

class _CurrencyTileState extends State<CurrencyTile> {
  var val = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          title: Text('${widget.currencyName}'),
          leading: Image.asset(
            widget.imageURL,
            width: 45.0,
          ),
          trailing: CustomCircularCheckbox.CircularCheckBox(
            value: val,
            onChanged: (bool value) {
              setState(() {
                val = !val;
              });
              widget.onChanged(value);
            },
          ),
        ),
        !widget.lastTile
            ? Divider(
                indent: 15.0,
                endIndent: 25.0,
                color: Colors.grey,
                thickness: 0.4,
              )
            : SizedBox.shrink(),
      ],
    );
  }
}
