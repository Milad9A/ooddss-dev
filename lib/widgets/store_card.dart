import 'package:flutter/material.dart';

class StoreCard extends StatelessWidget {
  final Function onTapShopping;
  final Function onTapHeart;
  final String imageURL;
  final String currencyName;
  final String price;
  final String oldPriceAndCurrency;

  const StoreCard({
    @required this.onTapShopping,
    @required this.onTapHeart,
    @required this.imageURL,
    @required this.currencyName,
    @required this.price,
    this.oldPriceAndCurrency,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
      decoration: BoxDecoration(
        color: Color(0xFFF5F5F5),
        borderRadius: BorderRadius.circular(18.0),
        boxShadow: [
          BoxShadow(
            offset: Offset(-2.0, 4.0),
            color: Colors.black26,
            blurRadius: 15.0,
          ),
        ],
      ),
      child: Container(
        padding: EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 22.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 35.0),
            Container(
              height: 180.0,
              child: Image(
                height: 180.0,
                image: AssetImage(
                  imageURL,
                ),
              ),
            ),
            SizedBox(height: 35.0),
            Text(
              'Shoes in Red&Black',
              style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(height: 20.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: GestureDetector(
                    onTap: onTapShopping,
                    child: Image(
                      height: 22.0,
                      image: AssetImage(
                        'assets/images/icons/blank_heart.png',
                      ),
                    ),
                  ),
                ),
                Column(
                  children: [
                    !(oldPriceAndCurrency == null)
                        ? Text(
                            oldPriceAndCurrency,
                            style: TextStyle(
                              fontSize: 11.5,
                              decoration: TextDecoration.lineThrough,
                            ),
                          )
                        : SizedBox.shrink(),
                    SizedBox(
                      height: 4.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          price,
                          style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(width: 1.5),
                        Column(
                          children: [
                            Text(
                              currencyName,
                              style: TextStyle(
                                fontSize: 11.0,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            SizedBox(height: 2.5),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: GestureDetector(
                    onTap: onTapHeart,
                    child: Image(
                      height: 22.0,
                      image: AssetImage(
                        'assets/images/icons/add_to_cart_icon.png',
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
