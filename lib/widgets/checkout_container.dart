import 'package:flutter/material.dart';

import 'rounded_edges_flat_button.dart';

class CheckoutContainer extends StatelessWidget {
  const CheckoutContainer({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 24.0,
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      alignment: Alignment.centerRight,
                      width: 75.0,
                      child: Text(
                        'Subtotal :',
                        style: TextStyle(
                          fontSize: 14.0,
                        ),
                      ),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.baseline,
                      children: [
                        Text(
                          '35.500',
                          style: TextStyle(
                            fontSize: 17.0,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(width: 2.0),
                        Text(
                          'KWD',
                          style: TextStyle(
                            fontSize: 11.0,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 5.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      alignment: Alignment.centerRight,
                      width: 75.0,
                      child: Text(
                        'Discount :',
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Color(0xFFDC264D),
                        ),
                      ),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.baseline,
                      children: [
                        Text(
                          '5.500',
                          style: TextStyle(
                            color: Color(0xFFDC264D),
                            fontSize: 17.0,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(width: 2.0),
                        Text(
                          'KWD',
                          style: TextStyle(
                            color: Color(0xFFDC264D),
                            fontSize: 11.0,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          Divider(
            indent: 12.0,
            endIndent: 12.0,
            thickness: 1.2,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  alignment: Alignment.centerRight,
                  width: 75.0,
                  child: Text(
                    'Total :',
                    style: TextStyle(
                      fontSize: 17.0,
                    ),
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  children: [
                    Text(
                      '30.000',
                      style: TextStyle(
                        fontSize: 21.0,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(width: 2.0),
                    Text(
                      'KWD',
                      style: TextStyle(
                        fontSize: 11.0,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 15.0,
          ),
          RoundedEdgesFlatButton(
            title: 'CHECKOUT',
            onPressed: () {},
          ),
          SizedBox(height: 12.0),
        ],
      ),
    );
  }
}
