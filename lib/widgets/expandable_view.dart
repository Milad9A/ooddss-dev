import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';

import 'my_expansion_tile.dart';

class ExpandableView extends StatelessWidget {
  final GlobalKey titleTextKey = new GlobalKey();

  ExpandableView(
      {Key key,
      @required this.title,
      @required this.childWidget,
      this.leadingIconPath,
      this.trailingText,
      this.trailingIcon,
      this.date,
      this.trailingColor})
      : assert(title != null),
        assert(childWidget != null),
        assert(
          (trailingIcon != null &&
                  trailingText != null &&
                  date != null &&
                  trailingColor != null) ||
              (trailingIcon == null && trailingText == null && date == null),
        ),
        super(key: key);

  final Widget childWidget;
  final String title;
  String leadingIconPath;
  Widget trailingIcon;
  DateTime date;
  String trailingText;
  Color trailingColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: MyExpansionTile(
        onExpansionChanged: (expanded) {},
        title: leadingIconPath != null
            ? Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Image.asset(
                    leadingIconPath,
                    height: 26,
                    width: 24.5,
                  ),
                  SizedBox(
                    height: 1,
                    width: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text(
                      title,
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.black,
                        fontFamily: 'Grava',
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                ],
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    '#' + title,
                    style: TextStyle(
                      fontFamily: 'Grave',
                      fontSize: 17,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
        trailing: trailingIcon != null
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  FittedBox(
                    fit: BoxFit.fill,
                    child: Text(
                      DateFormat.yMMMMd().format(date).toString(),
                      style: TextStyle(
                        fontFamily: 'Grava',
                        fontSize: 13,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      trailingIcon,
                      SizedBox(
                        width: 10,
                        height: 1,
                      ),
                      Text(
                        trailingText,
                        style: TextStyle(
                          fontSize: 15,
                          fontFamily: 'Grava',
                          fontWeight: FontWeight.w400,
                          color: trailingColor,
                        ),
                      ),
                    ],
                  ),
                  //trailing: trailing ?? trailing,
                ],
              )
            : null,
        children: <Widget>[childWidget],
      ),
    );
  }
}
