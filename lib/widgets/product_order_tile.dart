import 'package:flutter/material.dart';
import '../models/product_model.dart';

class ProductOrderTile extends StatelessWidget {
  final Product product;
  final int index;
  final String deliveryCompany;
  final String deliveryETA;
  final Widget child;
  final bool hasImage;

  const ProductOrderTile({
    @required this.product,
    this.index,
    this.deliveryCompany,
    this.deliveryETA,
    this.child,
    this.hasImage = true,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 2.5,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    !hasImage
                        ? Container(
                            padding: EdgeInsets.fromLTRB(10.0, 4.0, 4.0, 4.0),
                            margin: EdgeInsets.only(left: 12.0),
                            // height: 72.0,
                            // width: 72.0,
                            height: MediaQuery.of(context).size.width * 0.18,
                            width: MediaQuery.of(context).size.width * 0.18,
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                '#${index + 1}',
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ),
                          )
                        : Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(14.0),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  offset: Offset(2.0, 2.0),
                                  color: Colors.black26,
                                ),
                                BoxShadow(
                                  offset: Offset(-2.0, 2.0),
                                  color: Colors.black12,
                                ),
                              ],
                            ),
                            padding: EdgeInsets.all(4.0),
                            margin: EdgeInsets.only(left: 12.0),
                            // height: 72.0,
                            // width: 72.0,
                            height: MediaQuery.of(context).size.width * 0.18,
                            width: MediaQuery.of(context).size.width * 0.18,
                            child: Image(
                              image: AssetImage(
                                product.productImageURL,
                              ),
                            ),
                          ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Row(
                        children: [
                          Text(
                            '1',
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          SizedBox(width: 10.0),
                          Text(
                            '×',
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.18,
                      child: Text(
                        product.productName,
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerRight,
                      width: MediaQuery.of(context).size.width / 4,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.baseline,
                        children: [
                          Text(
                            product.productPrice,
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              color: Color(0xFF808080),
                            ),
                          ),
                          Text(
                            product.productPriceCurrency,
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              color: Color(0xFF808080),
                              fontWeight: FontWeight.w500,
                              fontSize: 7.5,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Row(
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 2.5,
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Store',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(height: 4.0),
                        Text(
                          'Color',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(height: 4.0),
                        Text(
                          'Size',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(':'),
                        SizedBox(height: 4.0),
                        Text(':'),
                        SizedBox(height: 4.0),
                        Text(':'),
                      ],
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 4,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'NIKE',
                            style: TextStyle(
                              color: Color(0xFF808080),
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(height: 4.0),
                          Text(
                            'BLUE',
                            style: TextStyle(
                              color: Color(0xFF808080),
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(height: 4.0),
                          Text(
                            '42',
                            style: TextStyle(
                              color: Color(0xFF808080),
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          child != null
              ? Container(
                  child: child,
                )
              : SizedBox.shrink(),
          index != products.length - 1
              ? Divider(
                  thickness: 1.0,
                  indent: 16.0,
                  endIndent: 16.0,
                )
              : SizedBox.shrink(),
        ],
      ),
    );
  }
}
