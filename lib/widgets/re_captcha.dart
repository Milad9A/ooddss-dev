import 'package:flutter/material.dart';
import 'package:flutter_recaptcha_v2/flutter_recaptcha_v2.dart';

class Recpatcha extends StatefulWidget {
  @override
  _RecpatchaState createState() => _RecpatchaState();
}

class _RecpatchaState extends State<Recpatcha> {
  String verifyResult = "";
  RecaptchaV2Controller recaptchaV2Controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    recaptchaV2Controller = RecaptchaV2Controller();
    recaptchaV2Controller.show();

  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: RecaptchaV2(
        apiKey: "6LeCwZYUAAAAAJo8IVvGX9dH65Rw89vxaxErCeou",
        apiSecret: "6LeCwZYUAAAAAKGahIjwfOARevvRETgvwhPMKCs_",
        controller: recaptchaV2Controller,
        onVerifiedError: (err){
          print(err);
        },
        onVerifiedSuccessfully: (success) {
          setState(() {
            if (success) {
              verifyResult = "You've been verified successfully.";
            } else {
              verifyResult = "Failed to verify.";
            }
          });
        },
      ),
    );
  }
}
