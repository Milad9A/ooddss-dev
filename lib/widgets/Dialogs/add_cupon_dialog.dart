import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../custom_text_field.dart';
import '../rounded_edges_flat_button.dart';

class AddCuponDialog extends StatefulWidget {
  @override
  _AddCuponDialogState createState() => _AddCuponDialogState();
}

class _AddCuponDialogState extends State<AddCuponDialog> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height;
    if (MediaQuery.of(context).size.aspectRatio >= 1.0) {
      height = width * MediaQuery.of(context).size.aspectRatio;
    } else {
      height = width / MediaQuery.of(context).size.aspectRatio;
    }

    return Container(
      color: Colors.grey[100],
      width: width,
      child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.only(top: 0, left: 12, right: 12),
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'ADD CUPON',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
          CustomTextFields(
            TextEditingController(),
            padding: EdgeInsets.only(top: 18, bottom: 28),
            hintText: "Please enter a valid coupon",
            validateFunction: (val) {
              return null;
            },
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: RoundedEdgesFlatButton(
                    title: "BACK",
                    color: const Color(0XFF485258),
                    onPressed: () {}),
              ),
              Expanded(
                child: RoundedEdgesFlatButton(
                  title: "APPLY",
                  onPressed: () {},
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
