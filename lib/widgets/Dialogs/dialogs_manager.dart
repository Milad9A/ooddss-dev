import 'package:flutter/material.dart';

Future<dynamic> showAlert(
  mainContext, {
  bool barrierDismissible = false,
  isBuilderChild = true,
  page,
}) async {
  return isBuilderChild == true
      ? showDialog<dynamic>(
          context: mainContext,
          barrierDismissible:
              barrierDismissible,
          builder: (BuildContext context) {
            return CustomDialog(
              page: page,
            );
          },
        )
      : showDialog<dynamic>(
          context: mainContext,
          barrierDismissible: barrierDismissible,
          child: page);
}

class CustomDialog extends StatelessWidget {
  final Widget page;

  CustomDialog({@required this.page});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.grey[100],
      insetPadding: EdgeInsets.only(left: 22, right: 22),
      shape: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(16.0)),
          borderSide: BorderSide.none),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          IconButton(
            icon: Image.asset(
              'assets/images/icons/minus.png',
              width: 26,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      titlePadding: EdgeInsets.only(left: 7, right: 7, bottom: 4),
      content: page,
      contentPadding: EdgeInsets.only(bottom: 26.0,),
    );
  }
}

