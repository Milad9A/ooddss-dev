import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../utils/colors_repository.dart' as repo;
import '../custom_dropdown_button.dart';
import '../rounded_edges_flat_button.dart';

class AddOrderDetailsAdminDialog extends StatefulWidget {
  @override
  _AddOrderDetailsAdminDialogState createState() =>
      _AddOrderDetailsAdminDialogState();
}

class _AddOrderDetailsAdminDialogState
    extends State<AddOrderDetailsAdminDialog> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height;
    if (MediaQuery.of(context).size.aspectRatio >= 1.0) {
      height = width * MediaQuery.of(context).size.aspectRatio;
    } else {
      height = width / MediaQuery.of(context).size.aspectRatio;
    }

    return Container(
      color: Colors.grey[100],
      width: width,
      child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.only(top: 0, left: 12, right: 12),
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                '#4479',
                style: TextStyle(
                    color: repo.grey1,
                    fontSize: 15.0,
                    fontWeight: FontWeight.w500),
              ),
              SizedBox(
                height: 4,
              ),
              Text(
                'STATUS UPDATE',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
          SizedBox(
            height: 16,
          ),
          Padding(
              padding: EdgeInsets.only(left: 22, right: 22),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Image.asset(
                    'assets/images/icons/status.png',
                    width: 20,
                    height: 20,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text(
                    'STATUS',
                    style: TextStyle(
                        color: repo.grey2,
                        fontSize: 14.5,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              )),
          SizedBox(
            height: 3,
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: CustomDropDownButton(
                  onChange: (val) {
                    print(val);
                  },
                  items: [
                    "One Option",
                    "Two Option",
                    "Three Option",
                    "Four Option"
                  ],
                  selectedItem: "Complain",
                  title: "Select Option",
                ),
              ),
            ],
          ),
          SizedBox(
            height: 24,
          ),
          Padding(
              padding: EdgeInsets.only(left: 22, right: 22),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Image.asset(
                    'assets/images/icons/question.png',
                    width: 16,
                    height: 16,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text(
                    'REASON',
                    style: TextStyle(
                        color: repo.grey2,
                        fontSize: 14.5,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              )),
          SizedBox(
            height: 3,
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: CustomDropDownButton(
                  title: "Select Size",
                  onChange: (val) {
                    print(val);
                  },
                  items: ["22 - 32", "23 - 34", "25 - 36", "27 - 38"],
                  selectedItem: "Wrong Size",
                ),
              ),
            ],
          ),
          SizedBox(
            height: 32,
          ),
          RoundedEdgesFlatButton(title: "UPDATE", onPressed: () {}),
        ],
      ),
    );
  }
}

class Test extends StatefulWidget {
  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<Test> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height;
    if (MediaQuery.of(context).size.aspectRatio >= 1.0) {
      height = width * MediaQuery.of(context).size.aspectRatio;
    } else {
      height = width / MediaQuery.of(context).size.aspectRatio;
    }

    return Material(
        color: Colors.black38.withOpacity(0.3),
        child: ListView(
          children: [
            Container(
              child: Stack(
                alignment: Alignment.topCenter,
                children: [
                  Center(
                    child: Container(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.aspectRatio >= 1.0
                              ? width / 8
                              : width / 3.6,
                          left: 16,
                          right: 16),
                      child: Card(
                        color: Colors.grey[100],
                        shape: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(16.0)),
                            borderSide: BorderSide.none),
                        child: Container(
                            width: width,
                            decoration: BoxDecoration(
                              color: Colors.grey[100],
                              borderRadius:
                                  BorderRadius.all(Radius.circular(16.0)),
                            ),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                // title
                                Container(
                                  padding: EdgeInsets.only(left: 4, right: 4),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      IconButton(
                                        icon: Image.asset(
                                          'assets/images/icons/minus.png',
                                          width: 26,
                                        ),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(
                                      top: 0, left: 12, right: 12, bottom: 26),
                                  child: Column(
//                                  shrinkWrap: true,
//                                  padding: EdgeInsets.only(top: 0, left: 12, right: 12, bottom: 26),
                                    children: <Widget>[
                                      SizedBox(
                                        height: width / 8 + 12,
                                      ),
                                      Padding(
                                          padding: EdgeInsets.only(
                                              left: 22, right: 22),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Image.asset(
                                                'assets/images/icons/status.png',
                                                width: 20,
                                                height: 20,
                                              ),
                                              SizedBox(
                                                width: 8,
                                              ),
                                              Text(
                                                'STATUS',
                                                style: TextStyle(
                                                    color: repo.grey2,
                                                    fontSize: 14.5,
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                            ],
                                          )),
                                      SizedBox(
                                        height: 3,
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: CustomDropDownButton(
                                              onChange: (val) {
                                                print(val);
                                              },
                                              items: [
                                                "One Option",
                                                "Two Option",
                                                "Three Option",
                                                "Four Option"
                                              ],
                                              selectedItem: "Complain",
                                              title: "Select Option",
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 24,
                                      ),
                                      Padding(
                                          padding: EdgeInsets.only(
                                              left: 22, right: 22),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Image.asset(
                                                'assets/images/icons/question.png',
                                                width: 16,
                                                height: 16,
                                              ),
                                              SizedBox(
                                                width: 8,
                                              ),
                                              Text(
                                                'REASON',
                                                style: TextStyle(
                                                    color: repo.grey2,
                                                    fontSize: 14.5,
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                            ],
                                          )),
                                      SizedBox(
                                        height: 3,
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: CustomDropDownButton(
                                              title: "Select Size",
                                              onChange: (val) {
                                                print(val);
                                              },
                                              items: [
                                                "22 - 32",
                                                "23 - 34",
                                                "25 - 36",
                                                "27 - 38"
                                              ],
                                              selectedItem: "Wrong Size",
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 32,
                                      ),
                                      RoundedEdgesFlatButton(
                                          title: "UPDATE", onPressed: () {}),
                                    ],
                                  ),
                                ),
                              ],
                            )),
                      ),
                    ),
                  ),
                  Positioned(
                    top: MediaQuery.of(context).size.aspectRatio >= 1.0
                        ? width / 32
                        : width / 6.0,
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Card(
                            color: Colors.white,
                            elevation: 2,
                            shape: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(22),
                              ),
                              borderSide: BorderSide.none,
                            ),
                            child: Container(
                              width: width / 5.0,
                              height: width / 5.0,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(22),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            '#4479',
                            style: TextStyle(
                                color: repo.grey1,
                                fontSize: 15.0,
                                fontFamily: "Grava",
                                fontWeight: FontWeight.w500),
                          ),
                          SizedBox(
                            height: 4,
                          ),
                          Text(
                            'STATUS UPDATE',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}
