import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../models/product_model.dart';

class OrderCardTile extends StatelessWidget {
  final Product product;

  const OrderCardTile({
    @required this.product,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 8, 16, 8),
      child: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 75.0,
              width: 75.0,
              padding: EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.22),
                    blurRadius: 0.7,
                    spreadRadius: 0.7,
                    offset: Offset(0, 2),
                  ),
                ],
                borderRadius: BorderRadius.all(
                  Radius.circular(15),
                ),
              ),
              child: FadeInImage(
                fit: BoxFit.fill,
                placeholder: AssetImage(
                  'assets/images/icons/test_img.jpg',
                ),
                image: AssetImage(
                  product.productImageURL,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text(
                '1',
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: Color(0xff282828),
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Grava',
                    fontSize: 15),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0, bottom: 4),
              child: Text(
                'x',
                style: TextStyle(
                  color: Color(0xff282828),
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Grava',
                  fontSize: 15,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 7,
            child: Padding(
              padding: const EdgeInsets.only(left: 12.0, bottom: 4),
              child: Text(
                product.productName,
                style: TextStyle(
                  color: Color(0xff282828),
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Grava',
                  fontSize: 15,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 5,
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0, bottom: 4),
              child: RichText(
                textAlign: TextAlign.end,
                text: TextSpan(
                  text: product.productPrice,
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 15,
                    fontFamily: 'Grava',
                    color: Color(0xff858585),
                  ),
                  children: [
                    TextSpan(
                      text: ' KWD',
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 8.0,
                        fontFamily: 'Grava',
                        color: Color(0xff858585),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
