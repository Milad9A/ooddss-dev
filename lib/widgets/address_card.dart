import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddressCard extends StatelessWidget {
  final String title;

  final String phoneNumber;

  final String address;

  const AddressCard({
    @required this.title,
    @required this.phoneNumber,
    @required this.address,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
      ),
      margin: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
      elevation: 4.0,
      child: Container(
        padding: EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  title,
                  style: TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Row(
                  children: [
                    GestureDetector(
                      child: Image(
                        image: AssetImage(
                          'assets/images/icons/edit_square.png',
                        ),
                        height: 26.0,
                      ),
                      onTap: () {},
                    ),
                    SizedBox(
                      width: 25.0,
                    ),
                    GestureDetector(
                      child: Image(
                        image: AssetImage(
                          'assets/images/icons/delete.png',
                        ),
                        height: 26.0,
                      ),
                      onTap: () {},
                    ),
                    SizedBox(
                      width: 15.0,
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(height: 4.0),
            Text(
              phoneNumber,
              style: TextStyle(
                fontSize: 12.5,
                color: Color(0xFF7fab33),
              ),
            ),
            SizedBox(height: 3.0),
            Text(
              address,
              style: TextStyle(
                fontSize: 12.5,
                color: Colors.grey,
                height: 1.5,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
