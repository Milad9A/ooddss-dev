import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'product_details_screen.dart';
import '../widgets/widgets.dart';

class AddProductScreen extends StatefulWidget {
  num _selected = 0;

  String text = 'some text';

  String product;

  @override
  _AddProductScreenState createState() => _AddProductScreenState();
}

class _AddProductScreenState extends State<AddProductScreen> {
  ///first page
  TextEditingController _enProductTextController = TextEditingController();
  TextEditingController _arProductTextController = TextEditingController();
  String condition;
  String brand;
  TextEditingController _enOtherBrandTextController = TextEditingController();
  TextEditingController _arOtherTextController = TextEditingController();
  TextEditingController _keyWordsTextController = TextEditingController();
  TextEditingController _enShortDescriptionTextController =
      TextEditingController();
  TextEditingController _arShortDescriptionTextController =
      TextEditingController();
  TextEditingController _enLongDescriptionTextController =
      TextEditingController();
  TextEditingController _arLongDescriptionTextController =
      TextEditingController();

  ///second page
  String gender;

  String category;

  TextEditingController _subCategoryTextController = TextEditingController();

  TextEditingController _subSubCategoryTextController = TextEditingController();

  TextEditingController _weightTextController = TextEditingController();

  TextEditingController _skuTextController = TextEditingController();

  TextEditingController _stockTextController = TextEditingController();

  /// third page
  var videoClip;

  List<String> images = [
    'assets/images/icons/shose_test.png',
    null,
    null,
    null
  ];

  @override
  Widget build(BuildContext context) {
    List<Widget> subWidgets = [
      firstPage(),
      secondPage(),
      thirdPage(),
      forthPage(),
      fifthPage()
    ];
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(),
        body: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 50.0),
              child: AnimatedSwitcher(
                reverseDuration: Duration(milliseconds: 800),
                child: Container(child: subWidgets[widget._selected]),
                duration: Duration(milliseconds: 800),
              ),
            ),
            AddProductStepper(selected: widget._selected),
          ],
        ),
      ),
    );
  }

  Widget firstPage() {
    return SingleChildScrollView(
      key: UniqueKey(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 17.66, bottom: 20),
            child: Text(
              'ABOUT YOUR PRODUCT',
              style: TextStyle(
                  fontSize: 15.23,
                  fontWeight: FontWeight.w700,
                  fontFamily: 'Grava'),
            ),
          ),
          CustomTextFields(
            _enProductTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'Product Name (en)',
          ),
          CustomTextFields(
            _arProductTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'Product Name (ar)',
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            child: CustomDropDownButton(
              title: 'Condition',
              onChange: (value) {
                condition = value;
              },
              items: [Text('test')],
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            child: CustomDropDownButton(
              title: 'Brand',
              onChange: (value) {
                brand = value;
              },
              items: [Text('test')],
            ),
          ),
          CustomTextFields(
            _enOtherBrandTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'If other brand name (en)',
          ),
          CustomTextFields(
            _arOtherTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'If other brand name (ar)',
          ),
          CustomTextFields(
            _keyWordsTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'Keywords',
            maxLines: 3,
          ),
          CustomTextFields(
            _enShortDescriptionTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'Short Description (en)',
          ),
          CustomTextFields(
            _arShortDescriptionTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'Short Description (ar)',
          ),
          CustomTextFields(
            _enLongDescriptionTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'Long Description (en)',
            maxLines: 8,
          ),
          CustomTextFields(
            _arLongDescriptionTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'Long Description (ar)',
            maxLines: 8,
          ),
          RoundedEdgesFlatButton(
              title: 'PROCEED',
              margin:
                  const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
              onPressed: () {
                /// validation
                /// saving information into a new Product Widget
                widget.product = _arProductTextController.text;

                ///proceed to the next step
                nextPage();
              })
        ],
      ),
    );
  }

  Widget secondPage() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 17.66, bottom: 20),
            child: Text(
              'CATEGORIES',
              style: TextStyle(
                  fontSize: 15.23,
                  fontWeight: FontWeight.w700,
                  fontFamily: 'Grava'),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            child: CustomDropDownButton(
              title: 'Category',
              onChange: (value) {
                category = value;
              },
              items: [Text('test')],
            ),
          ),
          CustomTextFields(
            _subCategoryTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'Sub-Category',
          ),
          CustomTextFields(
            _subSubCategoryTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'Sub-Sub-Category',
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 40),
            child: CustomDropDownButton(
              title: 'Gender',
              onChange: (value) {
                gender = value;
              },
              items: [Text('test')],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 17.66, bottom: 20),
            child: Text(
              'PRODUCT DIMENSIONS',
              style: TextStyle(
                  fontSize: 15.23,
                  fontWeight: FontWeight.w700,
                  fontFamily: 'Grava'),
            ),
          ),
          Row(
            children: [
              Expanded(
                child: Column(
                  children: [
                    CustomTextFields(
                      _weightTextController,
                      padding: const EdgeInsets.only(
                          left: 17.66, right: 12.83, bottom: 20),
                      hintText: 'Weight',
                    ),
                    CustomTextFields(
                      _weightTextController,
                      padding: const EdgeInsets.only(
                          left: 17.66, right: 12.83, bottom: 20),
                      hintText: 'Width',
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  children: [
                    CustomTextFields(
                      _weightTextController,
                      padding: const EdgeInsets.only(
                          left: 12.83, right: 17.66, bottom: 20),
                      hintText: 'Length',
                    ),
                    CustomTextFields(
                      _weightTextController,
                      padding: const EdgeInsets.only(
                          left: 12.83, right: 17.66, bottom: 20),
                      hintText: 'Height',
                    ),
                  ],
                ),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 17.66, bottom: 20, top: 20),
            child: Text(
              'STOCK KEEPING',
              style: TextStyle(
                  fontSize: 15.23,
                  fontWeight: FontWeight.w700,
                  fontFamily: 'Grava'),
            ),
          ),
          Row(
            children: [
              Expanded(
                child: CustomTextFields(
                  _skuTextController,
                  padding: const EdgeInsets.only(
                      left: 12.83, right: 17.66, bottom: 49.33),
                  hintText: 'SKU',
                ),
              ),
              Expanded(
                child: CustomTextFields(
                  _stockTextController,
                  padding: const EdgeInsets.only(
                      left: 12.83, right: 17.66, bottom: 49.33),
                  hintText: 'Stock',
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                  child: RoundedEdgesFlatButton(
                      color: Color(0xff485258),
                      title: 'BACK',
                      margin: const EdgeInsets.only(
                          left: 17.66, right: 17.66, bottom: 20),
                      onPressed: () {
                        /// validation
                        /// saving information into a new Product Widget
                        widget.product = _arProductTextController.text;

                        ///proceed to the next step
                        previousPage();
                      })),
              Expanded(
                  child: RoundedEdgesFlatButton(
                      title: 'PROCEED',
                      margin: const EdgeInsets.only(
                          left: 17.66, right: 17.66, bottom: 20),
                      onPressed: () {
                        /// validation
                        /// saving information into a new Product Widget
                        widget.product = _arProductTextController.text;

                        ///proceed to the next step
                        nextPage();
                      })),
            ],
          ),
        ],
      ),
    );
  }

  Widget thirdPage() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 17.66, bottom: 20),
          child: RichText(
            text: TextSpan(
                text: 'PHOTOS',
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 15.23,
                    fontFamily: 'Grava',
                    color: Colors.black),
                children: [
                  TextSpan(
                    text: ' (UP TO 4)',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 12.5,
                      fontFamily: 'Grava',
                    ),
                  ),
                ]),
          ),
        ),
        Padding(
          padding:
              const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 37.33),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              for (var i = 0; i < 4; i++)
                images[i] == null
                    ? DottedBorder(
                        strokeCap: StrokeCap.butt,
                        dashPattern: [8, 4],
                        color: Color(0xffcdcbdb),
                        borderType: BorderType.RRect,
                        radius: Radius.circular(12),
                        padding: EdgeInsets.all(6),
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.1,
                            width: MediaQuery.of(context).size.height * 0.1,
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Image.asset(
                                'assets/images/icons/plus_sign.png',
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
                        ),
                      )
                    : Stack(
                        overflow: Overflow.visible,
                        children: [
                          Container(
                            height: MediaQuery.of(context).size.height * 0.1,
                            width: MediaQuery.of(context).size.height * 0.1,
                            child: Image.asset(
                              images[i],
                              fit: BoxFit.contain,
                            ),
                            decoration: BoxDecoration(),
                          ),
                          Positioned(
                            left: MediaQuery.of(context).size.height * 0.09,
                            bottom: MediaQuery.of(context).size.height * 0.09,
                            child: Container(
                              height: 70 / 4,
                              width: 70 / 4,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Color(0xff0f6da9)),
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Image.asset(
                                  'assets/images/icons/close.png',
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 17.66, bottom: 20),
          child: RichText(
            text: TextSpan(
                text: 'VIDEO',
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 15.23,
                    fontFamily: 'Grava',
                    color: Colors.black),
                children: [
                  TextSpan(
                    text: ' (ONLY 1 CLIP)',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 12.5,
                      fontFamily: 'Grava',
                    ),
                  ),
                ]),
          ),
        ),
        Padding(
          padding:
              const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 37.33),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              videoClip == null
                  ? DottedBorder(
                      strokeCap: StrokeCap.butt,
                      dashPattern: [8, 4],
                      color: Color(0xffcdcbdb),
                      borderType: BorderType.RRect,
                      radius: Radius.circular(12),
                      padding: EdgeInsets.all(6),
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.1,
                          width: MediaQuery.of(context).size.height * 0.1,
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Image.asset(
                              'assets/images/icons/plus_sign.png',
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                      ),
                    )
                  : Stack(
                      overflow: Overflow.visible,
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.height * 0.1,
                          width: MediaQuery.of(context).size.height * 0.1,
                          child: Image.asset(
                            videoClip,
                            fit: BoxFit.contain,
                          ),
                          decoration: BoxDecoration(),
                        ),
                        Positioned(
                          left: MediaQuery.of(context).size.height * 0.09,
                          bottom: MediaQuery.of(context).size.height * 0.09,
                          child: Container(
                            height: 70 / 4,
                            width: 70 / 4,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xff0f6da9)),
                            child: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Image.asset(
                                'assets/images/icons/close.png',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.only(left: 80.66),
                child: RoundedEdgesOutlineButton(
                    borderColor: Colors.grey,
                    title: 'UPLOAD',
                    textColor: Colors.grey,
                    onPressed: () {}),
              ))
            ],
          ),
        ),
        Spacer(),
        Padding(
          padding: const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
          child: Row(
            children: [
              Expanded(
                child: RoundedEdgesFlatButton(
                    color: Color(0xff485258),
                    title: 'BACK',
                    margin: const EdgeInsets.only(right: 17.66),
                    onPressed: () {
                      /// validation
                      /// saving information to the  Product Widget
                      widget.product = _arProductTextController.text;

                      ///proceed to the next step
                      previousPage();
                    }),
              ),
              Expanded(
                child: RoundedEdgesFlatButton(
                    title: 'CONTINUE',
                    margin: const EdgeInsets.only(left: 17.66),
                    onPressed: () {
                      /// validation
                      /// saving information to the  Product Widget
                      widget.product = _arProductTextController.text;

                      ///proceed to the next step
                      nextPage();
                    }),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget forthPage() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 17.66, bottom: 20),
            child: Text(
              'SHIPPING INFORMATION',
              style: TextStyle(
                  fontSize: 15.23,
                  fontWeight: FontWeight.w700,
                  fontFamily: 'Grava'),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 7.66),
            child: Text(
              'Shipping Method',
              style: TextStyle(
                  fontFamily: 'Grave',
                  fontSize: 12.5,
                  fontWeight: FontWeight.w400),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 10),
            child: Divider(
              thickness: 0.3,
              color: Color(0xffa0a6aa),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'OODDSS Fast Shipping',
                  style: TextStyle(
                      fontFamily: 'Grave',
                      fontSize: 12.5,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff353535)),
                ),
                Container(
                    height: 40,
                    child: FittedBox(
                        fit: BoxFit.fitHeight,
                        child: CircularCheckBox(
                          value: true,
                          onChanged: (value) {},
                        ))),
              ],
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 21),
            child: Divider(
              thickness: 0.3,
              color: Color(0xffa0a6aa),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Aramex',
                  style: TextStyle(
                      fontFamily: 'Grave',
                      fontSize: 12.5,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff353535)),
                ),
                Container(
                    height: 40,
                    child: FittedBox(
                        fit: BoxFit.fitHeight,
                        child: CircularCheckBox(
                          value: true,
                          onChanged: (value) {},
                        ))),
              ],
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 21),
            child: Divider(
              thickness: 0.3,
              color: Color(0xffa0a6aa),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'DHP',
                  style: TextStyle(
                      fontFamily: 'Grave',
                      fontSize: 12.5,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff353535)),
                ),
                Container(
                    height: 40,
                    child: FittedBox(
                        fit: BoxFit.fitHeight,
                        child: CircularCheckBox(
                          value: false,
                          onChanged: (value) {},
                        ))),
              ],
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 21),
            child: Divider(
              thickness: 0.3,
              color: Color(0xffa0a6aa),
            ),
          ),
          CustomTextFields(
            _subSubCategoryTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'Free shipping delivery time',
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 8),
            child: Text(
              'Like To Add Your Own Shipping?',
              style: TextStyle(fontSize: 17.58, fontWeight: FontWeight.w400),
            ),
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 17.66, right: 14.3),
                child: Container(
                    height: 40,
                    child: FittedBox(
                        fit: BoxFit.fitHeight,
                        child: CircularCheckBox(
                          value: true,
                          onChanged: (value) {},
                        ))),
              ),
              Text(
                'Yes',
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 17.58,
                    fontFamily: 'Grava'),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 89.66, right: 14.3),
                child: Container(
                    height: 40,
                    child: FittedBox(
                        fit: BoxFit.fitHeight,
                        child: CircularCheckBox(
                          value: true,
                          onChanged: (value) {},
                        ))),
              ),
              Text(
                'No',
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 17.58,
                    fontFamily: 'Grava'),
              ),
            ],
          ),
          CustomTextFields(
            _subSubCategoryTextController,
            padding: const EdgeInsets.only(
                left: 17.66, right: 17.66, bottom: 20, top: 27.33),
            hintText: 'Local Shipping Delivery',
          ),
          CustomTextFields(
            _subSubCategoryTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'Local Shipping Charges',
          ),
          CustomTextFields(
            _subSubCategoryTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'International Shipping Delivery',
          ),
          CustomTextFields(
            _subSubCategoryTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 80),
            hintText: 'International Shipping Charges',
          ),
          Row(
            children: [
              Expanded(
                  child: RoundedEdgesFlatButton(
                      color: Color(0xff485258),
                      title: 'BACK',
                      margin: const EdgeInsets.only(
                          left: 17.66, right: 17.66, bottom: 20),
                      onPressed: () {
                        /// validation
                        /// saving information into a new Product Widget
                        widget.product = _arProductTextController.text;

                        ///proceed to the next step
                        previousPage();
                      })),
              Expanded(
                  child: RoundedEdgesFlatButton(
                      title: 'PROCEED',
                      margin: const EdgeInsets.only(
                          left: 17.66, right: 17.66, bottom: 20),
                      onPressed: () {
                        /// validation
                        /// saving information into a new Product Widget
                        widget.product = _arProductTextController.text;

                        ///proceed to the next step
                        nextPage();
                      })),
            ],
          ),
        ],
      ),
    );
  }

  Widget fifthPage() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          CustomTextFields(
            _subSubCategoryTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'Colour',
          ),
          CustomTextFields(
            _subSubCategoryTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'Colour Swatch',
          ),
          CustomTextFields(
            _subSubCategoryTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'Size',
          ),
          CustomTextFields(
            _subSubCategoryTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'Capacity',
          ),
          CustomTextFields(
            _subSubCategoryTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'Stock',
          ),
          CustomTextFields(
            _subSubCategoryTextController,
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            hintText: 'Price',
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 21),
            child: Divider(
              thickness: 0.3,
              color: Color(0xffa0a6aa),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 8),
            child: Text(
              'Is It On Sale?',
              style: TextStyle(fontSize: 17.58, fontWeight: FontWeight.w400),
            ),
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 17.66, right: 14.3),
                child: Container(
                    height: 40,
                    child: FittedBox(
                        fit: BoxFit.fitHeight,
                        child: CircularCheckBox(
                          value: true,
                          onChanged: (value) {},
                        ))),
              ),
              Text(
                'Yes',
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 17.58,
                    fontFamily: 'Grava'),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 89.66, right: 14.3),
                child: Container(
                    height: 40,
                    child: FittedBox(
                        fit: BoxFit.fitHeight,
                        child: CircularCheckBox(
                          value: true,
                          onChanged: (value) {},
                        ))),
              ),
              Text(
                'No',
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 17.58,
                    fontFamily: 'Grava'),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 17.66, right: 17.66, top: 28),
            child: Text(
              'Sale Duration',
              style: TextStyle(fontSize: 17.58, fontWeight: FontWeight.w400),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 17.66,
              right: 17.66,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Container(
                    child: CustomDropDownButton(
                      title: 'from',
                      selectedItem: '1/1/1996',
                      onChange: (val) {},
                      icon: 'assets/images/icons/calender.png',
                    ),
                  ),
                ),
                SizedBox(
                  width: 13.66,
                ),
                Expanded(
                  child: Container(
                    child: CustomDropDownButton(
                      title: 'to',
                      selectedItem: '1/1/1996',
                      onChange: (val) {},
                      icon: 'assets/images/icons/calender.png',
                    ),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 17.66, right: 17.66, bottom: 21, top: 20),
            child: Divider(
              thickness: 0.3,
              color: Color(0xffa0a6aa),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 17.66, bottom: 20),
            child: Text(
              'VARIANTS',
              style: TextStyle(
                  fontSize: 15.23,
                  fontWeight: FontWeight.w700,
                  fontFamily: 'Grava'),
            ),
          ),
          ProductVariantItem(),
          RoundedEdgesFlatButton(
              title: 'SAVE & ADD VARIANT',
              margin:
                  const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
              onPressed: () {
                /// validation
                /// saving information into a new Product Widget
                widget.product = _arProductTextController.text;

                ///proceed to the next step
                previousPage();
              }),
          Row(
            children: [
              Expanded(
                  child: RoundedEdgesFlatButton(
                      color: Color(0xff485258),
                      title: 'BACK',
                      margin: const EdgeInsets.only(
                          left: 17.66, right: 17.66, bottom: 20),
                      onPressed: () {
                        /// validation
                        /// saving information into a new Product Widget
                        widget.product = _arProductTextController.text;

                        ///proceed to the next step
                        previousPage();
                      })),
              Expanded(
                  child: RoundedEdgesFlatButton(
                      title: 'PROCEED',
                      margin: const EdgeInsets.only(
                          left: 17.66, right: 17.66, bottom: 20),
                      onPressed: () {
                        /// validation
                        /// saving information into a new Product Widget
                        widget.product = _arProductTextController.text;

                        ///proceed to the next step
                        nextPage();
                      })),
            ],
          ),
        ],
      ),
    );
  }

  void nextPage() {
    setState(() {
      widget._selected++;
    });
  }

  void previousPage() {
    setState(() {
      widget._selected--;
    });
  }
}

class ProductVariantItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.33, right: 16.33, bottom: 20.33),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 13.0),
            child: Text(
              '#1',
              style: TextStyle(
                  fontFamily: 'Grava',
                  fontSize: 15.27,
                  fontWeight: FontWeight.w700),
            ),
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(16.66)),
                  boxShadow: [
                    BoxShadow(
                        color: Color(0xff000000).withOpacity(0.1),
                        blurRadius: 10,
                        spreadRadius: 0.9)
                  ]),
              child: Padding(
                padding: const EdgeInsets.all(22),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Colour',
                              style: TextStyle(
                                  fontFamily: 'Grava',
                                  fontSize: 13.89,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff282828)),
                            ),
                          ),
                          Expanded(
                            child: Text(
                              ':',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: 'Grava',
                                  fontSize: 13.89,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff282828)),
                            ),
                          ),
                          Expanded(
                              child: Row(
                            children: [
                              Container(
                                  height: 22,
                                  width: 22,
                                  child: OneColorCircle(color: Colors.purple)),
                              Padding(
                                padding: const EdgeInsets.only(left: 16.0),
                                child: Text(
                                  'purple',
                                  style: TextStyle(
                                      fontFamily: 'Grava',
                                      fontSize: 13.89,
                                      fontWeight: FontWeight.w500,
                                      color: Color(0xff282828)),
                                ),
                              ),
                            ],
                          ))
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Color Swatch',
                              style: TextStyle(
                                  fontFamily: 'Grava',
                                  fontSize: 13.89,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff282828)),
                            ),
                          ),
                          Expanded(
                            child: Text(
                              ':',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: 'Grava',
                                  fontSize: 13.89,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff282828)),
                            ),
                          ),
                          Expanded(
                              child: Text(
                            'HEX900',
                            style: TextStyle(
                                fontFamily: 'Grava',
                                fontSize: 13.89,
                                fontWeight: FontWeight.w500,
                                color: Color(0xff282828)),
                          ))
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Size',
                              style: TextStyle(
                                  fontFamily: 'Grava',
                                  fontSize: 13.89,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff282828)),
                            ),
                          ),
                          Expanded(
                            child: Text(
                              ':',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: 'Grava',
                                  fontSize: 13.89,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff282828)),
                            ),
                          ),
                          Expanded(
                              child: Text(
                            '120 GB',
                            style: TextStyle(
                                fontFamily: 'Grava',
                                fontSize: 13.89,
                                fontWeight: FontWeight.w500,
                                color: Color(0xff282828)),
                          ))
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Capacity',
                              style: TextStyle(
                                  fontFamily: 'Grava',
                                  fontSize: 13.89,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff282828)),
                            ),
                          ),
                          Expanded(
                            child: Text(
                              ':',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: 'Grava',
                                  fontSize: 13.89,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff282828)),
                            ),
                          ),
                          Expanded(
                              child: Text(
                            '-----',
                            style: TextStyle(
                                fontFamily: 'Grava',
                                fontSize: 13.89,
                                fontWeight: FontWeight.w500,
                                color: Color(0xff282828)),
                          ))
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Stock',
                              style: TextStyle(
                                  fontFamily: 'Grava',
                                  fontSize: 13.89,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff282828)),
                            ),
                          ),
                          Expanded(
                            child: Text(
                              ':',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: 'Grava',
                                  fontSize: 13.89,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff282828)),
                            ),
                          ),
                          Expanded(
                              child: Text(
                            '1200',
                            style: TextStyle(
                                fontFamily: 'Grava',
                                fontSize: 13.89,
                                fontWeight: FontWeight.w500,
                                color: Colors.red),
                          ))
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Price',
                              style: TextStyle(
                                  fontFamily: 'Grava',
                                  fontSize: 13.89,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff282828)),
                            ),
                          ),
                          Expanded(
                            child: Text(
                              ':',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: 'Grava',
                                  fontSize: 13.89,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff282828)),
                            ),
                          ),
                          Expanded(
                              child: Text(
                            '1000 USD',
                            style: TextStyle(
                                fontFamily: 'Grava',
                                fontSize: 13.89,
                                fontWeight: FontWeight.w500,
                                color: Colors.black),
                          ))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 13.0),
            child: Container(
              height: 24.66,
              child: Image.asset(
                'assets/images/icons/delete.png',
                fit: BoxFit.fitHeight,
              ),
            ),
          )
        ],
      ),
    );
  }
}

class AddProductStepper extends StatelessWidget {
  const AddProductStepper({
    Key key,
    @required num selected,
  })  : _selected = selected,
        super(key: key);

  final num _selected;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        color: Colors.white,
        height: 30,
        child: FittedBox(
          fit: BoxFit.contain,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              for (var i = 0; i < 5; i++)
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Center(
                        child: AnimatedContainer(
                          duration: Duration(milliseconds: 500),
                          height: _selected == i ? 26.66 : 10,
                          width: _selected == i ? 26.66 : 10,
                          child: Center(
                              child: Padding(
                            padding: const EdgeInsets.only(top: 4.0),
                            child: Text(
                              _selected == i ? '${i + 1}' : '',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 15,
                                color: Colors.white,
                              ),
                            ),
                          )),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: _selected == i
                                ? Color(0xff0f6da9)
                                : Color(0xffa0a6aa),
                          ),
                        ),
                      ),
                    ),
                    i != 4
                        ? Padding(
                            padding: const EdgeInsets.all(4.5),
                            child: Image.asset(
                              'assets/images/icons/dashes.png',
                              fit: BoxFit.fill,
                              width: MediaQuery.of(context).size.width * 0.15,
                            ),
                          )
                        : Container(),
                  ],
                ),
            ],
          ),
        ),
      ),
    );
  }
}
