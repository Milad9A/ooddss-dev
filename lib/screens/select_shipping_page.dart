import 'package:flutter/material.dart';

import '../utils/colors_repository.dart' as repo;
import '../widgets/widgets.dart';

class SelectShipping extends StatefulWidget {
  @override
  _SelectShippingState createState() => _SelectShippingState();
}

class _SelectShippingState extends State<SelectShipping> {
  int idxChecked;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    idxChecked = 0;
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text('SHOPPING CART'),
      ),
      body: SafeArea(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: Container(
                padding: const EdgeInsets.only(left: 16.0, right: 16),
                child: Column(
                  children: [
                    SizedBox(
                      height: 22,
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.only(top: 12),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.grey,
                                    width: 0.6,
                                    style: BorderStyle.solid))),
                        child: Column(
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: width / 4.6,
                                  child: Center(
                                    child: Card(
                                      color: Colors.grey[100],
                                      shadowColor: Colors.grey[200],
                                      elevation: 4,
                                      shape: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(12)),
                                        borderSide: BorderSide.none,
                                      ),
                                      child: Container(
                                        width: width / 5,
                                        height: width / 5,
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image: AssetImage(
                                                  "assets/images/examples/iphone.png",
                                                ),
                                                fit: BoxFit.fill)),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 12,
                                ),
                                Expanded(
                                  child: Container(
                                    padding:
                                        EdgeInsets.only(top: 10, bottom: 10),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'Apple IPhone XR 64GB esim dual',
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 12,
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: CustomDropDownButton(
                                                onChange: (val) {
                                                  print(val);
                                                },
                                                title: "Choose Shipping Method",
                                                items: [
                                                  "OODDSS Fast Delivery",
                                                  "OODDSS Delivery Two Method",
                                                  "OODDSS Delivery Three Method",
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 8.0, right: 8, top: 6, bottom: 10),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Flexible(
                                    child: Text(
                                      'FairyHub',
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.grey),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        )),
                    Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.only(top: 12),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.grey,
                                    width: 0.6,
                                    style: BorderStyle.solid))),
                        child: Column(
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: width / 4.6,
                                  child: Center(
                                    child: Card(
                                      color: Colors.grey[100],
                                      shadowColor: Colors.grey[200],
                                      elevation: 4,
                                      shape: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(12)),
                                        borderSide: BorderSide.none,
                                      ),
                                      child: Container(
                                        width: width / 5,
                                        height: width / 5,
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image: AssetImage(
                                                  "assets/images/examples/Nike-Shoes.png",
                                                ),
                                                fit: BoxFit.fill)),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 12,
                                ),
                                Expanded(
                                  child: Container(
                                    padding:
                                        EdgeInsets.only(top: 10, bottom: 10),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'Apple IPhone XR 64GB esim dual',
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 12,
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: CustomDropDownButton(
                                                onChange: (val) {
                                                  print(val);
                                                },
                                                title: "Choose Shipping Method",
                                                items: [
                                                  "OODDSS Fast Delivery",
                                                  "OODDSS Delivery Two Method",
                                                  "OODDSS Delivery Three Method",
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 8.0, right: 8, top: 6, bottom: 10),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Flexible(
                                    child: Text(
                                      'Alshaya',
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.grey),
                                    ),
                                  ),
                                  Flexible(
                                    child: Text(
                                      'Shipping Cost: 2000 KWD',
                                      style: TextStyle(
                                          fontSize: 14, color: repo.blue3),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        )),
                    CustomGroupCheckBox(
                      onChange: (index) {
                        print(index);
                      },
                      titles: [
                        "\nCheck if this is a Gift Card and Add a Gift Message below",
                      ],
                      allowNullCheck: true,
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    CustomTextFields(
                      TextEditingController(),
                      padding: EdgeInsets.all(0),
                      hintText: "Message",
                      maxLines: 4,
                      keyboardType: TextInputType.multiline,
                      validateFunction: (val) {
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 62,
                    ),
                  ],
                ),
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              // fillOverscroll: true, // Set true to change overscroll behavior. Purely preference.
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  padding: const EdgeInsets.only(
                    left: 16.0,
                    right: 16,
                    bottom: 20,
                  ),
                  child: RoundedEdgesFlatButton(
                      title: "CONTINUE", onPressed: () {}),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
