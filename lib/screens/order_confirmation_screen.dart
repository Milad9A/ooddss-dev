import 'package:flutter/material.dart';

import '../models/product_model.dart';
import '../widgets/widgets.dart';

class OrderConfirmationScreen extends StatefulWidget {
  final int orderNumber;

  const OrderConfirmationScreen({
    this.orderNumber,
  });

  @override
  _OrderConfirmationScreenState createState() =>
      _OrderConfirmationScreenState();
}

class _OrderConfirmationScreenState extends State<OrderConfirmationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        children: [
          Text(
            'Thank you for ordering with OODDSS.\nYour order no. is #${widget.orderNumber} and you will\nshortly receive a confirmation email.',
            textAlign: TextAlign.center,
            style: TextStyle(
              height: 1.4,
              fontSize: 15.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          Container(
            padding: EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Divider(
                  thickness: 1.2,
                ),
                SizedBox(height: 6.0),
                Text(
                  'Mr. Zein',
                  style: TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 6.0),
                Text('+ 999 999 999'),
                Text('zein@zprojects.com'),
              ],
            ),
          ),
          ProductsListCard(
            products: products,
            childOfProductTile: DeliveryRow(
              deliveryCompany: 'Some company',
              deliveryETA: '6',
            ),
          ),
          SizedBox(height: 15.0),
          AddressInformationExpandableContainer(),
          SizedBox(height: 15.0),
          PaymentInformationExpandableContainer(
            fontSize: 15.0,
            kwdFontSize: 10,
          ),
          SizedBox(height: 15.0),
        ],
      ),
    );
  }
}
