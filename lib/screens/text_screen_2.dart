import 'package:country_pickers/countries.dart';
import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

class TextScreen2 extends StatefulWidget {
  @override
  _TextScreen2State createState() => _TextScreen2State();
}

class _TextScreen2State extends State<TextScreen2> {
  var selected = 'none';

  String getFlagByIso(String iso) {
    return 'packages/country_pickers/assets/${iso.toLowerCase()}.png';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView.builder(
        itemCount: countryList.length,
        itemBuilder: (BuildContext context, int index) {
          return CurrencyTile(
            currencyName: countryList[index].name,
            imageURL: getFlagByIso(countryList[index].isoCode.toLowerCase()),
            lastTile: index == countryList.length - 1,
            onChanged: (bool value) {
              print(value);
            },
          );
        },
      ),
    );
  }
}
