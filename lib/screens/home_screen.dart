import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

import '../widgets/widgets.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<String> suggestions = ["nike", "Adidas", "H & M", "barbie"];
  TextEditingController _searchQueryController = TextEditingController();
  bool _isSearching = false;
  String searchQuery = "Search query";

  var listSuggestionsEntry;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        bottom: null,
        automaticallyImplyLeading: false,
        leadingWidth: 0.00001,
        titleSpacing: 0.0,
        backgroundColor: Color(0xfff4f5f9),
        leading: _isSearching
            ? Container(
                width: 0.005,
              )
            : Container(
                width: 0.005,
              ),
        title: _isSearching
            ? _buildSearchField()
            : Text(
                'app title',
                style: TextStyle(color: Colors.black),
              ),
        actions: _buildActions(),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min, // Use children total size

          children: [
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * .25,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: BezierCarouselContainer(),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height * .40,
                decoration: BoxDecoration(
                  color: Color(0xff0f6daa),
                  borderRadius: BorderRadius.all(
                    Radius.circular(50 / 4),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                  child: Text(
                                'CHOOSE FROM OVER 22.000 GREAT PRODUCTS.',
                                maxLines: 3,
                                style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400),
                              )),
                              Expanded(
                                  child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Icon(
                                        Icons.add_shopping_cart,
                                        color: Colors.white,
                                        size: 50,
                                      )))
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      width: 60,
                                      height: 60,
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white,
                                          image: new DecorationImage(
                                              scale: 0.5,
                                              fit: BoxFit.scaleDown,
                                              image: new AssetImage(
                                                  "assets/images/icons/teddy.png"))),
                                    ),
                                    Expanded(
                                        child: Center(
                                      child: Text(
                                        'Trending',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ))
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      width: 60,
                                      height: 60,
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white,
                                          image: new DecorationImage(
                                              scale: 0.5,
                                              fit: BoxFit.scaleDown,
                                              image: new AssetImage(
                                                  "assets/images/icons/teddy.png"))),
                                    ),
                                    Expanded(
                                        child: Center(
                                      child: Text(
                                        'Trending',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ))
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      width: 60,
                                      height: 60,
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white,
                                          image: new DecorationImage(
                                              scale: 0.5,
                                              fit: BoxFit.scaleDown,
                                              image: new AssetImage(
                                                  "assets/images/icons/teddy.png"))),
                                    ),
                                    Expanded(
                                        child: Center(
                                      child: Text(
                                        'Trending',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ))
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * .4,
              color: Color(0xfff0f1f3),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'SHOP BY STORIES',
                      textAlign: TextAlign.start,
                    ),
                  ),
                  Expanded(
                    child: ListView(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      children: [
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ConstrainedBox(
                              constraints: BoxConstraints(
                                  minWidth:
                                      MediaQuery.of(context).size.width * .1,
                                  maxHeight:
                                      MediaQuery.of(context).size.width * .5,
                                  maxWidth:
                                      MediaQuery.of(context).size.width * .5,
                                  minHeight:
                                      MediaQuery.of(context).size.width * .1),
                              child: Container(
                                child: ClipRRect(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15)),
                                    child: FadeInImage(
                                      fit: BoxFit.contain,
                                      placeholder: AssetImage(
                                          'assets/images/icons/test_img.jpg'),
                                      image: NetworkImage(
                                          'https://images-na.ssl-images-amazon.com/images/I/71iS7jKcpZL._UY695_.jpg'),
                                    )),
                                decoration: BoxDecoration(
                                    color: Colors.red,
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.black.withOpacity(0.22),
                                          blurRadius: 0.7,
                                          spreadRadius: 0.7,
                                          offset: Offset(0, 2)),
                                    ],
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15))),
                              ),
                            )),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ConstrainedBox(
                              constraints: BoxConstraints(
                                  minWidth:
                                      MediaQuery.of(context).size.width * .1,
                                  maxHeight:
                                      MediaQuery.of(context).size.width * .5,
                                  maxWidth:
                                      MediaQuery.of(context).size.width * .5,
                                  minHeight:
                                      MediaQuery.of(context).size.width * .1),
                              child: Container(
                                child: ClipRRect(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15)),
                                    child: FadeInImage(
                                      fit: BoxFit.contain,
                                      placeholder: AssetImage(
                                          'assets/images/icons/test_img.jpg'),
                                      image: NetworkImage(
                                          'https://images-na.ssl-images-amazon.com/images/I/71iS7jKcpZL._UY695_.jpg'),
                                    )),
                                decoration: BoxDecoration(
                                    color: Colors.red,
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.black.withOpacity(0.22),
                                          blurRadius: 0.7,
                                          spreadRadius: 0.7,
                                          offset: Offset(0, 2)),
                                    ],
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15))),
                              ),
                            )),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ConstrainedBox(
                              constraints: BoxConstraints(
                                  minWidth:
                                      MediaQuery.of(context).size.width * .1,
                                  maxHeight:
                                      MediaQuery.of(context).size.width * .5,
                                  maxWidth:
                                      MediaQuery.of(context).size.width * .5,
                                  minHeight:
                                      MediaQuery.of(context).size.width * .1),
                              child: Container(
                                child: ClipRRect(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15)),
                                    child: FadeInImage(
                                      fit: BoxFit.contain,
                                      placeholder: AssetImage(
                                          'assets/images/icons/test_img.jpg'),
                                      image: NetworkImage(
                                          'https://images-na.ssl-images-amazon.com/images/I/71iS7jKcpZL._UY695_.jpg'),
                                    )),
                                decoration: BoxDecoration(
                                    color: Colors.red,
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.black.withOpacity(0.22),
                                          blurRadius: 0.7,
                                          spreadRadius: 0.7,
                                          offset: Offset(0, 2)),
                                    ],
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15))),
                              ),
                            )),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ConstrainedBox(
                              constraints: BoxConstraints(
                                  minWidth:
                                      MediaQuery.of(context).size.width * .1,
                                  maxHeight:
                                      MediaQuery.of(context).size.width * .5,
                                  maxWidth:
                                      MediaQuery.of(context).size.width * .5,
                                  minHeight:
                                      MediaQuery.of(context).size.width * .1),
                              child: Container(
                                child: ClipRRect(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15)),
                                    child: FadeInImage(
                                      fit: BoxFit.contain,
                                      placeholder: AssetImage(
                                          'assets/images/icons/test_img.jpg'),
                                      image: NetworkImage(
                                          'https://images-na.ssl-images-amazon.com/images/I/71iS7jKcpZL._UY695_.jpg'),
                                    )),
                                decoration: BoxDecoration(
                                    color: Colors.red,
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.black.withOpacity(0.22),
                                          blurRadius: 0.7,
                                          spreadRadius: 0.7,
                                          offset: Offset(0, 2)),
                                    ],
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15))),
                              ),
                            )),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ConstrainedBox(
                              constraints: BoxConstraints(
                                  minWidth:
                                      MediaQuery.of(context).size.width * .1,
                                  maxHeight:
                                      MediaQuery.of(context).size.width * .5,
                                  maxWidth:
                                      MediaQuery.of(context).size.width * .5,
                                  minHeight:
                                      MediaQuery.of(context).size.width * .1),
                              child: Container(
                                child: ClipRRect(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15)),
                                    child: FadeInImage(
                                      fit: BoxFit.contain,
                                      placeholder: AssetImage(
                                          'assets/images/icons/test_img.jpg'),
                                      image: NetworkImage(
                                          'https://images-na.ssl-images-amazon.com/images/I/71iS7jKcpZL._UY695_.jpg'),
                                    )),
                                decoration: BoxDecoration(
                                    color: Colors.red,
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.black.withOpacity(0.22),
                                          blurRadius: 0.7,
                                          spreadRadius: 0.7,
                                          offset: Offset(0, 2)),
                                    ],
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15))),
                              ),
                            )),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ConstrainedBox(
                              constraints: BoxConstraints(
                                  minWidth:
                                      MediaQuery.of(context).size.width * .1,
                                  maxHeight:
                                      MediaQuery.of(context).size.width * .5,
                                  maxWidth:
                                      MediaQuery.of(context).size.width * .5,
                                  minHeight:
                                      MediaQuery.of(context).size.width * .1),
                              child: Container(
                                child: ClipRRect(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15)),
                                    child: FadeInImage(
                                      fit: BoxFit.contain,
                                      placeholder: AssetImage(
                                          'assets/images/icons/test_img.jpg'),
                                      image: NetworkImage(
                                          'https://images-na.ssl-images-amazon.com/images/I/71iS7jKcpZL._UY695_.jpg'),
                                    )),
                                decoration: BoxDecoration(
                                    color: Colors.red,
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.black.withOpacity(0.22),
                                          blurRadius: 0.7,
                                          spreadRadius: 0.7,
                                          offset: Offset(0, 2)),
                                    ],
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15))),
                              ),
                            )),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ConstrainedBox(
                              constraints: BoxConstraints(
                                  minWidth:
                                      MediaQuery.of(context).size.width * .1,
                                  maxHeight:
                                      MediaQuery.of(context).size.width * .5,
                                  maxWidth:
                                      MediaQuery.of(context).size.width * .5,
                                  minHeight:
                                      MediaQuery.of(context).size.width * .1),
                              child: Container(
                                child: ClipRRect(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15)),
                                    child: FadeInImage(
                                      fit: BoxFit.contain,
                                      placeholder: AssetImage(
                                          'assets/images/icons/test_img.jpg'),
                                      image: NetworkImage(
                                          'https://images-na.ssl-images-amazon.com/images/I/71iS7jKcpZL._UY695_.jpg'),
                                    )),
                                decoration: BoxDecoration(
                                    color: Colors.red,
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.black.withOpacity(0.22),
                                          blurRadius: 0.7,
                                          spreadRadius: 0.7,
                                          offset: Offset(0, 2)),
                                    ],
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15))),
                              ),
                            )),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: ListView(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration: BoxDecoration(
                                color: Colors.green,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(50 / 4))),
                            width: MediaQuery.of(context).size.width * .9,
                            height: MediaQuery.of(context).size.height * .1,
                            child: ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(50 / 4)),
                                child: Image.asset(
                                  'assets/images/icons/landscape.jpg',
                                  fit: BoxFit.cover,
                                )),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration: BoxDecoration(
                                color: Colors.green,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(50 / 4))),
                            width: MediaQuery.of(context).size.width * .9,
                            height: MediaQuery.of(context).size.height * .1,
                            child: ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(50 / 4)),
                                child: Image.asset(
                                  'assets/images/icons/landscape.jpg',
                                  fit: BoxFit.cover,
                                )),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration: BoxDecoration(
                                color: Colors.green,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(50 / 4))),
                            width: MediaQuery.of(context).size.width * .9,
                            height: MediaQuery.of(context).size.height * .1,
                            child: ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(50 / 4)),
                                child: Image.asset(
                                  'assets/images/icons/landscape.jpg',
                                  fit: BoxFit.cover,
                                )),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildSearchField() {
    return TypeAheadFormField(
      textFieldConfiguration: TextFieldConfiguration(
          cursorColor: Colors.transparent,
          style: TextStyle(
              fontSize: 16.66,
              color: Colors.black,
              fontFamily: 'Grava',
              fontWeight: FontWeight.w400),
          controller: this._searchQueryController,
          decoration: InputDecoration(
            fillColor: Colors.transparent,
            hoverColor: Colors.transparent,
            border: InputBorder.none,
            focusedBorder: InputBorder.none,
            disabledBorder: InputBorder.none,
            errorBorder: InputBorder.none,
            focusColor: Colors.transparent,
            focusedErrorBorder: InputBorder.none,
            prefixIcon: IconButton(
              icon: Image.asset(
                'assets/images/icons/arrow_back.png',
                height: 16.33,
              ),
              onPressed: _stopSearching,
            ),
            suffixIcon: IconButton(
              icon: Icon(
                Icons.clear,
                color: Colors.black,
              ),
              onPressed: _clearSearchQuery,
            ),
          )),
      suggestionsCallback: (pattern) {
        print(pattern);

        return suggestions;
      },
      itemBuilder: (context, suggestion) {
        print(suggestions);

        return ListTile(
          leading: Image.asset(
            'assets/images/icons/search.png',
            height: 20,
          ),
          title: Text(
            suggestion,
            style: TextStyle(
                fontFamily: 'Grava',
                fontWeight: FontWeight.w400,
                fontSize: 15,
                color: Color(0xff464a50)),
          ),
        );
      },
      transitionBuilder: (context, suggestionsBox, controller) {
        return suggestionsBox;
      },
      onSuggestionSelected: (suggestion) {
        this._searchQueryController.text = suggestion;
      },
    );
  }

  List<Widget> _buildActions() {
    if (!_isSearching) {
      return <Widget>[
        IconButton(
          icon: const Icon(
            Icons.search,
            color: Colors.black,
          ),
          onPressed: _startSearch,
        ),
      ];
    }
    return [];
  }

  void _startSearch() {
    ModalRoute.of(context)
        .addLocalHistoryEntry(LocalHistoryEntry(onRemove: _stopSearching));

    setState(() {
      _isSearching = true;
    });
  }

  void updateSearchQuery(String newQuery) {
    setState(() {
      searchQuery = newQuery;
    });
  }

  void _stopSearching() {
    _clearSearchQuery();

    setState(() {
      _isSearching = false;
    });
  }

  void _clearSearchQuery() {
    setState(() {
      _searchQueryController.clear();
      updateSearchQuery("");
    });
  }

// void updateOverlay(String newText) {
//   if (listSuggestionsEntry == null) {
//     final Size textFieldSize = (context.findRenderObject() as RenderBox)
//         .size;
//     final width = MediaQuery
//         .of(context)
//         .size
//         .width;
//     final height = textFieldSize.height;
//     listSuggestionsEntry = new OverlayEntry(builder: (context) {
//       return new Positioned(
//           width: width,
//           child: CompositedTransformFollower(
//               link: _layerLink,
//               showWhenUnlinked: false,
//               offset: Offset(0.0, height-500),
//               child: new SizedBox(
//                   width: width,
//                   child: new Card(color: Colors.green,
//                       child: new Column(
//                           children: [
//                             Row(children: [
//                               new Expanded(
//                                   child: new InkWell(
//                                     child: Text('asdlkgjasdl;kgjl'),
//                                   ))
//                             ])
//                           ]
//                       )))
//           )
//       );
//     });
//     Overlay.of(context).insert(listSuggestionsEntry);
//   }
// }

}
