import 'package:flutter/material.dart';

import '../models/product_model.dart';
import '../widgets/widgets.dart';

class SoldHistoryDetailsScreen extends StatefulWidget {
  @override
  _SoldHistoryDetailsScreenState createState() =>
      _SoldHistoryDetailsScreenState();
}

class _SoldHistoryDetailsScreenState extends State<SoldHistoryDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Ordered on 07-01-2020',
                style: TextStyle(
                  fontSize: 12.5,
                  color: Color(0xFF6F7071),
                ),
              ),
              SizedBox(height: 4.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Order Status',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            'Payment Status',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(width: 30.0),
                      Column(
                        children: [
                          Text(
                            ':',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            ':',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.centerRight,
                    width: MediaQuery.of(context).size.width * 0.4,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Shipped',
                          overflow: TextOverflow.ellipsis,
                        ),
                        Text(
                          'Awaiting Cash Payment',
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(height: 4.0),
          Divider(thickness: 1.0),
          RoundedEdgesFlatButton(
            title: 'ORDER SUMMARY',
            margin: EdgeInsets.all(0),
            onPressed: () {},
          ),
          SizedBox(height: 15.0),
          Text(
            'SHIPPING METHOD(S)',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 15.0,
            ),
          ),
          SizedBox(height: 8.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 80.0,
                padding: EdgeInsets.symmetric(
                  horizontal: 18.0,
                  vertical: 10.0,
                ),
                decoration: BoxDecoration(
                  color: Color(0xFF0F6DA9).withOpacity(0.15),
                  borderRadius: BorderRadius.circular(9.0),
                ),
                child: Image(
                  height: 20.0,
                  image: AssetImage(
                    'assets/images/icons/truck_blue.png',
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'OODDSS Fast Shipping',
                  ),
                  Text(
                    'ARAMEX',
                  ),
                ],
              ),
            ],
          ),
          SizedBox(height: 10.0),
          ProductsListCard(
            products: products,
            childOfProductTile: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Column(
                children: [
                  CustomDropDownButton(
                    title: 'Shipping Status',
                    items: ['Shipped'],
                    onChange: (value) {
                      print(value);
                    },
                  ),
                  SizedBox(height: 5.0),
                  CustomDropDownButton(
                    title: 'Order Status',
                    items: ['Completed'],
                    onChange: (value) {
                      print(value);
                    },
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 12.0),
          PaymentInformationExpandableContainer(
            fontSize: 14.0,
            kwdFontSize: 7.0,
          ),
          SizedBox(height: 12.0),
        ],
      ),
    );
  }
}
