import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

class StoreScreen extends StatefulWidget {
  @override
  _StoreScreenState createState() => _StoreScreenState();
}

class _StoreScreenState extends State<StoreScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView(
        children: [
          StoreCard(
            onTapHeart: () {},
            onTapShopping: () {},
            currencyName: 'KWD',
            price: '29.99',
            oldPriceAndCurrency: '35.00 KWD',
            imageURL: 'assets/images/examples/Nike-Shoes.png',
          ),
          StoreCard(
            onTapHeart: () {},
            onTapShopping: () {},
            currencyName: 'KWD',
            price: '29.99',
            oldPriceAndCurrency: '35.00 KWD',
            imageURL: 'assets/images/examples/Nike-Shoes-2.png',
          ),
        ],
      ),
    );
  }
}
