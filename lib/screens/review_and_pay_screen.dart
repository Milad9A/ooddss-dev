import 'package:flutter/material.dart';

import '../models/product_model.dart';
import '../widgets/widgets.dart';

class ReviewAndPayScreen extends StatefulWidget {
  @override
  ReviewAndPayScreenState createState() => ReviewAndPayScreenState();
}

class ReviewAndPayScreenState extends State<ReviewAndPayScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFf1f5fa),
      appBar: AppBar(),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        children: [
          SizedBox(height: 4.0),
          PaymentMethodRow(
            onChanged: (value) {},
          ),
          SizedBox(height: 14.0),
          PaymentDetails(
            couponHasDottedLine: true,
          ),
          SizedBox(height: 14.0),
          ProductsListCard(
            products: products,
            childOfProductTile: DeliveryRow(
              deliveryCompany: 'ARAMEX',
              deliveryETA: '2',
            ),
          ),
          SizedBox(height: 14.0),
          AddressInformationExpandableContainer(),
          SizedBox(height: 14.0),
          RoundedEdgesFlatButton(
            title: 'BUY NOW',
            margin: EdgeInsets.all(0),
            onPressed: () {},
          ),
          SizedBox(height: 14.0),
        ],
      ),
    );
  }
}
