import 'package:flutter/material.dart';

import '../models/product_model.dart';
import '../widgets/widgets.dart';

class WishlistScreen extends StatefulWidget {
  @override
  _WishlistScreenState createState() => _WishlistScreenState();
}

class _WishlistScreenState extends State<WishlistScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView.builder(
        itemCount: products.length,
        itemBuilder: (BuildContext context, int index) {
          return ShoppingCartTile(
            product: products[index],
            isLastTile: index == products.length - 1,
            isWishlistTile: true,
          );
        },
      ),
    );
  }
}
