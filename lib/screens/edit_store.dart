import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../utils/colors_repository.dart';
import '../widgets/widgets.dart';

class EditStoreScreen extends StatefulWidget {
  @override
  _EditStoreScreenState createState() => _EditStoreScreenState();
}

class _EditStoreScreenState extends State<EditStoreScreen> {
  var _selectedTap;

  TextEditingController _controller = TextEditingController();

  var isRounded = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 17.66, top: 23.33),
            child: Stack(
              children: [
                Align(
                  alignment: _selectedTap == 1
                      ? Alignment.centerLeft
                      : Alignment.centerRight,
                  child: Container(
                      height: 41.66 / 2,
                      width: MediaQuery.of(context).size.width / 2,
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Color(0xff578fdd).withOpacity(0.07),
                                spreadRadius: 0,
                                blurRadius: 10,
                                offset:
                                    Offset(_selectedTap == 1 ? -20 : 20, 30))
                          ],
                          borderRadius:
                              BorderRadius.all(Radius.circular(50 / 4)))),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Container(
                    height: 41.66,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius:
                            BorderRadius.all(Radius.circular(50 / 4))),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              _selectedTap = 1;
                            });
                          },
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/images/icons/sort.png',
                                height: 16.33,
                                width: 16.33,
                                fit: BoxFit.fill,
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              Text(
                                'SORT',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Color(0xff393d49),
                                    fontWeight: FontWeight.w400,
                                    fontSize: 13.89),
                              ),
                            ],
                          ),
                        ),
                        Text('|',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.w400, fontSize: 13.89)),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              _selectedTap = 2;
                            });
                          },
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/images/icons/filter.png',
                                height: 16.33,
                                width: 16.33,
                                fit: BoxFit.fill,
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              Text(
                                'FILTER',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Color(0xff393d49),
                                    fontWeight: FontWeight.w400,
                                    fontSize: 13.89),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 17.66, bottom: 20),
            child: Text(
              'STORE LOGO',
              style: TextStyle(
                  fontSize: 15.23,
                  fontWeight: FontWeight.w700,
                  fontFamily: 'Grava'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 21.33),
            child: Align(
              alignment: Alignment.center,
              child: isRounded
                  ? DottedBorder(
                      strokeCap: StrokeCap.butt,
                      dashPattern: [8, 4],
                      color: Color(0xffcdcbdb),
                      borderType: BorderType.RRect,
                      radius: Radius.circular(12),
                      padding: EdgeInsets.all(6),
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.2,
                          width: MediaQuery.of(context).size.height * 0.2,
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Image.asset(
                              'assets/images/icons/plus_sign.png',
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                      ))
                  : Stack(
                      overflow: Overflow.visible,
                      children: [
                        Container(
                            height: MediaQuery.of(context).size.height * 0.2,
                            width: MediaQuery.of(context).size.height * 0.2,
                            child: Padding(
                              padding: const EdgeInsets.all(25.0),
                              child: Image.asset(
                                'assets/images/icons/nike_logo.png',
                                fit: BoxFit.contain,
                              ),
                            ),
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(13.33)),
                                border:
                                    Border.all(color: black1, width: 0.33))),
                        Positioned(
                          left: MediaQuery.of(context).size.height * 0.18,
                          bottom: MediaQuery.of(context).size.height * 0.18,
                          child: Container(
                            height: 70 / 4,
                            width: 70 / 4,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xff0f6da9)),
                            child: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Image.asset(
                                'assets/images/icons/close.png',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 17.66, bottom: 20),
            child: Text(
              'BANNER',
              style: TextStyle(
                  fontSize: 15.23,
                  fontWeight: FontWeight.w700,
                  fontFamily: 'Grava'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 23),
            child: Align(
              alignment: Alignment.center,
              child: isRounded
                  ? Padding(
                      padding: const EdgeInsets.only(left: 17.66, right: 17.66),
                      child: DottedBorder(
                          strokeCap: StrokeCap.butt,
                          dashPattern: [8, 4],
                          color: Color(0xffcdcbdb),
                          borderType: BorderType.RRect,
                          radius: Radius.circular(12),
                          padding: EdgeInsets.all(6),
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            child: Container(
                              height: MediaQuery.of(context).size.height * 0.15,
                              width: MediaQuery.of(context).size.width,
                              child: Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Image.asset(
                                  'assets/images/icons/plus_sign.png',
                                  fit: BoxFit.contain,
                                ),
                              ),
                            ),
                          )),
                    )
                  : Stack(
                      overflow: Overflow.visible,
                      children: [
                        Padding(
                          padding:
                              const EdgeInsets.only(left: 17.66, right: 17.66),
                          child: Container(
                              height: MediaQuery.of(context).size.height * 0.15,
                              width: MediaQuery.of(context).size.width,
                              child: Padding(
                                padding: const EdgeInsets.all(25.0),
                                child: Image.asset(
                                  'assets/images/icons/nike_logo.png',
                                  fit: BoxFit.contain,
                                ),
                              ),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(13.33)),
                                  border:
                                      Border.all(color: black1, width: 0.33))),
                        ),
                        Positioned(
                          left: MediaQuery.of(context).size.width * 0.92,
                          bottom: MediaQuery.of(context).size.height * 0.18,
                          child: Container(
                            height: 70 / 4,
                            width: 70 / 4,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xff0f6da9)),
                            child: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Image.asset(
                                'assets/images/icons/close.png',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
            ),
          ),
          CustomTextFields(
            _controller,
            padding: EdgeInsets.only(
              left: 17.66,
              right: 17.66,
              bottom: 40,
            ),
            hintText: 'Description',
            maxLines: 4,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 17.66, bottom: 23.33),
            child: Text(
              'SHIPPING DETAILS',
              style: TextStyle(
                  fontSize: 15.23,
                  fontWeight: FontWeight.w700,
                  fontFamily: 'Grava'),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 7.66),
            child: Text(
              'Shipping Method',
              style: TextStyle(
                  fontFamily: 'Grave',
                  fontSize: 12.5,
                  fontWeight: FontWeight.w400),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 10),
            child: Divider(
              thickness: 0.3,
              color: Color(0xffa0a6aa),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'OODDSS Fast Shipping',
                  style: TextStyle(
                      fontFamily: 'Grave',
                      fontSize: 12.5,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff353535)),
                ),
                Container(
                    height: 40,
                    child: FittedBox(
                        fit: BoxFit.fitHeight,
                        child: CircularCheckBox(
                          value: true,
                          onChanged: (value) {},
                        ))),
              ],
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 21),
            child: Divider(
              thickness: 0.3,
              color: Color(0xffa0a6aa),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Aramex',
                  style: TextStyle(
                      fontFamily: 'Grave',
                      fontSize: 12.5,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff353535)),
                ),
                Container(
                    height: 40,
                    child: FittedBox(
                        fit: BoxFit.fitHeight,
                        child: CircularCheckBox(
                          value: true,
                          onChanged: (value) {},
                        ))),
              ],
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 21),
            child: Divider(
              thickness: 0.3,
              color: Color(0xffa0a6aa),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'DHP',
                  style: TextStyle(
                      fontFamily: 'Grave',
                      fontSize: 12.5,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff353535)),
                ),
                Container(
                    height: 40,
                    child: FittedBox(
                        fit: BoxFit.fitHeight,
                        child: CircularCheckBox(
                          value: false,
                          onChanged: (value) {},
                        ))),
              ],
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 21),
            child: Divider(
              thickness: 0.3,
              color: Color(0xffa0a6aa),
            ),
          ),
          CustomTextFields(
            _controller,
            padding: EdgeInsets.only(
              left: 17.66,
              right: 17.66,
              bottom: 20,
            ),
            hintText: 'Address Line 1',
          ),
          CustomTextFields(
            _controller,
            padding: EdgeInsets.only(
              left: 17.66,
              right: 17.66,
              bottom: 20,
            ),
            hintText: 'Address Line 2',
          ),
          CustomTextFields(
            _controller,
            padding: EdgeInsets.only(
              left: 17.66,
              right: 17.66,
              bottom: 20,
            ),
            hintText: 'State',
          ),
          CustomTextFields(
            _controller,
            padding: EdgeInsets.only(
              left: 17.66,
              right: 17.66,
              bottom: 20,
            ),
            hintText: 'City',
          ),
          CustomTextFields(
            _controller,
            padding: EdgeInsets.only(
              left: 17.66,
              right: 17.66,
              bottom: 20,
            ),
            hintText: 'Postal Code',
          ),
          Padding(
            padding: EdgeInsets.only(
              left: 17.66,
              right: 17.66,
              bottom: 20,
            ),
            child: CustomDropDownButton(
              title: 'Country',
              items: ['Kuwait'],
              onChange: (val) {},
            ),
          ),
          CustomTextFields(
            _controller,
            padding: EdgeInsets.only(
              left: 17.66,
              right: 17.66,
              bottom: 20,
            ),
            hintText: 'Return Days',
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: CustomDropDownButton(
                    onChange: (val) {
                      print(val);
                    },
                    title: "Code",
                    items: ["+963", "+976", "+963", "+954"],
                    selectedItem: "+963",
                  ),
                  flex: 2,
                ),
                SizedBox(
                  width: 12,
                ),
                Expanded(
                  child: CustomTextFields(
                    TextEditingController(),
                    padding: EdgeInsets.all(0),
                    hintText: "Phone",
                    validateFunction: (val) {
                      return null;
                    },
                  ),
                  flex: 3,
                ),
              ],
            ),
          ),
          RoundedEdgesFlatButton(
              title: 'SAVE & UPDATE',
              margin:
                  const EdgeInsets.only(left: 17.66, right: 17.66, bottom: 20),
              onPressed: () {
                /// validation
                /// saving information into a new Product Widget

                ///proceed to the next step
              })
        ],
      )),
    );
  }
}
