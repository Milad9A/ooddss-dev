import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

class BezierContainerScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            BezierContainer(),
            SizedBox(
              height: 50,
              width: 5,
            ),
            BezierCarouselContainer()
          ],
        ),
      ),
    );
  }
}
