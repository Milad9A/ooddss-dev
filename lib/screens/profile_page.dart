import 'package:flutter/material.dart';

import '../utils/colors_repository.dart' as repo;
import '../widgets/widgets.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  bool _checked;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _checked = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit - Profile'),
      ),
      body: SafeArea(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: Container(
                padding: const EdgeInsets.only(left: 16.0, right: 16),
                child: Column(
                  children: [
                    SizedBox(
                      height: 32,
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: CustomDropDownButton(
                            onChange: (val) {
                              print(val);
                            },
                            title: "Title",
                            items: ["Miss", "Mr"],
                            selectedItem: "Miss",
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    CustomTextFields(
                      TextEditingController(),
                      padding: EdgeInsets.all(0),
                      hintText: "First Name",
                      validateFunction: (val) {
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    CustomTextFields(
                      TextEditingController(),
                      padding: EdgeInsets.all(0),
                      hintText: "Last Name",
                      validateFunction: (val) {
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    CustomTextFields(
                      TextEditingController(),
                      padding: EdgeInsets.all(0),
                      hintText: "example@examole.com",
                      keyboardType: TextInputType.emailAddress,
                      whiteFillColor: false,
                      validateFunction: (val) {
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: CustomDropDownButton(
                            onChange: (val) {
                              print(val);
                            },
                            title: "Country",
                            items: ["Syria", "Kuwait", "Iraq", "Lebanon"],
                            selectedItem: "Kuwait",
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: CustomDropDownButton(
                            onChange: (val) {
                              print(val);
                            },
                            title: "Code",
                            items: ["+963", "+976", "+963", "+954"],
                            selectedItem: "+963",
                          ),
                          flex: 2,
                        ),
                        SizedBox(
                          width: 12,
                        ),
                        Expanded(
                          child: CustomTextFields(
                            TextEditingController(),
                            padding: EdgeInsets.all(0),
                            hintText: "Phone",
                            validateFunction: (val) {
                              return null;
                            },
                          ),
                          flex: 3,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 42,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "CHANGE PASSWORD",
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 18,
                    ),
                    CustomTextFields(
                      TextEditingController(),
                      padding: EdgeInsets.all(0),
                      hintText: "New Password",
                      validateFunction: (val) {
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    CustomTextFields(
                      TextEditingController(),
                      padding: EdgeInsets.all(0),
                      hintText: "Confirm Password",
                      validateFunction: (val) {
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 42,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "NEWSLETTER",
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Text(
                            "Subscribe to the newsletter\nand get the lateest updates",
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.black,
                            ),
                          ),
                        ),
                        Switch(
                          value: _checked,
                          onChanged: (value) {
                            setState(() {
                              _checked = !_checked;
                            });
                          },
                          inactiveTrackColor: Colors.grey[300],
                          activeTrackColor: repo.blue5,
                          activeColor: repo.blue4,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 62,
                    ),
                  ],
                ),
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              // fillOverscroll: true, // Set true to change overscroll behavior. Purely preference.
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  padding: const EdgeInsets.only(
                    left: 16.0,
                    right: 16,
                    bottom: 20,
                  ),
                  child:
                      RoundedEdgesFlatButton(title: "UPDATE", onPressed: () {}),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
