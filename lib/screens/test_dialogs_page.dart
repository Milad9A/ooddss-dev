import 'package:flutter/material.dart';

import '../widgets/widgets.dart';
import 'contact_us_page.dart';
import 'custom_textfield_page.dart';
import 'edit_new_address_page.dart';
import 'edit_pay_account_page.dart';
import 'profile_page.dart';
import 'select_shipping_page.dart';
import 'seller_registration_page.dart';
import 'shipping_method.dart';
import 'test_side_menue.dart';

class TestDialogPage extends StatefulWidget {
  @override
  _TestDialogPageState createState() => new _TestDialogPageState();
}

class _TestDialogPageState extends State<TestDialogPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: new AppBar(
        title: new Text("Notification Widgegt"),
      ),
      body: new SafeArea(
          top: false,
          bottom: false,
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 32,
                ),
                RoundedEdgesFlatButton(
                    title: "Show Add Cupon Dialog",
                    onPressed: () {
                      showAlert(context,
                          page: AddCuponDialog(), barrierDismissible: true);
                    }),
                SizedBox(
                  height: 32,
                ),
                RoundedEdgesFlatButton(
                    title: "Show Add Order Details Admin Dialog",
                    onPressed: () {
                      showAlert(context,
                          page: AddOrderDetailsAdminDialog(),
                          barrierDismissible: true);
                    }),
                SizedBox(
                  height: 32,
                ),
                RoundedEdgesFlatButton(
                    title: "Show Add Order2 Details Admin Dialog",
                    onPressed: () {
                      showAlert(context,
                          page: Test(),
                          barrierDismissible: true,
                          isBuilderChild: false);
                    }),
                SizedBox(
                  height: 32,
                ),
                RoundedEdgesFlatButton(
                    title: "Show Advanced Search",
                    onPressed: () {
                      showMyBottomSheet(context, page: AdvancedSearchModal());
                    }),
                SizedBox(
                  height: 32,
                ),
                RoundedEdgesFlatButton(
                    title: "Show Advanced Search",
                    onPressed: () {
                      showMyBottomSheet(context, page: SortByModal());
                    }),
                SizedBox(
                  height: 32,
                ),
                RoundedEdgesFlatButton(
                    title: "Show Add To Cart",
                    onPressed: () {
                      showMyBottomSheet(context, page: AddToCartModal());
                    }),
                SizedBox(
                  height: 32,
                ),
                RoundedEdgesFlatButton(
                    title: "GO TO TEXT, DROPDOWUN",
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => CustomTextFieldPage()));
                    }),
                SizedBox(
                  height: 32,
                ),
                RoundedEdgesFlatButton(
                    title: "GO TO EDIT NEW ADDRESS SCREEN",
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => EditNewddress()));
                    }),
                SizedBox(
                  height: 32,
                ),
                RoundedEdgesFlatButton(
                    title: "GO TO EDIT PAY ACCOUNT",
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => EditPayAccount()));
                    }),
                SizedBox(
                  height: 32,
                ),
                RoundedEdgesFlatButton(
                    title: "GO TO PROFILE",
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ProfilePage()));
                    }),
                SizedBox(
                  height: 32,
                ),
                RoundedEdgesFlatButton(
                    title: "GO TO CART SHOPPPING",
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => SelectShipping()));
                    }),
                SizedBox(
                  height: 32,
                ),
                RoundedEdgesFlatButton(
                    title: "GO TO SELLER REGISTRATION",
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => SellerRegistration()));
                    }),
                SizedBox(
                  height: 32,
                ),
                RoundedEdgesFlatButton(
                    title: "GO TO CONTACT US PAGE",
                    onPressed: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => ContactUS()));
                    }),
                SizedBox(
                  height: 32,
                ),
                RoundedEdgesFlatButton(
                    title: "GO TO SIDE MENUE CONTINUE",
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => TestSideMenue()));
                    }),
                SizedBox(
                  height: 32,
                ),
                RoundedEdgesFlatButton(
                    title: "GO TO SELECT SHIPPING METHOD",
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ShippingMethodPage()));
                    }),
                SizedBox(
                  height: 32,
                ),
              ],
            ),
          )),
    );
  }
}
