import 'package:drawerbehavior/drawerbehavior.dart';
import 'package:drawerbehavior/menu_screen.dart';
import 'package:flutter/material.dart';

class TestSideMenue extends StatefulWidget {
  @override
  _TestSideMenueState createState() => _TestSideMenueState();
}

class _TestSideMenueState extends State<TestSideMenue> {
  List<MenuItem> items;
  Menu menu;
  String selectedMenuItemId;
  DrawerScaffoldController controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    items = new List();
    controller = DrawerScaffoldController();
    items.add(MenuItem(icon: Icons.add, id: "1", title: "FAQS"));
    items.add(MenuItem(icon: Icons.settings, id: "2", title: "SETTINGS"));
    items.add(MenuItem(icon: Icons.privacy_tip, id: "3", title: "PRIVACY"));
    items.add(MenuItem(icon: Icons.person, id: "4", title: "CONTACT US"));
    items.add(MenuItem(
        icon: Icons.branding_watermark, id: "5", title: "TERMS & CONDITIONS"));
    items.add(MenuItem(icon: Icons.settings, id: "6", title: "العربية"));
    menu = new Menu(items: items);
    selectedMenuItemId = "1";
  }

  @override
  Widget build(BuildContext context) {
    return new DrawerScaffold(
      controller: controller,
      enableGestures: true,
      builder: (context, index) => Scaffold(
        appBar: AppBar(
          title: Text('SIDE MENUE'),
          leading: GestureDetector(
              onTap: () {
                controller.toggle();
              },
              child: Icon(
                Icons.menu,
              )),
        ),
      ),
      drawers: [
        SideDrawer(
          percentage: 0.6,
          alignment: Alignment.centerLeft,
          duration: Duration(milliseconds: 400),
          menu: menu,
          cornerRadius: 0,
          direction: Direction.left,
          animation: true,
          drawerWidth: 260,
          selectorColor: Colors.white,
          color: Color(0xFF121212),
          selectedItemId: selectedMenuItemId,
          onMenuItemSelected: (itemId) {
            print(itemId);
            setState(() {
              selectedMenuItemId = itemId;
            });
          },
          headerView: Container(
            width: double.infinity,
            padding: EdgeInsets.only(top: 32, left: 16, right: 16, bottom: 32),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Hello,",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
                Text(
                  "Mohamad Ali",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 19,
                      fontWeight: FontWeight.w600),
                ),
              ],
            ),
          ),
          footerView: Container(
            width: double.infinity,
            padding: EdgeInsets.only(top: 36, left: 16, right: 16, bottom: 22),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "FIND US HERE",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  width: 32,
                ),
                Expanded(
                    child: Container(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.mail,
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: 32,
                      ),
                      Icon(
                        Icons.video_library,
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: 32,
                      ),
                      Icon(
                        Icons.mail,
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: 32,
                      ),
                      Icon(
                        Icons.video_library,
                        color: Colors.white,
                      ),
                    ],
                  ),
                )),
              ],
            ),
          ),
        )
      ],
    );
  }
}
