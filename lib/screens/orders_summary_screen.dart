import 'package:flutter/material.dart';

import '../models/product_model.dart';
import '../widgets/widgets.dart';

class OrdersSummaryScreen extends StatefulWidget {
  @override
  _OrdersSummaryScreenState createState() => _OrdersSummaryScreenState();
}

class _OrdersSummaryScreenState extends State<OrdersSummaryScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        children: [
          Image(
            height: 55,
            image: AssetImage('assets/images/icons/ooddss_logo.png'),
          ),
          SizedBox(height: 25.0),
          Text(
            'June 07, 2020, 11:45pm',
            style: TextStyle(
              color: Color(0xFF6F7071),
              fontSize: 13.0,
            ),
          ),
          Divider(thickness: 1.2),
          Text(
            'SHIPPING METHOD(S)',
            style: TextStyle(
              fontSize: 15.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 10.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 80.0,
                padding: EdgeInsets.symmetric(
                  horizontal: 18.0,
                  vertical: 10.0,
                ),
                decoration: BoxDecoration(
                  color: Color(0xFF0F6DA9).withOpacity(0.15),
                  borderRadius: BorderRadius.circular(9.0),
                ),
                child: Image(
                  height: 20.0,
                  image: AssetImage(
                    'assets/images/icons/truck_blue.png',
                  ),
                ),
              ),
              Text(
                'OODDSS Fast Shipping',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
          SizedBox(height: 15.0),
          ProductsListCard(
            products: products,
            hasImage: false,
          ),
        ],
      ),
    );
  }
}
