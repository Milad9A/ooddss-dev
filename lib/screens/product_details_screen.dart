import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../widgets/widgets.dart';
import 'store_products_screen.dart';

class ProductDetailsScreen extends StatefulWidget {
  @override
  _ProductDetailsScreenState createState() => _ProductDetailsScreenState();
}

class _ProductDetailsScreenState extends State<ProductDetailsScreen> {
  List<Widget> images = [
    FadeInImage(
      fit: BoxFit.fill,
      placeholder: AssetImage(
        'assets/images/icons/test_img.jpg',
      ),
      image: AssetImage('assets/images/icons/shose_test.png'),
    ),
    FadeInImage(
      fit: BoxFit.fill,
      placeholder: AssetImage(
        'assets/images/icons/test_img.jpg',
      ),
      image: AssetImage('assets/images/icons/shose_test.png'),
    ),
    FadeInImage(
      fit: BoxFit.fill,
      placeholder: AssetImage(
        'assets/images/icons/test_img.jpg',
      ),
      image: AssetImage('assets/images/icons/shose_test.png'),
    ),
    FadeInImage(
      fit: BoxFit.fill,
      placeholder: AssetImage(
        'assets/images/icons/test_img.jpg',
      ),
      image: AssetImage('assets/images/icons/shose_test.png'),
    ),
  ];
  List<Widget> colors = [
    TwoColorsCircle(color1: Colors.red[900], color2: Colors.blue[900]),
    OneColorCircle(color: Colors.black),
    OneColorCircle(color: Colors.pinkAccent),
    TwoColorsCircle(color1: Colors.purple, color2: Colors.white),
    TwoColorsCircle(color1: Colors.greenAccent, color2: Colors.orangeAccent),
    OneColorCircle(color: Colors.green),
    OneColorCircle(color: Colors.yellow),
  ];
  int _current = 0;

  int _selectedTap = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff9fafc),
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width / 3,
              child: Align(
                alignment: Alignment.centerLeft,
                child: Image(
                    image:
                        ExactAssetImage('assets/images/icons/arrow_left.png'),
                    height: 20.0,
                    width: 20.0,
                    alignment: FractionalOffset.center),
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width / 3,
              child: Align(
                alignment: Alignment.center,
                child: Image(
                    image: ExactAssetImage('assets/images/icons/nike_logo.png'),
                    height: 100.0,
                    width: 100.0,
                    alignment: FractionalOffset.center),
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.centerRight,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image(
                        image: ExactAssetImage(
                            'assets/images/icons/share_icon.png'),
                        height: 15.0,
                        width: 15.0,
                        alignment: FractionalOffset.center),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text(
                        'SHARE',
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            color: Color(0xff0f6da9),
                            fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 41.67, 0, 35.32),
              child: Container(
                  child: CarouselSlider(
                options: CarouselOptions(
                    height: 181.32,
                    autoPlay: false,
                    enlargeCenterPage: true,
                    aspectRatio: 2.0,
                    onPageChanged: (index, reason) {
                      setState(() {
                        _current = index;
                      });
                    }),
                items: images,
              )),
            ),
            Padding(
              padding: const EdgeInsets.all(28.67),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: images.map((url) {
                  int index = images.indexOf(url);
                  return Container(
                    width: 3.32,
                    height: 3.32,
                    margin: EdgeInsets.symmetric(horizontal: 2.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _current == index
                          ? Color(0xff0f6da9)
                          : Color.fromRGBO(0, 0, 0, 0.3),
                    ),
                  );
                }).toList(),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 21.67),
              child: Container(
                  width: 21.34,
                  height: 19,
                  child: Image.asset('assets/images/icons/blank_heart.png')),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 20),
              child: Text(
                'Shoes in Red&Black',
                style: TextStyle(
                    color: Color(0xff282828),
                    fontFamily: 'Grava',
                    fontWeight: FontWeight.w500,
                    fontSize: 16.67),
              ),
            ),
            Text(
              '35.000 KWD',
              style: TextStyle(
                decoration: TextDecoration.lineThrough,
                fontWeight: FontWeight.w500,
                fontSize: 11.93,
                fontFamily: 'Grava',
                color: Color(0xbb353535),
              ),
            ),
            RichText(
                textAlign: TextAlign.start,
                text: TextSpan(
                    text: '25.99',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 20.82,
                      fontFamily: 'Grava',
                      color: Colors.black,
                    ),
                    children: [
                      TextSpan(
                        text: ' KWD',
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 11.1,
                          fontFamily: 'Grava',
                          color: Colors.black,
                        ),
                      ),
                    ])),
            Padding(
              padding: const EdgeInsets.fromLTRB(19.32, 10, 19.3, 19.3),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Color :   ',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 13.89,
                      fontFamily: 'Grava',
                      color: Colors.black,
                    ),
                  ),
                  Text(
                    'Red',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 13.89,
                      fontFamily: 'Grava',
                      color: Color(0xbb353535),
                    ),
                  )
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 24.3),
                child: Wrap(
                  alignment: WrapAlignment.start,
                  runSpacing: 20,
                  spacing: 25,
                  children: colors,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 33.66),
              child: Stack(
                children: [
                  Align(
                    alignment: _selectedTap == 1
                        ? Alignment.centerLeft
                        : Alignment.centerRight,
                    child: Container(
                        height: 41.66 / 2,
                        width: MediaQuery.of(context).size.width / 2,
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xff578fdd).withOpacity(0.07),
                                  spreadRadius: 0,
                                  blurRadius: 10,
                                  offset:
                                      Offset(_selectedTap == 1 ? -20 : 20, 30))
                            ],
                            borderRadius:
                                BorderRadius.all(Radius.circular(50 / 4)))),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Container(
                      height: 41.66,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              BorderRadius.all(Radius.circular(50 / 4))),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                _selectedTap = 1;
                              });
                            },
                            child: Text(
                              'INFORMATION',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Color(0xff393d49),
                                  fontWeight: FontWeight.w400,
                                  fontSize: 13.89),
                            ),
                          ),
                          Text('|',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 13.89)),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                _selectedTap = 2;
                              });
                            },
                            child: Text('PRODUCT DETAILS',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Color(0xff8e8f93),
                                    fontWeight: FontWeight.w400,
                                    fontSize: 13.89)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),

            ///information section
            Padding(
              padding: const EdgeInsets.fromLTRB(49.66, 0, 49.66, 34.33),
              child: Container(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 4,
                            child: Text(
                              'SKU',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 13.89,
                                  color: Color(0xff282828)),
                            ),
                          ),
                          Expanded(
                              flex: 1,
                              child: Text(
                                ':',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 13.89),
                              )),
                          Expanded(
                              flex: 9,
                              child: Text(
                                '9038992',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 13.89),
                              )),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 4,
                            child: Text(
                              'Shipping ',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 13.89,
                                  color: Color(0xff282828)),
                            ),
                          ),
                          Expanded(
                              flex: 1,
                              child: Text(
                                ':',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 13.89),
                              )),
                          Expanded(
                              flex: 9,
                              child: Text(
                                'ooddss Fast Delivery',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 13.89),
                              )),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 4,
                            child: Text(
                              'Delivery ',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 13.89,
                                  color: Color(0xff282828)),
                            ),
                          ),
                          Expanded(
                              flex: 1,
                              child: Text(
                                ':',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 13.89),
                              )),
                          Expanded(
                              flex: 9,
                              child: Text(
                                '7 Days',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 13.89),
                              )),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 4,
                            child: Text(
                              'Country',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 13.89,
                                  color: Color(0xff282828)),
                            ),
                          ),
                          Expanded(
                              flex: 1,
                              child: Text(
                                ':',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 13.89),
                              )),
                          Expanded(
                              flex: 9,
                              child: Text(
                                'Kuwait',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 13.89),
                              )),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),

            /// buttons section
            Padding(
              padding: const EdgeInsets.only(bottom: 13.33),
              child: RoundedEdgesFlatButton(
                title: 'BUY DIRECT',
                onPressed: () {},
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 37.66),
              child: RoundedEdgesFlatButton(
                title: 'ADD TO CART',
                onPressed: () {},
                color: Colors.black,
              ),
            ),

            ///sold-by section
            Padding(
              padding: const EdgeInsets.fromLTRB(16.3, 0, 16.3, 16.3),
              child: Divider(
                thickness: 2,
              ),
            ),

            Padding(
              padding: const EdgeInsets.fromLTRB(18.33, 0, 0, 20),
              child: Align(
                alignment: Alignment.bottomLeft,
                child: Text(
                  'SOLD BY :',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 15.27,
                    fontFamily: 'Grava',
                    color: Colors.black,
                  ),
                ),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(bottom: 20, left: 16.33, right: 19.33),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: FadeInImage(
                      fit: BoxFit.cover,
                      placeholder:
                          AssetImage('assets/images/icons/test_img.jpg'),
                      image: AssetImage('assets/images/icons/teddy.png'),
                    ),
                    height: 72,
                    width: 85,
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.22),
                          blurRadius: 3,
                          spreadRadius: 0.07,
                          offset: Offset(-1.33, 2.78)),
                    ], borderRadius: BorderRadius.all(Radius.circular(15))),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 16.33),
                            child: Text(
                              'Teddy Enterprises',
                              style: TextStyle(
                                  color: Color(0xff282828),
                                  fontWeight: FontWeight.w500,
                                  fontSize: 15),
                            ),
                          ),
                          Container(
                            child: RoundedEdgesOutlineButton(
                                height: 38.67,
                                borderColor: Color(0xff0f6da9),
                                title: 'SEE ALL PRODUCTS',
                                textColor: Color(0xff0f6da9),
                                onPressed: () {}),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),

            /// you-might-also-like section
            Padding(
              padding: const EdgeInsets.fromLTRB(16.3, 0, 16.3, 16.3),
              child: Divider(
                thickness: 2,
              ),
            ),

            Padding(
              padding: const EdgeInsets.fromLTRB(16.0, 0, 16, 4),
              child: Align(
                alignment: Alignment.bottomLeft,
                child: Text(
                  'YOU MAY ALSO LIKE',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 15.27,
                    fontFamily: 'Grava',
                    color: Colors.black,
                  ),
                ),
              ),
            ),
            Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height / 1.6,
                child: ProductsGrid()),
          ],
        ),
      ),
    );
  }
}

class OneColorCircle extends StatelessWidget {
  OneColorCircle({Key key, @required this.color}) : super(key: key);
  Color color;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ClipRRect(
        child: Container(
          width: 40,
          height: 40,
          decoration: BoxDecoration(
              color: Colors.transparent,
              borderRadius: BorderRadius.circular(100),
              border: Border.all(color: Colors.red)),
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Container(
              decoration: BoxDecoration(
                  color: color, borderRadius: BorderRadius.circular(100)),
            ),
          ),
        ),
      ),
    );
  }
}

class TwoColorsCircle extends StatelessWidget {
  TwoColorsCircle({Key key, @required this.color1, @required this.color2})
      : assert(color1 != null, color2 != null),
        super(key: key);

  Color color1;
  Color color2;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ClipRRect(
        child: Container(
          width: 40,
          height: 40,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(100),
              border: Border.all(color: Colors.red)),
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        color: color1,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        color: color2,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
