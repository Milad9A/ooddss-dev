import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

class StoresScreen extends StatefulWidget {
  @override
  _StoresScreenState createState() => _StoresScreenState();
}

class _StoresScreenState extends State<StoresScreen>
    with SingleTickerProviderStateMixin {
  TextEditingController _controller = TextEditingController();
  AnimationController controller;
  Animation<double> animation;
  String _selectedCategory = 'Beauty';

  @override
  void initState() {
    controller = AnimationController(
        reverseDuration: Duration(milliseconds: 150),
        duration: Duration(milliseconds: 150),
        vsync: this);
    animation = new Tween(begin: 18.05, end: 25.0).animate(controller)
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        }
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 21.66, top: 21.66),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(
                  bottom: 28.66,
                  right: 21.66,
                ),
                child: Row(
                  children: [
                    Expanded(
                        child: CustomTextFields(
                      _controller,
                      padding: EdgeInsets.only(left: 0),
                    )),
                    Padding(
                      padding: const EdgeInsets.only(left: 25.66),
                      child: Image.asset(
                        'assets/images/icons/mike.png',
                        height: 25.66,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 25.33),
                child: Container(
                  height: 100,
                  width: MediaQuery.of(context).size.width,
                  child: ListView(
                    shrinkWrap: false,
                    scrollDirection: Axis.horizontal,
                    children: [
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            changeCategoryTitle(title: 'Beauty');
                          });
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(right: 40),
                          child: Column(
                            children: [
                              Expanded(
                                child: Image.asset(
                                  'assets/images/icons/beauty.png',
                                  height: 50,
                                  width: 50,
                                ),
                              ),
                              SizedBox(
                                width: 1,
                                height: 8,
                              ),
                              Expanded(
                                  child: Text(
                                'Beauty',
                                style: TextStyle(
                                    fontSize: 12.5,
                                    fontWeight: FontWeight.w400,
                                    color: _selectedCategory == 'Beauty'
                                        ? Color(0xff353535)
                                        : Colors.black),
                              ))
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            changeCategoryTitle(title: 'Sports');
                          });
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(right: 40),
                          child: Column(
                            children: [
                              Expanded(
                                child: Image.asset(
                                  'assets/images/icons/beauty.png',
                                  height: 50,
                                  width: 50,
                                ),
                              ),
                              SizedBox(
                                width: 1,
                                height: 8,
                              ),
                              Expanded(
                                  child: Text(
                                'Sports',
                                style: TextStyle(
                                    fontSize: 12.5,
                                    fontWeight: FontWeight.w400,
                                    color: _selectedCategory == 'Sports'
                                        ? Color(0xff353535)
                                        : Colors.black),
                              ))
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            changeCategoryTitle(title: 'Mobile');
                          });
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(right: 40),
                          child: Column(
                            children: [
                              Expanded(
                                child: Image.asset(
                                  'assets/images/icons/beauty.png',
                                  height: 50,
                                  width: 50,
                                ),
                              ),
                              SizedBox(
                                width: 1,
                                height: 8,
                              ),
                              Expanded(
                                  child: Text(
                                'Mobile',
                                style: TextStyle(
                                    fontSize: 12.5,
                                    fontWeight: FontWeight.w400,
                                    color: _selectedCategory == 'Mobile'
                                        ? Color(0xff353535)
                                        : Colors.black),
                              ))
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            changeCategoryTitle(title: 'Baby');
                          });
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(right: 40),
                          child: Column(
                            children: [
                              Expanded(
                                child: Image.asset(
                                  'assets/images/icons/beauty.png',
                                  height: 50,
                                  width: 50,
                                ),
                              ),
                              SizedBox(
                                width: 1,
                                height: 8,
                              ),
                              Expanded(
                                  child: Text(
                                'Baby',
                                style: TextStyle(
                                    fontSize: 12.5,
                                    fontWeight: FontWeight.w400,
                                    color: _selectedCategory == 'Baby'
                                        ? Color(0xff353535)
                                        : Colors.black),
                              ))
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            changeCategoryTitle(title: 'Electronics');
                          });
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(right: 40),
                          child: Column(
                            children: [
                              Expanded(
                                child: Image.asset(
                                  'assets/images/icons/beauty.png',
                                  height: 50,
                                  width: 50,
                                ),
                              ),
                              SizedBox(
                                width: 1,
                                height: 8,
                              ),
                              Expanded(
                                  child: Text(
                                'Electronics',
                                style: TextStyle(
                                    fontSize: 12.5,
                                    fontWeight: FontWeight.w400,
                                    color: _selectedCategory == 'Electronics'
                                        ? Color(0xff353535)
                                        : Colors.black),
                              ))
                            ],
                          ),
                        ),
                      ),
                      // Image.asset(
                      //   'assets/images/icons/beauty.png',
                      //   height: 5,width: 5,
                      // )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  bottom: 20.0,
                  right: 21.66,
                ),
                child: Container(
                  height: 41.66,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(16.66)),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            blurRadius: 4.33,
                            spreadRadius: 0.9,
                            offset: Offset(0, 1.66))
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: Row(
                      children: [
                        Expanded(
                            flex: 3,
                            child: Text(
                              _selectedCategory,
                              style: TextStyle(
                                fontFamily: 'Grava',
                                fontSize: animation.value,
                                fontWeight: FontWeight.w700,
                              ),
                            )),
                        Text('|'),
                        Padding(
                          padding: const EdgeInsets.only(left: 23.0),
                          child: Image.asset(
                            'assets/images/icons/filter.png',
                            height: 15.33,
                            width: 15.33,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 15),
                          child: Text(
                            'FILTER',
                            style: TextStyle(
                                fontFamily: 'Grava',
                                fontSize: 13.89,
                                fontWeight: FontWeight.w400,
                                color: Color(0xff464a50)),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  right: 21.66,
                ),
                child: GridView.extent(
                  maxCrossAxisExtent: MediaQuery.of(context).size.width / 2.7,
                  childAspectRatio: 0.85,
                  physics: NeverScrollableScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  crossAxisSpacing: 21.33,
                  mainAxisSpacing: 21.33,
                  children: [
                    StoresScreenGridITem(
                      image: 'assets/images/icons/nike_logo.png',
                      name: 'Nike',
                    ),
                    StoresScreenGridITem(
                      image: 'assets/images/icons/Everlast.png',
                      name: 'Everlast',
                    ),
                    StoresScreenGridITem(
                        image: 'assets/images/icons/centerpoint.png',
                        name: 'centerpoint'),
                    StoresScreenGridITem(
                        image: 'assets/images/icons/adidas.png',
                        name: 'adidas'),
                    StoresScreenGridITem(
                        image: 'assets/images/icons/puma.png', name: 'puma'),
                    StoresScreenGridITem(
                        image: 'assets/images/icons/hm.png', name: 'H & M'),
                    StoresScreenGridITem(
                        image: 'assets/images/icons/mcdavid.png',
                        name: 'mcdavid'),
                    StoresScreenGridITem(
                        image: 'assets/images/icons/shock.png', name: 'shock'),
                    StoresScreenGridITem(
                        image: 'assets/images/icons/puma.png', name: 'puma'),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void changeCategoryTitle({String title}) {
    controller?.forward();
    _selectedCategory = title;
  }
}

class StoresScreenGridITem extends StatelessWidget {
  final String image;
  final String name;

  const StoresScreenGridITem({
    Key key,
    this.image,
    this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(16.66)),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.1),
                        blurRadius: 4.33,
                        spreadRadius: 0.9,
                        offset: Offset(0, 1.66))
                  ]),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(
                  image,
                  fit: BoxFit.contain,
                ),
              )),
        ),
        SizedBox(
          height: 8,
        ),
        Text(
          name,
          style: TextStyle(
              fontSize: 13.89,
              fontFamily: 'Grava',
              fontWeight: FontWeight.w400),
        )
      ],
    );
  }
}
