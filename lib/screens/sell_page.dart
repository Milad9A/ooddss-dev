import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../widgets/widgets.dart';

class SellPage extends StatefulWidget {
  @override
  _SellPageState createState() => _SellPageState();
}

class _SellPageState extends State<SellPage> {
  var _selectedTap = 1;

  TextEditingController _urlController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 32.66, top: 23.33),
                child: Stack(
                  children: [
                    Align(
                      alignment: _selectedTap == 1
                          ? Alignment.centerLeft
                          : Alignment.centerRight,
                      child: Container(
                          height: 41.66 / 2,
                          width: MediaQuery.of(context).size.width / 2,
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xff578fdd).withOpacity(0.07),
                                    spreadRadius: 0,
                                    blurRadius: 10,
                                    offset: Offset(
                                        _selectedTap == 1 ? -20 : 20, 30))
                              ],
                              borderRadius:
                                  BorderRadius.all(Radius.circular(50 / 4)))),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Container(
                        height: 41.66,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.all(Radius.circular(50 / 4))),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  _selectedTap = 1;
                                });
                              },
                              child: Text(
                                'MY ACCOUNT',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Color(0xff393d49),
                                    fontWeight: FontWeight.w400,
                                    fontSize: 13.89),
                              ),
                            ),
                            Text('|',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 13.89)),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  _selectedTap = 2;
                                });
                              },
                              child: Text('MY STORE',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Color(0xff8e8f93),
                                      fontWeight: FontWeight.w400,
                                      fontSize: 13.89)),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 20.33, right: 29.33, bottom: 30.66),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                        child: CustomTextFields(
                      _urlController,
                      padding: EdgeInsets.only(right: 15),
                    )),
                    Container(
                      height: 25,
                      width: 25,
                      child: Image.asset(
                        'assets/images/icons/copy.png',
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(left: 0, right: 0, bottom: 23.33),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Expanded(
                      child: Column(
                        children: [
                          Container(
                            height: 53.33,
                            width: 53.33,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xfff160c2)),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 13.33, horizontal: 18.33),
                              child: Image.asset(
                                'assets/images/icons/ico_edit.png',
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 9.66,
                            width: 1,
                          ),
                          Text(
                            'Edit Store',
                            style: TextStyle(
                                fontSize: 12.5,
                                fontFamily: 'Grava',
                                fontWeight: FontWeight.w400,
                                color: Color(0xff353535)),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          Container(
                            height: 53.33,
                            width: 53.33,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xff45aae9)),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 13.33, horizontal: 18.33),
                              child: Image.asset(
                                'assets/images/icons/ico_new_product.png',
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 9.66,
                            width: 1,
                          ),
                          Text(
                            'New Product',
                            style: TextStyle(
                                fontSize: 12.5,
                                fontFamily: 'Grava',
                                fontWeight: FontWeight.w400,
                                color: Color(0xff353535)),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          Container(
                            height: 53.33,
                            width: 53.33,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xff9fcc52)),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 13.33, horizontal: 18.33),
                              child: Image.asset(
                                'assets/images/icons/ico_history.png',
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 9.66,
                            width: 1,
                          ),
                          Text(
                            'Sold History',
                            style: TextStyle(
                                fontSize: 12.5,
                                fontFamily: 'Grava',
                                fontWeight: FontWeight.w400,
                                color: Color(0xff353535)),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(left: 0, right: 0, bottom: 39.33),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Column(
                        children: [
                          Container(
                            height: 53.33,
                            width: 53.33,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xff5471ee)),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 13.33, horizontal: 18.33),
                              child: Image.asset(
                                'assets/images/icons/pay_account.png',
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 9.66,
                            width: 1,
                          ),
                          Text(
                            'Pay Account \nDetails',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 12.5,
                                fontFamily: 'Grava',
                                fontWeight: FontWeight.w400,
                                color: Color(0xff353535)),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        children: [
                          Container(
                            height: 53.33,
                            width: 53.33,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xfff16066)),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 13.33, horizontal: 18.33),
                              child: Image.asset(
                                'assets/images/icons/ico_wallet.png',
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 9.66,
                            width: 1,
                          ),
                          Text(
                            'Wallet',
                            style: TextStyle(
                                fontSize: 12.5,
                                fontFamily: 'Grava',
                                fontWeight: FontWeight.w400,
                                color: Color(0xff353535)),
                          )
                        ],
                      ),
                    ),
                    Expanded(flex: 1, child: Container()),
                  ],
                ),
              ),
              SellPageProductItem(),
              SellPageProductItem(),
              SellPageProductItem()
            ],
          ),
        ));
  }
}

class SellPageProductItem extends StatelessWidget {
  const SellPageProductItem({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 15.66),
            child: Row(
              children: [
                Container(
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.22),
                            offset: Offset(0, 2.33),
                            spreadRadius: 0.7,
                            blurRadius: 3.33)
                      ],
                      color: Colors.blue,
                      borderRadius: BorderRadius.all(Radius.circular(11.66))),
                  height: 87.33,
                  width: 87.33,
                  child: Image.asset(
                    'assets/images/icons/shose_test.png',
                    fit: BoxFit.contain,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            'Barbie',
                            style: TextStyle(
                              fontSize: 13.89,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Image.asset(
                              'assets/images/icons/clock_orange.png',
                              height: 13.33,
                              width: 13.33,
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 48.33,
                        width: 1,
                      ),
                      Text(
                        '10 left',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 12.5,
                            color: Color(0xffff003c),
                            fontWeight: FontWeight.w400),
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        RichText(
                            textAlign: TextAlign.start,
                            text: TextSpan(
                                text: '25.990',
                                style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 13.63,
                                  fontFamily: 'Grava',
                                  color: Color(0xff282828),
                                ),
                                children: [
                                  TextSpan(
                                    text: ' KWD',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 7.27,
                                      fontFamily: 'Grava',
                                      color: Color(0xff282828),
                                    ),
                                  ),
                                ])),
                        SizedBox(
                          height: 38.33,
                          width: 1,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Image.asset(
                              'assets/images/icons/edit_square.png',
                              height: 23.66,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 30.33),
                              child: Image.asset(
                                'assets/images/icons/delete.png',
                                height: 23.66,
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 6.66),
            child: Text(
              '26th March 2020, 11:45 pm',
              style: TextStyle(
                  fontSize: 13.63,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff282828)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 21.66),
            child: Text(
              'Aramex, DHL',
              style: TextStyle(
                  fontSize: 13.63,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff0595ed)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: Divider(
              color: Color(0xffa4a4a4),
              thickness: 0.33,
            ),
          )
        ],
      ),
    );
  }
}
