import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

class FiltersScreen extends StatefulWidget {
  @override
  _FiltersScreenState createState() => _FiltersScreenState();
}

class _FiltersScreenState extends State<FiltersScreen> {
  void onCheckChanged(bool value) {}
  bool checked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            FilterExpandableView(
              title: Text(
                'ARABIC TEXTBOOK',
                style: TextStyle(
                    fontFamily: 'Grava',
                    fontWeight: FontWeight.w400,
                    fontSize: 15),
              ),
              onCheckChanged: (bool value) {
                print(value);
              },
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(64, 8, 0, 16),
                  child: Container(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                                flex: 8,
                                child: Text(
                                  'TextBook for Grade 3',
                                  style: TextStyle(
                                      color: Color(0xaa353535),
                                      fontFamily: 'Grava',
                                      fontWeight: FontWeight.w400,
                                      fontSize: 15),
                                )),
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 13.0),
                                  child: CircularCheckBox(
                                      value: checked,
                                      materialTapTargetSize:
                                          MaterialTapTargetSize.padded,
                                      onChanged: (bool value) {
                                        setState(() {
                                          checked = !checked;
                                        });
                                        onCheckChanged(value);
                                      }),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: Divider(
                            color: Color(0xaa353535),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(64, 8, 0, 16),
                  child: Container(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                                flex: 8,
                                child: Text(
                                  'TextBook for Grade 3',
                                  style: TextStyle(
                                      color: Color(0xaa353535),
                                      fontFamily: 'Grava',
                                      fontWeight: FontWeight.w400,
                                      fontSize: 15),
                                )),
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 13.0),
                                  child: CircularCheckBox(
                                      value: checked,
                                      materialTapTargetSize:
                                          MaterialTapTargetSize.padded,
                                      onChanged: (bool value) {
                                        setState(() {
                                          checked = !checked;
                                        });
                                        onCheckChanged(value);
                                      }),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: Divider(
                            color: Color(0xaa353535),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(64, 8, 0, 16),
                  child: Container(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                                flex: 8,
                                child: Text(
                                  'TextBook for Grade 3',
                                  style: TextStyle(
                                      color: Color(0xaa353535),
                                      fontFamily: 'Grava',
                                      fontWeight: FontWeight.w400,
                                      fontSize: 15),
                                )),
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 13.0),
                                  child: CircularCheckBox(
                                      value: checked,
                                      materialTapTargetSize:
                                          MaterialTapTargetSize.padded,
                                      onChanged: (bool value) {
                                        setState(() {
                                          checked = !checked;
                                        });
                                        onCheckChanged(value);
                                      }),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: Divider(
                            color: Color(0xaa353535),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
            FilterExpandableView(
              title: Text(
                'ARABIC TEXTBOOK',
                style: TextStyle(
                    fontFamily: 'Grava',
                    fontWeight: FontWeight.w400,
                    fontSize: 15),
              ),
              onCheckChanged: (bool value) {
                print(value);
              },
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(64, 8, 0, 16),
                  child: Container(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                                flex: 8,
                                child: Text(
                                  'TextBook for Grade 3',
                                  style: TextStyle(
                                      color: Color(0xaa353535),
                                      fontFamily: 'Grava',
                                      fontWeight: FontWeight.w400,
                                      fontSize: 15),
                                )),
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 13.0),
                                  child: CircularCheckBox(
                                      value: checked,
                                      materialTapTargetSize:
                                          MaterialTapTargetSize.padded,
                                      onChanged: (bool value) {
                                        setState(() {
                                          checked = !checked;
                                        });
                                        onCheckChanged(value);
                                      }),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: Divider(
                            color: Color(0xaa353535),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(64, 8, 0, 16),
                  child: Container(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                                flex: 8,
                                child: Text(
                                  'TextBook for Grade 3',
                                  style: TextStyle(
                                      color: Color(0xaa353535),
                                      fontFamily: 'Grava',
                                      fontWeight: FontWeight.w400,
                                      fontSize: 15),
                                )),
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 13.0),
                                  child: CircularCheckBox(
                                      value: checked,
                                      materialTapTargetSize:
                                          MaterialTapTargetSize.padded,
                                      onChanged: (bool value) {
                                        setState(() {
                                          checked = !checked;
                                        });
                                        onCheckChanged(value);
                                      }),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: Divider(
                            color: Color(0xaa353535),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(64, 8, 0, 16),
                  child: Container(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                                flex: 8,
                                child: Text(
                                  'TextBook for Grade 3',
                                  style: TextStyle(
                                      color: Color(0xaa353535),
                                      fontFamily: 'Grava',
                                      fontWeight: FontWeight.w400,
                                      fontSize: 15),
                                )),
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 13.0),
                                  child: CircularCheckBox(
                                      value: checked,
                                      materialTapTargetSize:
                                          MaterialTapTargetSize.padded,
                                      onChanged: (bool value) {
                                        setState(() {
                                          checked = !checked;
                                        });
                                        onCheckChanged(value);
                                      }),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: Divider(
                            color: Color(0xaa353535),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
            FilterExpandableView(
              title: Text(
                'ARABIC TEXTBOOK',
                style: TextStyle(
                    fontFamily: 'Grava',
                    fontWeight: FontWeight.w400,
                    fontSize: 15),
              ),
              onCheckChanged: (bool value) {
                print(value);
              },
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(64, 8, 0, 16),
                  child: Container(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                                flex: 8,
                                child: Text(
                                  'TextBook for Grade 3',
                                  style: TextStyle(
                                      color: Color(0xaa353535),
                                      fontFamily: 'Grava',
                                      fontWeight: FontWeight.w400,
                                      fontSize: 15),
                                )),
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 13.0),
                                  child: CircularCheckBox(
                                      value: checked,
                                      materialTapTargetSize:
                                          MaterialTapTargetSize.padded,
                                      onChanged: (bool value) {
                                        setState(() {
                                          checked = !checked;
                                        });
                                        onCheckChanged(value);
                                      }),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: Divider(
                            color: Color(0xaa353535),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(64, 8, 0, 16),
                  child: Container(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                                flex: 8,
                                child: Text(
                                  'TextBook for Grade 3',
                                  style: TextStyle(
                                      color: Color(0xaa353535),
                                      fontFamily: 'Grava',
                                      fontWeight: FontWeight.w400,
                                      fontSize: 15),
                                )),
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 13.0),
                                  child: CircularCheckBox(
                                      value: checked,
                                      materialTapTargetSize:
                                          MaterialTapTargetSize.padded,
                                      onChanged: (bool value) {
                                        setState(() {
                                          checked = !checked;
                                        });
                                        onCheckChanged(value);
                                      }),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: Divider(
                            color: Color(0xaa353535),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(64, 8, 0, 16),
                  child: Container(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                                flex: 8,
                                child: Text(
                                  'TextBook for Grade 3',
                                  style: TextStyle(
                                      color: Color(0xaa353535),
                                      fontFamily: 'Grava',
                                      fontWeight: FontWeight.w400,
                                      fontSize: 15),
                                )),
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 13.0),
                                  child: CircularCheckBox(
                                      value: checked,
                                      materialTapTargetSize:
                                          MaterialTapTargetSize.padded,
                                      onChanged: (bool value) {
                                        setState(() {
                                          checked = !checked;
                                        });
                                        onCheckChanged(value);
                                      }),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: Divider(
                            color: Color(0xaa353535),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
