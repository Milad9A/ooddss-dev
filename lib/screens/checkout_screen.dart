import 'package:flutter/material.dart';

import '../models/product_model.dart';
import '../widgets/widgets.dart';

class CheckoutScreen extends StatefulWidget {
  @override
  _CheckoutScreenState createState() => _CheckoutScreenState();
}

class _CheckoutScreenState extends State<CheckoutScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView(
        children: [
          Column(
            children: products
                .asMap()
                .map(
                  (index, product) => MapEntry(
                    index,
                    ShoppingCartTile(
                      product: product,
                      isLastTile: index == products.length - 1,
                    ),
                  ),
                )
                .values
                .toList(),
          ),
          CheckoutContainer(),
        ],
      ),
    );
  }
}
