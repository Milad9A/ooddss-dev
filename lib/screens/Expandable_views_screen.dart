import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

class ExpandableViewScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            // HistoryOrderCard(),
            AddressInformationExpandableContainer(),
            PaymentInformationExpandableContainer(
              fontSize: 15.0,
              kwdFontSize: 10,
            ),
          ],
        ),
      ),
    );
  }
}
