import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../models/transaction_model.dart';
import '../utils/constants.dart';
import '../widgets/widgets.dart';

class WalletScreen extends StatefulWidget {
  @override
  _WalletScreenState createState() => _WalletScreenState();
}

class _WalletScreenState extends State<WalletScreen> {
  var myGroup = AutoSizeGroup();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            Container(
              height: MediaQuery.of(context).size.height / 6,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width * 0.6,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AutoSizeText(
                              'OODDSS Commission (%)',
                              group: myGroup,
                              maxLines: 1,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            AutoSizeText(
                              'Total Product commission value',
                              group: myGroup,
                              maxLines: 1,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            AutoSizeText(
                              'Total Shipping commission value',
                              group: myGroup,
                              maxLines: 1,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            AutoSizeText(
                              'Total Order commission value',
                              group: myGroup,
                              maxLines: 1,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 18.0),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text(
                            ':',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(
                            ':',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(
                            ':',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(
                            ':',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        '5%',
                        style: TextStyle(
                          color: kOODDSSGrey,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.baseline,
                        children: [
                          Text(
                            '10.000',
                            style: TextStyle(
                              color: kOODDSSGrey,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            'KWD',
                            style: TextStyle(
                              fontSize: 7.0,
                              color: kOODDSSGrey,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.baseline,
                        children: [
                          Text(
                            '2.500',
                            style: TextStyle(
                              color: kOODDSSGrey,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            'KWD',
                            style: TextStyle(
                              fontSize: 7.0,
                              color: kOODDSSGrey,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.baseline,
                        children: [
                          Text(
                            '1.500',
                            style: TextStyle(
                              color: kOODDSSGrey,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            'KWD',
                            style: TextStyle(
                              fontSize: 7.0,
                              color: kOODDSSGrey,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(height: 4.0),
            Divider(
              indent: 4.0,
              endIndent: 4.0,
              thickness: 1.0,
            ),
            Container(
              height: MediaQuery.of(context).size.height / 4.5,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  WalletRow(
                    amount: '300.00',
                    title: 'Available Wallet Balance',
                  ),
                  WalletRow(
                    amount: '13.00',
                    title: 'Total Amount addedd to the Wallet',
                  ),
                  WalletRow(
                    amount: '33.00',
                    title: 'Total Amount deducted from Wallet',
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(18.0),
                  boxShadow: [
                    BoxShadow(),
                  ],
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(18.0),
                  child: Column(
                    children: [
                      Container(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 16.0,
                          vertical: 12.0,
                        ),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'STORE TRANSACTIONS',
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Expanded(
                        child: ListView.builder(
                          itemCount: transactions.length,
                          itemBuilder: (BuildContext context, int index) {
                            Transaction transaction = transactions[index];
                            return StoreTransactionsTile(
                              transaction: transaction,
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
