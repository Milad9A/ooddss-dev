import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import '../models/order_model.dart';
import '../models/product_model.dart';
import '../widgets/widgets.dart';

class OrderDetailsScreen2 extends StatefulWidget {
  final Order order;

  const OrderDetailsScreen2({
    this.order,
  });

  @override
  _OrderDetailsScreen2State createState() => _OrderDetailsScreen2State();
}

class _OrderDetailsScreen2State extends State<OrderDetailsScreen2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Ordered on 07-01-2020',
                style: TextStyle(
                  color: Color(0xFF6F7071),
                ),
              ),
              SizedBox(height: 8.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Order Status',
                            style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            'Payment Status',
                            style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(width: 25.0),
                      Column(
                        children: [
                          Text(
                            ':',
                            style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            ':',
                            style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.centerRight,
                    width: MediaQuery.of(context).size.width * 0.4,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AutoSizeText(
                          'Shipped',
                          maxLines: 1,
                          style: TextStyle(
                            fontSize: 15.0,
                          ),
                        ),
                        AutoSizeText(
                          'Awaiting Cash Payment',
                          maxLines: 1,
                          style: TextStyle(
                            fontSize: 15.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(height: 6.0),
          Divider(thickness: 1.0),
          SizedBox(height: 12.0),
          RoundedEdgesOutlineButton(
            title: 'TRACK ORDER',
            borderColor: Theme.of(context).accentColor,
            textColor: Theme.of(context).accentColor,
            onPressed: () {},
          ),
          SizedBox(height: 12.0),
          RoundedEdgesFlatButton(
            title: 'ORDER SUMMARY',
            margin: EdgeInsets.all(0),
            onPressed: () {},
          ),
          SizedBox(height: 25.0),
          Text(
            'STATUS UPDATE',
            style: TextStyle(
              fontSize: 15.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 10.0),
          CustomDropDownButton(
            title: 'Status',
            items: ['Complain'],
            onChange: (value) {},
          ),
          SizedBox(height: 12.0),
          CustomDropDownButton(
            title: 'Reason',
            items: ['Wrong Size'],
            onChange: (value) {},
          ),
          SizedBox(height: 14.0),
          ProductsListCard(
            products: products,
          ),
          SizedBox(height: 14.0),
          AddressInformationExpandableContainer(),
          SizedBox(height: 14.0),
          PaymentInformationExpandableContainer(
            fontSize: 14.0,
            kwdFontSize: 7.0,
          ),
          SizedBox(height: 14.0),
        ],
      ),
    );
  }
}
