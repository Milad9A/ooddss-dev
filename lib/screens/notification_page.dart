import 'package:flutter/material.dart';

import '../models/my_notification_model.dart';
import '../widgets/widgets.dart';

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => new _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  List<MyNotification> notifications;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    notifications = new List();
    notifications
        .add(new MyNotification("Back in Stock!", "Nike React Ait 450", true));
    notifications.add(new MyNotification("Fast Selling!",
        "The products in your wishlist are fast selling.", false));
    notifications.add(new MyNotification(
        "Order Shipped!", "Your order #VS3900 is on it' way yo you!", false));
    notifications.add(new MyNotification(
        "Back in Stock!",
        "Nike React Ait 450, Your order #VS3900 is on it' way yo you way yo you!",
        true));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: new AppBar(
        title: new Text("Notification Widgegt"),
      ),
      body: new SafeArea(
          top: false,
          bottom: false,
          child: ListView.builder(
            itemCount: notifications.length,
            itemBuilder: (BuildContext context, int index) {
              return NotificationListTile(notifications[index],
                  didChange: () {});
            },
          )),
    );
  }
}
