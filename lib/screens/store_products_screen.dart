import 'package:flutter/material.dart';

import '../widgets/widgets.dart';
import 'product_details_screen.dart';

class StoreProductsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ProductsGrid(),
    );
  }
}

class ProductsGrid extends StatelessWidget {
  const ProductsGrid({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 2,
      childAspectRatio: 260 / MediaQuery.of(context).size.width,
      mainAxisSpacing: 0,
      crossAxisSpacing: 0,
      children: [
        GestureDetector(
            onTap: () =>
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => ProductDetailsScreen(),
                )),
            child: ProductGridItem()),
        GestureDetector(
            onTap: () =>
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => ProductDetailsScreen(),
                )),
            child: ProductGridItem()),
        GestureDetector(
            onTap: () =>
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => ProductDetailsScreen(),
                )),
            child: ProductGridItem()),
      ],
      /*
    FilterExpandableView(
      title: Text(
        'ARABIC TEXTBOOK',
        style: TextStyle(
            fontFamily: 'Grava',
            fontWeight: FontWeight.w400,
            fontSize: 15),
      ),
      onCheckChanged: (bool value) {
        print(value);
      },
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(64, 8, 0, 16),
          child: Container(
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                        flex: 8,
                        child: Text(
                          'TextBook for Grade 3',
                          style: TextStyle(
                              color: Color(0xaa353535),
                              fontFamily: 'Grava',
                              fontWeight: FontWeight.w400,
                              fontSize: 15),
                        )),
                    Expanded(
                      flex: 2,
                      child: CircularCheckBox(
                          value: checked,
                          materialTapTargetSize:
                              MaterialTapTargetSize.padded,
                          onChanged: (bool value) {
                            setState(() {
                              checked = !checked;
                            });
                            onCheckChanged(value);
                          }),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: Divider(
                    color: Color(0xaa353535),
                  ),
                )
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(64, 8, 0, 16),
          child: Container(
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                        flex: 8,
                        child: Text(
                          'TextBook for Grade 3',
                          style: TextStyle(
                              color: Color(0xaa353535),
                              fontFamily: 'Grava',
                              fontWeight: FontWeight.w400,
                              fontSize: 15),
                        )),
                    Expanded(
                      flex: 2,
                      child: CircularCheckBox(
                          value: checked,
                          materialTapTargetSize:
                              MaterialTapTargetSize.padded,
                          onChanged: (bool value) {
                            setState(() {
                              checked = !checked;
                            });
                            onCheckChanged(value);
                          }),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: Divider(
                    color: Color(0xaa353535),
                  ),
                )
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(64, 8, 0, 16),
          child: Container(
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                        flex: 8,
                        child: Text(
                          'TextBook for Grade 3',
                          style: TextStyle(
                              color: Color(0xaa353535),
                              fontFamily: 'Grava',
                              fontWeight: FontWeight.w400,
                              fontSize: 15),
                        )),
                    Expanded(
                      flex: 2,
                      child: CircularCheckBox(
                          value: checked,
                          materialTapTargetSize:
                              MaterialTapTargetSize.padded,
                          onChanged: (bool value) {
                            setState(() {
                              checked = !checked;
                            });
                            onCheckChanged(value);
                          }),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: Divider(
                    color: Color(0xaa353535),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    ),
    FilterExpandableView(
      title: Text(
        'ARABIC TEXTBOOK',
        style: TextStyle(
            fontFamily: 'Grava',
            fontWeight: FontWeight.w400,
            fontSize: 15),
      ),
      onCheckChanged: (bool value) {
        print(value);
      },
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(64, 8, 0, 16),
          child: Container(
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                        flex: 8,
                        child: Text(
                          'TextBook for Grade 3',
                          style: TextStyle(
                              color: Color(0xaa353535),
                              fontFamily: 'Grava',
                              fontWeight: FontWeight.w400,
                              fontSize: 15),
                        )),
                    Expanded(
                      flex: 2,
                      child: CircularCheckBox(
                          value: checked,
                          materialTapTargetSize:
                              MaterialTapTargetSize.padded,
                          onChanged: (bool value) {
                            setState(() {
                              checked = !checked;
                            });
                            onCheckChanged(value);
                          }),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: Divider(
                    color: Color(0xaa353535),
                  ),
                )
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(64, 8, 0, 16),
          child: Container(
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                        flex: 8,
                        child: Text(
                          'TextBook for Grade 3',
                          style: TextStyle(
                              color: Color(0xaa353535),
                              fontFamily: 'Grava',
                              fontWeight: FontWeight.w400,
                              fontSize: 15),
                        )),
                    Expanded(
                      flex: 2,
                      child: CircularCheckBox(
                          value: checked,
                          materialTapTargetSize:
                              MaterialTapTargetSize.padded,
                          onChanged: (bool value) {
                            setState(() {
                              checked = !checked;
                            });
                            onCheckChanged(value);
                          }),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: Divider(
                    color: Color(0xaa353535),
                  ),
                )
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(64, 8, 0, 16),
          child: Container(
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                        flex: 8,
                        child: Text(
                          'TextBook for Grade 3',
                          style: TextStyle(
                              color: Color(0xaa353535),
                              fontFamily: 'Grava',
                              fontWeight: FontWeight.w400,
                              fontSize: 15),
                        )),
                    Expanded(
                      flex: 2,
                      child: CircularCheckBox(
                          value: checked,
                          materialTapTargetSize:
                              MaterialTapTargetSize.padded,
                          onChanged: (bool value) {
                            setState(() {
                              checked = !checked;
                            });
                            onCheckChanged(value);
                          }),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: Divider(
                    color: Color(0xaa353535),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    ),
    FilterExpandableView(
      title: Text(
        'ARABIC TEXTBOOK',
        style: TextStyle(
            fontFamily: 'Grava',
            fontWeight: FontWeight.w400,
            fontSize: 15),
      ),
      onCheckChanged: (bool value) {
        print(value);
      },
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(64, 8, 0, 16),
          child: Container(
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                        flex: 8,
                        child: Text(
                          'TextBook for Grade 3',
                          style: TextStyle(
                              color: Color(0xaa353535),
                              fontFamily: 'Grava',
                              fontWeight: FontWeight.w400,
                              fontSize: 15),
                        )),
                    Expanded(
                      flex: 2,
                      child: CircularCheckBox(
                          value: checked,
                          materialTapTargetSize:
                              MaterialTapTargetSize.padded,
                          onChanged: (bool value) {
                            setState(() {
                              checked = !checked;
                            });
                            onCheckChanged(value);
                          }),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: Divider(
                    color: Color(0xaa353535),
                  ),
                )
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(64, 8, 0, 16),
          child: Container(
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                        flex: 8,
                        child: Text(
                          'TextBook for Grade 3',
                          style: TextStyle(
                              color: Color(0xaa353535),
                              fontFamily: 'Grava',
                              fontWeight: FontWeight.w400,
                              fontSize: 15),
                        )),
                    Expanded(
                      flex: 2,
                      child: CircularCheckBox(
                          value: checked,
                          materialTapTargetSize:
                              MaterialTapTargetSize.padded,
                          onChanged: (bool value) {
                            setState(() {
                              checked = !checked;
                            });
                            onCheckChanged(value);
                          }),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: Divider(
                    color: Color(0xaa353535),
                  ),
                )
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(64, 8, 0, 16),
          child: Container(
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                        flex: 8,
                        child: Text(
                          'TextBook for Grade 3',
                          style: TextStyle(
                              color: Color(0xaa353535),
                              fontFamily: 'Grava',
                              fontWeight: FontWeight.w400,
                              fontSize: 15),
                        )),
                    Expanded(
                      flex: 2,
                      child: CircularCheckBox(
                          value: checked,
                          materialTapTargetSize:
                              MaterialTapTargetSize.padded,
                          onChanged: (bool value) {
                            setState(() {
                              checked = !checked;
                            });
                            onCheckChanged(value);
                          }),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: Divider(
                    color: Color(0xaa353535),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    ),
    */
    );
  }
}
