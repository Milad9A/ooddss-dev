import 'package:flutter/material.dart';

import '../models/product_model.dart';
import '../widgets/widgets.dart';

class OrderDetailsAdminScreen extends StatefulWidget {
  @override
  _OrderDetailsAdminScreenState createState() =>
      _OrderDetailsAdminScreenState();
}

class _OrderDetailsAdminScreenState extends State<OrderDetailsAdminScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        children: [
          SizedBox(height: 20.0),
          PaymentMethodRow(
            onChanged: (value) {},
          ),
          SizedBox(height: 20.0),
          PaymentDetails(),
          SizedBox(height: 20.0),
          ProductsListCard(products: products),
          SizedBox(height: 10.0),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(18.0),
              boxShadow: [
                BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0.0, 2.0),
                ),
              ],
            ),
            child: AddressInformationExpandableContainer(),
          ),
          SizedBox(height: 10.0),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(18.0),
              boxShadow: [
                BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0.0, 2.0),
                ),
              ],
            ),
            child: PaymentInformationExpandableContainer(
              fontSize: 15.0,
              kwdFontSize: 10,
            ),
          ),
          SizedBox(height: 20.0),
        ],
      ),
    );
  }
}
