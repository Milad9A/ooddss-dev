import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

class EditPayAccount extends StatefulWidget {
  @override
  _EditPayAccountState createState() => _EditPayAccountState();
}

class _EditPayAccountState extends State<EditPayAccount> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AppBar'),
      ),
      body: SafeArea(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: Container(
                padding: const EdgeInsets.only(left: 16.0, right: 16),
                child: Column(
                  children: [
                    SizedBox(
                      height: 22,
                    ),
                    Text(
                      'The OODDS Transaction fees will be outamatically deducted and '
                      'the remaining balance will be transferred you bank account.',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 15.5,
                        color: Colors.black,
                        height: 1.5,
                      ),
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    CustomTextFields(
                      TextEditingController(),
                      padding: EdgeInsets.all(0),
                      hintText: "Account Name",
                      validateFunction: (val) {
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    CustomTextFields(
                      TextEditingController(),
                      padding: EdgeInsets.all(0),
                      hintText: "Bank Name",
                      validateFunction: (val) {
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    CustomTextFields(
                      TextEditingController(),
                      padding: EdgeInsets.all(0),
                      hintText: "Account Number",
                      validateFunction: (val) {
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    CustomTextFields(
                      TextEditingController(),
                      padding: EdgeInsets.all(0),
                      hintText: "IBAN",
                      validateFunction: (val) {
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 62,
                    ),
                  ],
                ),
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              // fillOverscroll: true, // Set true to change overscroll behavior. Purely preference.
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  padding: const EdgeInsets.only(
                    left: 16.0,
                    right: 16,
                    bottom: 20,
                  ),
                  child:
                      RoundedEdgesFlatButton(title: "SAVE", onPressed: () {}),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
