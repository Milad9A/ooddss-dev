import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TextScreen1 extends StatefulWidget {
  @override
  _TextScreen1State createState() => _TextScreen1State();
}

class _TextScreen1State extends State<TextScreen1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView(
        padding: EdgeInsets.fromLTRB(20.0, 2.0, 20.0, 36.0),
        children: [
          Text(
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum ut tristique et egestas. Tempus egestas sed sed risus pretium. Diam quam nulla porttitor massa id neque aliquam vestibulum. Odio pellentesque diam volutpat commodo sed egestas egestas fringilla. Auctor augue mauris augue neque gravida in. Tortor vitae purus faucibus ornare suspendisse sed. A arcu cursus vitae congue mauris. Eget duis at tellus at urna condimentum. Enim sed faucibus turpis in eu mi bibendum neque. Scelerisque varius morbi enim nunc faucibus a pellentesque sit amet. Rhoncus urna neque viverra justo nec ultrices. Orci nulla pellentesque dignissim enim sit amet venenatis urna. Integer eget aliquet nibh praesent tristique magna sit. Tincidunt eget nullam non nisi est sit amet. Porttitor leo a diam sollicitudin. Urna id volutpat lacus laoreet non curabitur gravida. Sem viverra aliquet eget sit amet tellus cras adipiscing. Pellentesque massa placerat duis \n \nultricies lacus sed turpis. Natoque penatibus et magnis dis.  Elit duis tristique sollicitudinnibh sit amet commodo nulla. Volutpat consequat mauris nunc congue nisi vitae suscipit tellus mauris. Lobortis elementum nibh tellus molestie nunc. Eget egestas purus viverra accumsan in nisl nisi scelerisque. At varius vel pharetra vel. Viverra adipiscing at in tellus. Urna condimentum mattis pellentesque id nibh tortor id aliquet lectus. Nec feugiat in fermentum posuere urna. Cursus eget nunc scelerisque viverra mauris in aliquam sem. Rutrum tellus pellentesque eu tincidunt tortor aliquam nulla. Enim facilisis gravida neque convallis a cras. Diam vulputate ut pharetra sit amet aliquam id diam maecenas. Risus nec feugiat in fermentum posuere urna. Leo integer malesuada nunc vel risus commodo viverra. Pharetra pharetra massa massa ultricies mi. Consectetur adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Fames ac turpis egestas integer eget aliquet nibh praesent tristique. Justo laoreet sit amet cursus sit amet dictum sit amet. Gravida in fermentum et sollicitudin. Fermentum odio eu feugiat pretium nibh ipsum. Placerat duis ultricies lacus sed. Egestas diam in arcu cursus euismod quis. Vehicula ipsum a arcu cursus vitae congue mauris. Proin fermentum leo vel orci porta non pulvinar neque laoreet. Enim neque volutpat ac tincidunt vitae semper quis. Ridiculus mus mauris vitae ultricies leo integer malesuada. Fames ac turpis egestas sed. Lorem ipsum dolor sit amet consectetur adipiscing elit ut aliquam. Ut diam quam nulla porttitor. Aliquet.',
            textAlign: TextAlign.justify,
            style: TextStyle(
              wordSpacing: 4.0,
              fontSize: 16.0,
              height: 1.5,
            ),
          ),
          SizedBox(
            height: 25.0,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Icon(
                CupertinoIcons.mail,
                size: 32.0,
              ),
              SizedBox(
                width: 10.0,
              ),
              Expanded(
                child: Text(
                  'If you have any questions, please email us at info@ooddss.com',
                  style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
