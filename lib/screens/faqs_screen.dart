import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

class FaqsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            FaqItem(),
            FaqItem(),
            FaqItem(),
          ],
        ),
      ),
    );
  }
}
