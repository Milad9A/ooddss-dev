import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

class ContactUS extends StatefulWidget {
  @override
  _ContactUSState createState() => _ContactUSState();
}

class _ContactUSState extends State<ContactUS> {
  int idxChecked;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    idxChecked = 0;
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text('Seller Registration'),
      ),
      body: SafeArea(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: Container(
                child: Column(
                  children: [
                    SizedBox(
                      height: 22,
                    ),
                    ClipPath(
                      clipper: BezierClipper(),
                      child: Container(
                        width: width,
                        height: width / 2,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                'assets/images/examples/map.png',
                              ),
                              fit: BoxFit.fitWidth),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      'Floor 7, Office 27, Al Salaam Mall\nSalem Al Mubarak Street Salmiya',
                      style: TextStyle(
                        fontSize: 17,
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 16, right: 16),
                      child: Column(
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                  child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Image.asset(
                                    'assets/images/icons/smart_phone.png',
                                    width: 42,
                                    height: 42,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        '+965 99005544',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 6,
                                      ),
                                      Text(
                                        '+965 96441643',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 16,
                                          height: 1.5,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              )),
                              Expanded(
                                  child: Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      'assets/images/icons/mail.png',
                                      width: 36,
                                      height: 36,
                                    ),
                                    SizedBox(
                                      height: 6,
                                    ),
                                    Text(
                                      'info@ooddss.com',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ],
                                ),
                              ))
                            ],
                          ),
                          SizedBox(
                            height: 36,
                          ),
                          Row(
                            children: [
                              Text(
                                "WRITE TO US",
                                style: TextStyle(
                                  fontSize: 22,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width / 2,
                                child: CustomDropDownButton(
                                  onChange: (val) {
                                    print(val);
                                  },
                                  title: "Title",
                                  items: ["Miss", "Mr"],
                                  selectedItem: "Miss",
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 22,
                          ),
                          CustomTextFields(
                            TextEditingController(),
                            padding: EdgeInsets.all(0),
                            hintText: "First Name",
                            validateFunction: (val) {
                              return null;
                            },
                          ),
                          SizedBox(
                            height: 22,
                          ),
                          CustomTextFields(
                            TextEditingController(),
                            padding: EdgeInsets.all(0),
                            hintText: "Last Name",
                            validateFunction: (val) {
                              return null;
                            },
                          ),
                          SizedBox(
                            height: 22,
                          ),
                          CustomTextFields(
                            TextEditingController(),
                            padding: EdgeInsets.all(0),
                            hintText: "example@examole.com",
                            keyboardType: TextInputType.emailAddress,
                            whiteFillColor: false,
                            validateFunction: (val) {
                              return null;
                            },
                          ),
                          SizedBox(
                            height: 22,
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: CustomDropDownButton(
                                  onChange: (val) {
                                    print(val);
                                  },
                                  title: "Country",
                                  items: ["Syria", "Kuwait", "Iraq", "Lebanon"],
                                  selectedItem: "Kuwait",
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 22,
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: CustomDropDownButton(
                                  onChange: (val) {
                                    print(val);
                                  },
                                  title: "Code",
                                  items: ["+963", "+976", "+963", "+954"],
                                  selectedItem: "+963",
                                ),
                                flex: 2,
                              ),
                              SizedBox(
                                width: 12,
                              ),
                              Expanded(
                                child: CustomTextFields(
                                  TextEditingController(),
                                  padding: EdgeInsets.all(0),
                                  hintText: "Phone",
                                  validateFunction: (val) {
                                    return null;
                                  },
                                ),
                                flex: 3,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 22,
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: CustomDropDownButton(
                                  onChange: (val) {
                                    print(val);
                                  },
                                  title: "Type of Request",
                                  items: [
                                    "Complaint",
                                    "Question",
                                  ],
                                  selectedItem: "Complaint",
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 22,
                          ),
                          CustomTextFields(
                            TextEditingController(),
                            padding: EdgeInsets.all(0),
                            hintText: "Message",
                            maxLines: 4,
                            keyboardType: TextInputType.multiline,
                            validateFunction: (val) {
                              return null;
                            },
                          ),
                          SizedBox(height: 32),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 62,
                    ),
                  ],
                ),
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              // fillOverscroll: true, // Set true to change overscroll behavior. Purely preference.
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  padding: const EdgeInsets.only(
                    left: 16.0,
                    right: 16,
                    bottom: 20,
                  ),
                  child:
                      RoundedEdgesFlatButton(title: "SEND", onPressed: () {}),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
