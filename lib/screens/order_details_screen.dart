import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../models/order_model.dart';
import '../widgets/widgets.dart';

class OrderDetailsScreen extends StatefulWidget {
  final Order order;
  final bool isAdmin;
  const OrderDetailsScreen({
    @required this.order,
    this.isAdmin = false,
  });

  @override
  _OrderDetailsScreenState createState() => _OrderDetailsScreenState();
}

class _OrderDetailsScreenState extends State<OrderDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView(
        padding: const EdgeInsets.symmetric(
          vertical: 8.0,
          horizontal: 20.0,
        ),
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Ordered On',
                        style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      SizedBox(height: 12.0),
                      Text(
                        'Order Status',
                        style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      SizedBox(height: 12.0),
                      Text(
                        'Payment Status',
                        style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    width: 25.0,
                  ),
                  Column(
                    children: [
                      Text(
                        ':',
                        style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      SizedBox(height: 12.0),
                      Text(
                        ':',
                        style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      SizedBox(height: 12.0),
                      Text(
                        ':',
                        style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Container(
                alignment: Alignment.centerRight,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      DateFormat.yMMMd().format(widget.order.dateTime),
                      style: TextStyle(
                        color: Color(0xFF777879),
                        fontSize: 15.0,
                        letterSpacing: 1.1,
                      ),
                    ),
                    SizedBox(height: 12.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Image(
                          height: 20.0,
                          image: AssetImage(widget.order.getStatusImageURL()),
                        ),
                        SizedBox(width: 10.0),
                        Text(
                          widget.order.status,
                          style: TextStyle(
                            fontSize: 15.0,
                            letterSpacing: 1.1,
                            color: widget.order.getStatusColor(),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 12.0),
                    Text(
                      'Paid',
                      style: TextStyle(
                        color: Color(0xFF777879),
                        letterSpacing: 1.1,
                        fontSize: 15.0,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 16.0),
          widget.order.products != null
              ? ProductsListCard(
                  products: widget.order.products,
                )
              : SizedBox.shrink(),
          SizedBox(height: 10.0),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(18.0),
              boxShadow: [
                BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0.0, 2.0),
                ),
              ],
            ),
            child: AddressInformationExpandableContainer(),
          ),
          SizedBox(height: 10.0),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(18.0),
              boxShadow: [
                BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0.0, 2.0),
                ),
              ],
            ),
            child: PaymentInformationExpandableContainer(
              fontSize: 15.0,
              kwdFontSize: 10,
            ),
          ),
          SizedBox(height: 20.0),
          widget.isAdmin
              ? Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    children: [
                      Image(
                        height: 30.0,
                        image: AssetImage('assets/images/icons/note.png'),
                      ),
                      SizedBox(width: 12.0),
                      Text(
                        'ORDER SUMMARY',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF0F6DA9),
                        ),
                      ),
                    ],
                  ),
                )
              : SizedBox.shrink(),
          SizedBox(height: 8.0),
        ],
      ),
      floatingActionButton:
          widget.isAdmin ? AdminFloatingActionButton() : SizedBox.shrink(),
    );
  }
}

class AutoSizeText {}
