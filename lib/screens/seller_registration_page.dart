import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../utils/colors_repository.dart' as repo;
import '../widgets/widgets.dart';

class SellerRegistration extends StatefulWidget {
  @override
  _SellerRegistrationState createState() => _SellerRegistrationState();
}

class _SellerRegistrationState extends State<SellerRegistration> {
  int idxChecked;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    idxChecked = 0;
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text('Seller Registration'),
      ),
      body: SafeArea(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: Container(
                child: Column(
                  children: [
                    SizedBox(
                      height: 22,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Already a seller? ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: repo.grey6,
                              fontSize: 15.0),
                        ),
                        SizedBox(
                          width: 6,
                        ),
                        InkWell(
                          onTap: () {},
                          child: Text(
                            "SIGN IN",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: repo.blue4,
                                fontSize: 16.8),
                          ),
                        ),
                      ],
                    ),
                    ClipPath(
                      clipper: BezierClipper(),
                      child: Container(
                        width: width,
                        height: width / 2,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                'assets/images/examples/register.png',
                              ),
                              fit: BoxFit.fitWidth),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 16, right: 16),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Text(
                                "SEE NOW",
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 22,
                          ),
                          CustomTextFields(
                            TextEditingController(),
                            padding: EdgeInsets.all(0),
                            hintText: "Business Instaram Username",
                            validateFunction: (val) {
                              return null;
                            },
                          ),
                          SizedBox(
                            height: 22,
                          ),
                          CustomTextFields(
                            TextEditingController(),
                            padding: EdgeInsets.all(0),
                            keyboardType: TextInputType.emailAddress,
                            hintText: "Email Address",
                            validateFunction: (val) {
                              return null;
                            },
                          ),
                          SizedBox(
                            height: 22,
                          ),
                          CustomTextFields(
                            TextEditingController(),
                            padding: EdgeInsets.all(0),
                            keyboardType: TextInputType.emailAddress,
                            hintText: "Confirm Email Address",
                            validateFunction: (val) {
                              return null;
                            },
                          ),
                          SizedBox(
                            height: 22,
                          ),
                          CustomTextFields(
                            TextEditingController(),
                            padding: EdgeInsets.all(0),
                            hintText: "Password",
                            validateFunction: (val) {
                              return null;
                            },
                          ),
                          SizedBox(
                            height: 22,
                          ),
                          CustomTextFields(
                            TextEditingController(),
                            padding: EdgeInsets.all(0),
                            hintText: "Confirm Password",
                            validateFunction: (val) {
                              return null;
                            },
                          ),
                          SizedBox(
                            height: 22,
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: CustomDropDownButton(
                                  onChange: (val) {
                                    print(val);
                                  },
                                  title: "Country",
                                  items: ["Syria", "Kuwait", "Iraq", "Lebanon"],
                                  selectedItem: "Kuwait",
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 22,
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: CustomDropDownButton(
                                  onChange: (val) {
                                    print(val);
                                  },
                                  title: "Code",
                                  items: ["+963", "+976", "+963", "+954"],
                                  selectedItem: "+963",
                                ),
                                flex: 2,
                              ),
                              SizedBox(
                                width: 12,
                              ),
                              Expanded(
                                child: CustomTextFields(
                                  TextEditingController(),
                                  padding: EdgeInsets.all(0),
                                  hintText: "Phone",
                                  validateFunction: (val) {
                                    return null;
                                  },
                                ),
                                flex: 3,
                              ),
                            ],
                          ),
                          SizedBox(
                            width: 12,
                          ),
                          CustomGroupCheckBox(
                            onChange: (index) {
                              print(index);
                            },
                            titles: [
                              "\nCheck if this is a Gift Card and Add a Gift Message below",
                            ],
                            widgets: [
                              Container(
                                child: RichText(
                                  text: TextSpan(
                                    text:
                                        '\nClicking ‘Register’ confirms that you accept our ',
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 16),
                                    children: <TextSpan>[
                                      TextSpan(
                                          text: 'Terms Of Use',
                                          style: TextStyle(
                                              color: repo.blue6, fontSize: 16),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              print("Terms");
                                            }),
                                    ],
                                  ),
                                ),
                              )
                            ],
                            allowNullCheck: true,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 62,
                    ),
                  ],
                ),
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              // fillOverscroll: true, // Set true to change overscroll behavior. Purely preference.
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  padding: const EdgeInsets.only(
                    left: 16.0,
                    right: 16,
                    bottom: 20,
                  ),
                  child: RoundedEdgesFlatButton(
                      title: "GET STARTED", onPressed: () {}),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
