import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

class CustomTextFieldPage extends StatefulWidget {
  @override
  _CustomTextFieldPageState createState() => new _CustomTextFieldPageState();
}

class _CustomTextFieldPageState extends State<CustomTextFieldPage> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  List<String> _colors = <String>[
    'Select Color',
    'red',
    'green',
    'blue',
    'orange'
  ];
  String _color = 'Select Color';

  ValueChanged<int> onChange;

  String ss;

  void f(String s) {
    s = "mohammad";
  }

  String selectedValue = "Kuwait";

  TextEditingController cont = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: new AppBar(
        title: new Text(''),
      ),
      body: new SafeArea(
          top: false,
          bottom: false,
          child: new Form(
              key: _formKey,
              autovalidate: true,
              child: new ListView(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                children: <Widget>[
                  SizedBox(
                    height: 32,
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width / 2,
                        child: CustomDropDownButton(
                          onChange: (val) {
                            print(val);
                          },
                          title: "Title",
                          items: ["Miss", "Mr"],
                          selectedItem: "Miss",
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 22,
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width / 2,
                        child: CustomDropDownButton(
                          onChange: (val) {
                            print(val);
                          },
                          title: "From",
                          items: ["From", "To"],
                          icon: "assets/images/icons/date.png",
                          selectedItem: "8-10-2020",
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 22,
                  ),
                  CustomTextFields(
                    TextEditingController(),
                    padding: EdgeInsets.all(0),
                    hintText: "First Name",
                    validateFunction: (val) {
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 22,
                  ),
                  CustomTextFields(
                    cont,
                    padding: EdgeInsets.all(0),
                    hintText: "Last Name",
                    validateFunction: (val) {
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 22,
                  ),
                  CustomTextFields(
                    TextEditingController(),
                    padding: EdgeInsets.all(0),
                    hintText: "mohammad.ali@kuwait.com",
                    validateFunction: (val) {
                      return null;
                    },
                    whiteFillColor: false,
                  ),
                  SizedBox(
                    height: 22,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: CustomDropDownButton(
                          onChange: (val) {
                            print(val);
                          },
                          title: "Country",
                          items: ["Kuwait", "Lebanon", "Iraq", "Syria"],
                          selectedItem: "Kuwait",
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 22,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: CustomDropDownButton(
                          onChange: (val) {
                            print(val);
                          },
                          title: "Code",
                          items: ["+963", "+976", "+963", "+954"],
                          selectedItem: "+963",
                        ),
                        flex: 2,
                      ),
                      SizedBox(
                        width: 12,
                      ),
                      Expanded(
                        child: CustomTextFields(
                          TextEditingController(),
                          padding: EdgeInsets.all(0),
                          hintText: "Phone",
                          validateFunction: (val) {
                            return null;
                          },
                        ),
                        flex: 3,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 22,
                  ),
                  CustomTextFields(
                    TextEditingController(),
                    padding: EdgeInsets.all(0),
                    hintText: "Free shiping Delivery Time",
                    suffixWidget: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(left: 16.0, right: 16),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            'Days',
                            style: TextStyle(
                                fontSize: 14,
                                color: Colors.black,
                                fontFamily: 'Writing'),
                            textAlign: TextAlign.center,
                          )
                        ],
                      ),
                    )),
                    validateFunction: (val) {
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 22,
                  ),
                ],
              ))),
    );
  }
}
