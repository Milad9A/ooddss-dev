import 'package:flutter/material.dart';

import 'test_dialogs_page.dart';

class MhmdScreen extends StatefulWidget {
  @override
  _MhmdScreenState createState() => _MhmdScreenState();
}

class _MhmdScreenState extends State<MhmdScreen> {
  @override
  Widget build(BuildContext context) {
    return TestDialogPage();
  }
}
