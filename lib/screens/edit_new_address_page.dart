import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

class EditNewddress extends StatefulWidget {
  @override
  _EditNewddressState createState() => _EditNewddressState();
}

class _EditNewddressState extends State<EditNewddress> {
  int idxChecked;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    idxChecked = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AppBar'),
      ),
      body: SafeArea(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: Container(
                padding: const EdgeInsets.only(left: 16.0, right: 16),
                child: Column(
                  children: [
                    SizedBox(
                      height: 32,
                    ),
                    CustomTextFields(
                      TextEditingController(),
                      padding: EdgeInsets.all(0),
                      hintText: "Home",
                      validateFunction: (val) {
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    CustomTextFields(
                      TextEditingController(),
                      padding: EdgeInsets.all(0),
                      hintText: "Street",
                      validateFunction: (val) {
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    CustomTextFields(
                      TextEditingController(),
                      padding: EdgeInsets.all(0),
                      hintText: "Floor",
                      validateFunction: (val) {
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    CustomTextFields(
                      TextEditingController(),
                      padding: EdgeInsets.all(0),
                      hintText: "Country",
                      validateFunction: (val) {
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    CustomTextFields(
                      TextEditingController(),
                      padding: EdgeInsets.all(0),
                      hintText: "City",
                      validateFunction: (val) {
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    CustomTextFields(
                      TextEditingController(),
                      padding: EdgeInsets.all(0),
                      hintText: "Country",
                      validateFunction: (val) {
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: CustomDropDownButton(
                            onChange: (val) {
                              print(val);
                            },
                            title: "Code",
                            items: ["+963", "+976", "+963", "+954"],
                            selectedItem: "+963",
                          ),
                          flex: 2,
                        ),
                        SizedBox(
                          width: 12,
                        ),
                        Expanded(
                          child: CustomTextFields(
                            TextEditingController(),
                            padding: EdgeInsets.all(0),
                            hintText: "Phone",
                            validateFunction: (val) {
                              return null;
                            },
                          ),
                          flex: 3,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    CustomTextFields(
                      TextEditingController(),
                      padding: EdgeInsets.all(0),
                      hintText: "Details",
                      maxLines: 4,
                      keyboardType: TextInputType.multiline,
                      validateFunction: (val) {
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    CustomGroupCheckBox(
                      onChange: (index) {
                        print(index);
//                          setState(() {
//                            idxChecked = index;
//                          });
                      },
                      titles: [
                        "Default for Shipping",
                        "Default for Billing",
                      ],
                      allowNullCheck: false,
                    ),
                    SizedBox(
                      height: 62,
                    ),
                  ],
                ),
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              // fillOverscroll: true, // Set true to change overscroll behavior. Purely preference.
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  padding: const EdgeInsets.only(
                    left: 16.0,
                    right: 16,
                    bottom: 20,
                  ),
                  child:
                      RoundedEdgesFlatButton(title: "UPDATE", onPressed: () {}),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
