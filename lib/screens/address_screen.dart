import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

class AddressScreen extends StatefulWidget {
  @override
  _AddressScreenState createState() => _AddressScreenState();
}

class _AddressScreenState extends State<AddressScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView(
        children: [
          AddressCard(
            title: 'HOME',
            phoneNumber: '+0999999999999',
            address: 'one line one line one line one line one line',
          ),
          AddressCard(
            title: 'WORK',
            phoneNumber: '+0999999999999',
            address:
                'two lines two lines two lines two lines two lines two lines two lines two lines two lines two lines two lines two lines',
          ),
          AddressCard(
            title: 'SCHOOL',
            phoneNumber: '+0999999999999',
            address:
                'five line five lines five lines five lines five lines five lines five lines five lines five lines five lines five lines five lines five line five lines five lines five lines five lines five lines five lines five lines five lines five lines five lines five lines five line five lines five lines five lines five lines five lines',
          ),
          AddAddressCard(),
        ],
      ),
    );
  }
}
