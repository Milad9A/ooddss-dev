import 'package:flutter/material.dart';

import 'product_details_screen.dart';

class AliScreen extends StatefulWidget {
  @override
  _AliScreenState createState() => _AliScreenState();
}

class _AliScreenState extends State<AliScreen> {
  @override
  Widget build(BuildContext context) {
    // return ExpandableViewScreen();
    // return FaqsScreen();
    // return StoreProductsScreen();
    // return FilterByScreen();
    return ProductDetailsScreen();
    // return BezierContainerScreen();
    // home screen not finished yet
    // return HomeScreen();
    // return AddProductScreen();
    // return SellPage();
    // return EditStoreScreen();
    // return StoresScreen();
  }
}
