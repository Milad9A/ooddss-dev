import 'package:flutter/material.dart';

import '../utils/colors_repository.dart' as repo;
import '../widgets/widgets.dart';

class ShippingMethodPage extends StatefulWidget {
  @override
  _ShippingMethodPageState createState() => _ShippingMethodPageState();
}

class _ShippingMethodPageState extends State<ShippingMethodPage> {
  int idxChecked;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    idxChecked = 0;
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text('SELECT SHIPPING METHOD'),
      ),
      body: SafeArea(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: Container(
                padding: const EdgeInsets.only(left: 16.0, right: 16),
                child: Column(
                  children: [
                    SizedBox(
                      height: 22,
                    ),
                    Card(
                      color: Colors.white,
                      elevation: 3,
                      shape: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(16)),
                        borderSide: BorderSide.none,
                      ),
                      child: Container(
                        width: double.infinity,
                        padding: EdgeInsets.all(14),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(16)),
                        ),
                        child: Column(
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Image.asset(
                                  "assets/images/icons/adidas.png",
                                  width: width / 10,
                                  height: width / 10,
                                ),
                                SizedBox(
                                  width: 12,
                                ),
                                Text(
                                  "Internet Shop",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 6,
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: CustomDropDownButton(
                                    onChange: (val) {
                                      print(val);
                                    },
                                    title: "Choose Shipping Method",
                                    items: [
                                      "OODDSS Fast Delivery",
                                      "OODDSS Delivery Two Method",
                                      "OODDSS Delivery Three Method",
                                      "OODDSS Delivery Four and Three Method",
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 8.0, right: 8, top: 6, bottom: 10),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Flexible(
                                    child: Text(
                                      'Shipping Cost: 2000 KWD',
                                      style: TextStyle(
                                          fontSize: 14, color: repo.blue3),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(top: 12, bottom: 16),
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: Colors.grey,
                                          width: 0.6,
                                          style: BorderStyle.solid))),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: width / 4.6,
                                    child: Center(
                                      child: Card(
                                        color: Colors.grey[100],
                                        shadowColor: Colors.grey[200],
                                        elevation: 4,
                                        shape: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(12)),
                                          borderSide: BorderSide.none,
                                        ),
                                        child: Container(
                                          width: width / 5,
                                          height: width / 5,
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                    "assets/images/examples/Nike-Shoes.png",
                                                  ),
                                                  fit: BoxFit.fill)),
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 12,
                                  ),
                                  Expanded(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Expanded(
                                          child: Text(
                                            '1 x ',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 15,
                                            ),
                                          ),
                                          flex: 1,
                                        ),
                                        Expanded(
                                          child: Text(
                                            'Apple IPhone XR 64GB esim dual',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 15,
                                            ),
                                          ),
                                          flex: 7,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(top: 12, bottom: 16),
                              decoration: BoxDecoration(border: Border()),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: width / 4.6,
                                    child: Center(
                                      child: Card(
                                        color: Colors.grey[100],
                                        shadowColor: Colors.grey[200],
                                        elevation: 4,
                                        shape: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(12)),
                                          borderSide: BorderSide.none,
                                        ),
                                        child: Container(
                                          width: width / 5,
                                          height: width / 5,
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                    "assets/images/examples/iphone.png",
                                                  ),
                                                  fit: BoxFit.fill)),
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 12,
                                  ),
                                  Expanded(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Expanded(
                                          child: Text(
                                            '1 x ',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 15,
                                            ),
                                          ),
                                          flex: 1,
                                        ),
                                        Expanded(
                                          child: Text(
                                            'Apple IPhone XR 64GB esim dual',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 15,
                                            ),
                                          ),
                                          flex: 7,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    Card(
                      color: Colors.white,
                      elevation: 3,
                      shape: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(16)),
                        borderSide: BorderSide.none,
                      ),
                      child: Container(
                        width: double.infinity,
                        padding: EdgeInsets.all(14),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(16)),
                        ),
                        child: Column(
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Image.asset(
                                  "assets/images/icons/adidas.png",
                                  width: width / 10,
                                  height: width / 10,
                                ),
                                SizedBox(
                                  width: 12,
                                ),
                                Text(
                                  "Internet Shop",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 6,
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: CustomDropDownButton(
                                    onChange: (val) {
                                      print(val);
                                    },
                                    title: "Choose Shipping Method",
                                    items: [
                                      "OODDSS Fast Delivery",
                                      "OODDSS Delivery Two Method",
                                      "OODDSS Delivery Three Method",
                                      "OODDSS Delivery Four and Three Method",
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 8.0, right: 8, top: 6, bottom: 10),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Flexible(
                                    child: Text(
                                      'Shipping Cost: 2000 KWD',
                                      style: TextStyle(
                                          fontSize: 14, color: repo.blue3),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(top: 12, bottom: 16),
                              decoration: BoxDecoration(border: Border()),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: width / 4.6,
                                    child: Center(
                                      child: Card(
                                        color: Colors.grey[100],
                                        shadowColor: Colors.grey[200],
                                        elevation: 4,
                                        shape: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(12)),
                                          borderSide: BorderSide.none,
                                        ),
                                        child: Container(
                                          width: width / 5,
                                          height: width / 5,
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                    "assets/images/examples/iphone.png",
                                                  ),
                                                  fit: BoxFit.fill)),
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 12,
                                  ),
                                  Expanded(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Expanded(
                                          child: Text(
                                            '1 x ',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 15,
                                            ),
                                          ),
                                          flex: 1,
                                        ),
                                        Expanded(
                                          child: Text(
                                            'Apple IPhone XR 64GB esim dual',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 15,
                                            ),
                                          ),
                                          flex: 7,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    CustomGroupCheckBox(
                      onChange: (index) {
                        print(index);
                      },
                      titles: [
                        "\nCheck if this is a Gift Card and Add a Gift Message below",
                      ],
                      allowNullCheck: true,
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    CustomTextFields(
                      TextEditingController(),
                      padding: EdgeInsets.all(0),
                      hintText: "Message",
                      maxLines: 4,
                      keyboardType: TextInputType.multiline,
                      validateFunction: (val) {
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 62,
                    ),
                  ],
                ),
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              // fillOverscroll: true, // Set true to change overscroll behavior. Purely preference.
              fillOverscroll: true,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  padding: const EdgeInsets.only(
                    left: 16.0,
                    right: 16,
                    bottom: 20,
                  ),
                  child: RoundedEdgesFlatButton(
                      title: "CONTINUE", onPressed: () {}),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
