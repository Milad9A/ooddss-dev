import 'package:flutter/material.dart';

import '../models/order_model.dart';
import '../widgets/widgets.dart';

class MyOrdersScreen extends StatefulWidget {
  @override
  _MyOrdersScreenState createState() => _MyOrdersScreenState();
}

class _MyOrdersScreenState extends State<MyOrdersScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView.builder(
        itemCount: orders.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(18.0),
              boxShadow: [
                BoxShadow(
                  color: Colors.black38,
                  offset: Offset(0.0, 2.0),
                  blurRadius: 3.0,
                ),
              ],
            ),
            margin: EdgeInsets.all(8.0),
            child: OrderCard(
              order: orders[index],
            ),
          );
        },
      ),
    );
  }
}
