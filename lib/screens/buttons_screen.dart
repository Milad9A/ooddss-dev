import 'package:flutter/material.dart';

import '../utils/constants.dart';
import '../widgets/widgets.dart';

class ButtonsScreen extends StatefulWidget {
  @override
  _ButtonsScreenState createState() => _ButtonsScreenState();
}

class _ButtonsScreenState extends State<ButtonsScreen> {
  var myValue = 1;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: RoundedEdgesFlatButton(
                    title: 'GET STARTED',
                    onPressed: () {},
                  ),
                ),
              ],
            ),
            SizedBox(height: 20.0),
            Row(
              children: [
                Expanded(
                  child: RoundedEdgesFlatButton(
                    title: 'GET STARTED',
                    onPressed: () {},
                  ),
                ),
                Expanded(
                  child: RoundedEdgesFlatButton(
                    title: 'GET STARTED',
                    color: kAccentColorBlackish,
                    onPressed: () {},
                  ),
                ),
              ],
            ),
            SizedBox(height: 20.0),
            Row(
              children: [
                Expanded(
                  child: RoundedEdgesOutlineButton(
                    borderColor: Colors.blue,
                    textColor: Colors.blue,
                    title: 'Track Order',
                    onPressed: () {},
                  ),
                ),
              ],
            ),
            SizedBox(height: 20.0),
            Row(
              children: [
                Expanded(
                  child: RoundedEdgesFlatButton(
                    title: 'SAVE & ADD VARIANT',
                    onPressed: () {},
                  ),
                ),
              ],
            ),
            SizedBox(height: 20.0),
            Row(
              children: [
                Expanded(
                  child: RoundedEdgesFlatButton(
                    title: 'بيبي هو بيبي',
                    onPressed: () {},
                  ),
                ),
              ],
            ),
            SizedBox(height: 20.0),
            Row(
              children: [
                RoundedEdgesFlatButton(
                  title: 'BACK',
                  color: kAccentColorBlackish,
                  onPressed: () {},
                ),
                Expanded(
                  child: RoundedEdgesOutlineButton(
                    borderColor: Colors.blue,
                    textColor: Colors.blue,
                    title: 'ADD PRODUCT',
                    onPressed: () {},
                  ),
                ),
              ],
            ),
            SizedBox(height: 20.0),
            Container(
              width: 150.0,
              child: ItemsNumberButton(
                onChanged: (value) {
                  setState(() {
                    myValue = value;
                  });
                },
              ),
            ),
            SizedBox(height: 20.0),
            Text('The Number of Items is: ${myValue.toString()}'),
            SizedBox(height: 20.0),
            RoundedEdgesFlatButton(
              title: 'ADD TO CART',
              color: Colors.black,
              icon: Icons.add_shopping_cart,
              onPressed: () {},
            )
          ],
        ),
      ),
    );
  }
}
