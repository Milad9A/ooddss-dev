class Transaction {
  String reference;
  bool type;
  DateTime dateTime;
  String amount;
  String currency;

  Transaction({
    this.amount,
    this.dateTime,
    this.reference,
    this.type,
    this.currency,
  });
}

List<Transaction> transactions = [
  Transaction(
    amount: '9.200',
    currency: 'KWD',
    reference: 'XSTUYKK',
    type: true,
    dateTime: DateTime.now(),
  ),
  Transaction(
    amount: '9.200',
    currency: 'KWD',
    reference: 'XSTUYKK',
    type: false,
    dateTime: DateTime.now(),
  ),
  Transaction(
    amount: '9.200',
    currency: 'KWD',
    reference: 'XSTUYKK',
    type: false,
    dateTime: DateTime.now(),
  ),
  Transaction(
    amount: '9.200',
    currency: 'KWD',
    reference: 'XSTUYKK',
    type: true,
    dateTime: DateTime.now(),
  ),
  Transaction(
    amount: '9.200',
    currency: 'KWD',
    reference: 'XSTUYKK',
    type: false,
    dateTime: DateTime.now(),
  ),
  Transaction(
    amount: '9.200',
    currency: 'KWD',
    reference: 'XSTUYKK',
    type: true,
    dateTime: DateTime.now(),
  ),
  Transaction(
    amount: '9.200',
    currency: 'KWD',
    reference: 'XSTUYKK',
    type: true,
    dateTime: DateTime.now(),
  ),
];
