class Product {
  String productImageURL;
  String productName;
  String productPrice;
  String productPriceCurrency;
  int numberOfAvailableProducts;
  String size;
  String color;
  Function onPressedHeart;
  Function onPressedDelete;
  Function onItemsNumberChanged;
  Function onPressedAddToCart;

  Product({
    this.productImageURL,
    this.numberOfAvailableProducts,
    this.onItemsNumberChanged,
    this.onPressedDelete,
    this.onPressedHeart,
    this.onPressedAddToCart,
    this.size,
    this.color,
    this.productName,
    this.productPrice,
    this.productPriceCurrency,
  });
}

List<Product> products = [
  Product(
    productPrice: '25.990',
    productPriceCurrency: 'KWD',
    productName: 'Apple iPhone XR 64Gb',
    productImageURL: 'assets/images/examples/iphone.png',
    numberOfAvailableProducts: 50,
    size: '118 ml',
    onPressedDelete: () {},
    onPressedHeart: () {},
    onItemsNumberChanged: (value) {},
  ),
  Product(
    productPrice: '5.000',
    productPriceCurrency: 'KWD',
    productName: 'iPhone Cable 3.00 mm',
    productImageURL: 'assets/images/examples/cable.png',
    numberOfAvailableProducts: 3,
    size: 'L',
    color: 'RED',
    onPressedDelete: () {},
    onPressedHeart: () {},
    onItemsNumberChanged: (value) {},
  ),
  Product(
    productPrice: '25.990',
    productPriceCurrency: 'KWD',
    productName: 'Nike Shoes 1',
    productImageURL: 'assets/images/examples/Nike-Shoes-2.png',
    numberOfAvailableProducts: 50,
    size: '118 ml',
    onPressedDelete: () {},
    onPressedHeart: () {},
    onItemsNumberChanged: (value) {},
  ),
  Product(
    productPrice: '5.000',
    productPriceCurrency: 'KWD',
    productName: 'Nike Shoes 2',
    productImageURL: 'assets/images/examples/Nike-Shoes.png',
    numberOfAvailableProducts: 3,
    size: 'L',
    color: 'RED',
    onPressedDelete: () {},
    onPressedHeart: () {},
    onItemsNumberChanged: (value) {},
  ),
];
