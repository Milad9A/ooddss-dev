import 'package:flutter/material.dart';

import 'product_model.dart';

class Order {
  int number;
  String status;
  DateTime dateTime;
  List<Product> products;

  Order({
    this.number,
    this.dateTime,
    this.products,
    this.status,
  });

  String getStatusImageURL() {
    String imageURL;

    switch (this.status) {
      case 'PENDING':
        imageURL = 'assets/images/icons/clock_orange.png';
        break;
      case 'DELIVERED':
        imageURL = 'assets/images/icons/truck_green.png';
        break;
      case 'CANCELLED':
        imageURL = 'assets/images/icons/cancelled_red.png';
        break;
      case 'SHIPPED':
        imageURL = 'assets/images/icons/ship_blue.png';
        break;
    }
    return imageURL;
  }

  Color getStatusColor() {
    Color color;
    switch (this.status) {
      case 'PENDING':
        color = Color(0xFFFF7200);
        break;
      case 'DELIVERED':
        color = Color(0xFF7FAB33);
        break;
      case 'CANCELLED':
        color = Color(0xFFED3123);
        break;
      case 'SHIPPED':
        color = Color(0xFF84B3D2);
        break;
      default:
        color = Colors.grey;
    }
    return color;
  }
}

List<Order> orders = [
  Order(
    dateTime: DateTime.now(),
    number: 432,
    products: products,
    status: 'PENDING',
  ),
  Order(
    dateTime: DateTime.now(),
    number: 432,
    products: products,
    status: 'DELIVERED',
  ),
  Order(
    dateTime: DateTime.now(),
    number: 432,
    products: products,
    status: 'CANCELLED',
  ),
  Order(
    dateTime: DateTime.now(),
    number: 432,
    products: products,
    status: 'SHIPPED',
  ),
];
