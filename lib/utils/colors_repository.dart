import 'package:flutter/material.dart';



Color blue1 = new Color(0xFF0D7D91);
Color blue2 = new Color(0xFF289FAA);
Color blue3 = new Color(0xFF00B5BC);
Color blue4 = new Color(0xFF0F6DA9);
Color blue5 = new Color(0xFF0595ED);
Color blue6 = new Color(0xFF1780C4);


Color green = new Color(0xFF145d6d);


Color yellow1 = new Color(0xFFFC9504);
Color yellow2 = new Color(0xFFE09136);

Color black1 = new Color(0xFF050505);
Color black2 = new Color(0xFFFEAF3F);
Color black3 = new Color(0xFF575757);

Color greyBackGround  = new Color(0xFFf1f2f4);
Color greyTextColor   = new Color(0XFF6f7070);
Color grey1 = new Color(0xFF606060);
Color grey2 = new Color(0xFF989898);
Color grey3 = new Color(0xFFA5A5A5);
Color grey4 = new Color(0xFFD4D4D4);
Color grey5 = new Color(0xFFE4E4E4);
Color grey6 = new Color(0xFF4A4B4B);



Color red = new Color(0xFFDC264D);