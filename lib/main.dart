import 'package:flutter/material.dart';

import 'screens/mhmd.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        accentColor: Color(0xFF0f6da9),
        backgroundColor: Color(0xFFf1f5fa),
        fontFamily: 'Grava',
      ),
      home: MhmdScreen(),
    );
  }
}
